# -*- coding: UTF-8 -*-

import datetime, time, pytz
import thread
import pprint
import logging

import r44ext_flight_data
from r44defs_airport_codes import getInfoForCode
import client_utils.RAMStore as r44RAMStore

import constants

log = logging.getLogger('pyprj_airside')

DEBUG = False

#--------------------------------------------------------------------#
# Retrieves current information from the real-time flight shared     #
# object.                                                            #
#                                                                    #
# Note: The flight information is cached, so the returned values are #
#       near real-time.                                              #
#                                                                    #
# Return: A dictionary with the current flight information.          #
#--------------------------------------------------------------------#
def getCurrentFlightData():
    return theRecentFlightData.getData()

#--------------------------------------------------------------------#

class RecentFlightData:
    """ Class to get and cache recent flight data. """

    CACHE_KEY = 'RecentFlightData'
    EXP_SECONDS = 30

    lock = thread.allocate_lock()
    flightData = None

    #----------------------------------------------------------------#
    # Retrieves current information from the real-time flight shared #
    # object.                                                        #
    #                                                                #
    # Note: The flight information is cached, so the returned values #
    #       are near real-time.  They will be a maximum of           #
    #       EXP_SECONDS old (unless there is an error, in which case #
    #       the last available values are returned).                 #
    #                                                                #
    # Return: A dictionary with the current flight information.      #
    #----------------------------------------------------------------#
    def getData(self):
        #log.debug('RecentFlightData.getData %d' % time.time())
        data = None

        try:
            RecentFlightData.lock.acquire()

            data = r44RAMStore.get(RecentFlightData.CACHE_KEY)
            if not data:
                #log.debug('    Calling shared object for flight data')
                data = self._getDataFromSystem()

                if data:
                    log.debug('RecentFlightData data = %s' % pprint.pformat(data))
                    r44RAMStore.set(RecentFlightData.CACHE_KEY, data, expireSeconds = RecentFlightData.EXP_SECONDS)
            else:
                #log.debug('    Returning flight info from cache')
                pass

        finally:
            RecentFlightData.lock.release()

        if not data: data = {}
        return data

    #----------------------------------------------------------------#
    # Retrieves current information from the real-time flight shared #
    # object.                                                        #
    #                                                                #
    # Note: This method should only be called from getData().        #
    #----------------------------------------------------------------#
    def _getDataFromSystem(self):
        data = {}

        try:
            # Instantiate the RecentFlightData object if it has not
            # been done already.
            if not RecentFlightData.flightData:
                #log.debug("Instantiating r44ext_flight_data.FlightDataRecent")
                RecentFlightData.flightData = r44ext_flight_data.FlightDataRecent()

            # Get the recent flight values.
            #log.debug("Calling r44ext_flight_data.FlightDataRecent.recent")
            recent_fdata = RecentFlightData.flightData.recent()
            if not recent_fdata:
                log.warning('RecentFlightData._getDataFromSystem: Calling flightData.recent() failed.')
                return data

            # Get the destination weather info and save it to the data dictionary.
            data['dest_weather'] = recent_fdata.get('dest_weather', {})

            # Get the pricing info and save it to the data dictionary.
            data['pricing'] = recent_fdata.get('pricing', '')
            data['routelock'] = recent_fdata.get('routelock', 'on')

            # Get the skymall info and save it to the data dictionary.
            data['skymall'] = recent_fdata.get('skymall', {})

            data['airline_id'] = recent_fdata.get('airline_id_icao', '')

            data['tailnum'] = recent_fdata.get('tailnum', '')
            if not data['tailnum']:
                log.warning('ERROR: Failed to get tailnum from shared object, assuming test plane')
                data['tailnum'] = 'N997ZZ'

            data['icao24b'] = recent_fdata.get('icao24b', 'unknown')
            data['public_ssid'] = recent_fdata.get('public_ssid', 'unknown')
            data['smu_sn'] = recent_fdata.get('smu_sn', '')

            # If the tail number is 6 digits and ends with 'ZZ', assume 
            # this is a test flight, otherwise assume it is an actual flight.
            if len(data['tailnum']) == 6 and data['tailnum'].upper().endswith('ZZ'):
                data['flighttype'] = constants.FLIGHT_TYPE_TEST
            else:
                data['flighttype'] = constants.FLIGHT_TYPE_REAL

            data['fltnum'] = recent_fdata.get('fltnum', '')
            data['fltleg'] = recent_fdata.get('dateleg', '')

            # Get the destination and origin city codes.
            dcode = recent_fdata.get('dest', '')
            ocode = recent_fdata.get('orig', '')
 
            # Lookup the cities that matches the codes.
            dcityinfo = getInfoForCode(dcode)
            ocityinfo = getInfoForCode(ocode)

            #log.debug('RecentFlightData._getDataFromSystem: city code = %s' % dcode)
            dcity = dcityinfo.get('display', '')
            ocity = ocityinfo.get('display', '')

            # Save the codes and cities to the data dictionary.
            data['dcode'] = dcode
            data['dcity'] = dcity
            data['ocode'] = ocode
            data['ocity'] = ocity

            if dcity:
                data['dcitycode'] = data['dcity']
            else:
                data['dcitycode'] = data['dcode']
            if ocity:
                data['ocitycode'] = data['ocity']
            else:
                data['ocitycode'] = data['ocode']

            #data['dstate'] = US_STATE_CODES.get(dcityinfo.get('state', 'XXX'), '')
            data['dstate'] = dcityinfo.get('state', '')
            data['ostate'] = ocityinfo.get('state', '')
 
            data['dcountry'] = dcityinfo.get('country', '')
            data['ocountry'] = ocityinfo.get('country', '')
 
            data['dicao'] = dcityinfo.get('icao', '')        # The 4-letter ICAO code
            data['diata'] = dcityinfo.get('iata', '')        # The 3-letter IATA code

            data['oicao'] = ocityinfo.get('icao', '')        # The 4-letter ICAO code
            data['oiata'] = ocityinfo.get('iata', '')        # The 3-letter IATA code

            # See whether this is a complimentary (free) flight or not.
            comproute = recent_fdata.get('comproute', '')
            if comproute == 'on':
                data['comproute'] = True
            else:
                data['comproute'] = False
            #log.debug('    comproute = %s' % str(data['comproute']))

            # Convert the numeric latitude to degree, minutes, seconds.
            latOrig = recent_fdata.get('lat', '0')
            lat = float(latOrig)
            latDir = 'North'
            tmp = lat
            if lat < 0:
                lat = lat * -1
                latDir = 'South'
            latDegree = int(tmp)
            tmp = (tmp - latDegree) * 60.0
            latMinute = int(tmp)
            tmp = (tmp - latMinute) * 60.0
            latSecond = int(tmp)

            # Convert the numeric longitude to degree, minutes, seconds.
            lonOrig = recent_fdata.get('lon', '0')
            lon = float(lonOrig)
            lonDir = 'East'
            tmp = lon
            if tmp < 0:
                tmp = tmp * -1
                lonDir = 'West'
            lonDegree = int(tmp)
            tmp = (tmp - lonDegree) * 60.0
            lonMinute = int(tmp)
            tmp = (tmp - lonMinute) * 60.0
            lonSecond = int(tmp)

            # Save the latitude and longitude info to the data dictionary.
            data['lat'] = latOrig
            data['latd'] = latDegree
            data['latm'] = latMinute
            data['lats'] = latSecond
            data['latns'] = latDir
            data['lon'] = lonOrig
            data['lond'] = lonDegree
            data['lonm'] = lonMinute
            data['lons'] = lonSecond
            data['lonew'] = lonDir

            # Save location(lat,lon) history to the data dictionary.
            data['lochist'] = recent_fdata.get('lochist', None)
            data['dist_remain'] = recent_fdata.get('dist_remain', None)

            currentTime = int(time.time())
            data['current'] = currentTime

            # Convert the ETA string to AM/PM time and time-to-go.
            eta = recent_fdata.get('dest_eta', '')
            eta = eta.replace('.', ':')
            tmp = eta.split(':')
            if len(tmp) >= 2:
                arrivalHour = int(tmp[0])
                arrivalMinute = int(tmp[1])

                #log.debug('    eta = %s, arrivalHour = %d, arrivalMinute = %d' % (eta, arrivalHour, arrivalMinute))

                # Get the current date/time in UTC.
                nowUTC = datetime.datetime.utcnow()
                nowUTC = nowUTC.replace(tzinfo = pytz.utc)

                # Get an estimate of the arrival date/time in UTC.
                #
                # The following is not perfect, but since the data we get from
                # the flight computer does not include date info, it is the best
                # we can do.  Take the current date/time and replace the hour
                # with the arrival hour.  If the arrival hour is less than the
                # original current hour, assume the arrival is tomorrow and
                # increment the day.
                #log.debug('    arrivalHour = %d, arrivalMinute = %d, nowHour = %d, nowMinute = %d' % (arrivalHour, arrivalMinute, nowUTC.hour, nowUTC.minute))
                arrivalUTC = nowUTC.replace(hour = arrivalHour, minute = arrivalMinute)
                arrivalTime = time.mktime(arrivalUTC.timetuple())
                theDiff = arrivalTime - currentTime
                #log.debug('    arrivalTime: %d, currentTime: %d, difference: %d' % (arrivalTime, currentTime, theDiff))
                if theDiff > 12 * 60 * 60:
                    # Decrement the date.
                    #log.debug('    decrementing the date!')
                    delta = datetime.timedelta(days = 1)
                    arrivalUTC = arrivalUTC - delta
                elif theDiff < -12 * 60 * 60:
                    # Increment the date.
                    #log.debug('    incrementing the date!')
                    delta = datetime.timedelta(days = 1)
                    arrivalUTC = arrivalUTC + delta

                #if arrivalHour < nowUTC.hour:
                #    # Increment the date.
                #    delta = datetime.timedelta(days = 1)
                #    arrivalUTC = arrivalUTC + delta
                #log.debug('    arrivalUTC = %s' % str(arrivalUTC))

                # Get timezone information for the destination city.
                tzone = dcityinfo.get('tzone', 'UTC')
                #log.debug('    timezone = %s' % tzone)
                tz = pytz.timezone(tzone)

                # Convert the arrival date/time to the local time of the
                # destination city.
                arrivalLocal = arrivalUTC.astimezone(tz)
                #log.debug('    arrivalLocal = %s' % str(arrivalLocal))

                # Format the arrival date to AM/PM notation.
                etad = arrivalLocal.strftime('%I:%M %p').lower()
                # Format the arrival date to 24-hour notation.
                eta24 = arrivalLocal.strftime('%H:%M').lower()

                etat = 'UTC'
                if tzone != 'UTC': etat = 'local time'

                # The time-to-go is the arrival time - current time.
                showNeg = False
                timeToGo = arrivalUTC - nowUTC
                #log.debug('    time-to-go days = %d, seconds = %d' % (timeToGo.days, timeToGo.seconds))
                if timeToGo.days < 0:
                    # The plane is late, so swap the subtraction but indicate
                    # the hour display should be negative.
                    showNeg = True
                    timeToGo = nowUTC - arrivalUTC
                    #log.debug('    time-to-go days = %d, seconds = %d' % (timeToGo.days, timeToGo.seconds))

                # Calculate the number of hours to go.
                ttgHours = (24 * timeToGo.days) + (timeToGo.seconds / 3600)

                # Calculate the number of minutes to go.
                ttgTmp = timeToGo.seconds % 3600
                ttgMinutes = ttgTmp / 60

                # Format the time-to-go.
                if showNeg:
                    ttgc = '-%dh %dm' % (ttgHours, ttgMinutes)
                    ttgt = '-%d hr %d min' % (ttgHours, ttgMinutes)
                else:
                    ttgc = '%dh %dm' % (ttgHours, ttgMinutes)
                    ttgt = '%d hr %d min' % (ttgHours, ttgMinutes)

                if ttgHours == 1:
                    ttgf = '1 hour '
                elif ttgHours > 1:
                    ttgf = '%d hours ' % ttgHours
                elif ttgHours == -1:
                    ttgf = '-1 hour '
                elif ttgHours < 1:
                    ttgf = '-%d hours ' % ttgHours
                if ttgHours >= 1 and ttgMinutes >= 1:
                    ttgf = ttgf + 'and '
                if ttgMinutes == 1:
                    ttgf = ttgf + '1 minute'
                elif ttgMinutes > 1:
                    ttgf = ttgf + '%d minutes' % ttgMinutes

                # Calculate the total number of minutes to go.
                ttg = ttgHours * 60 + ttgMinutes
                if showNeg: ttg = ttg * -1;

                # Save the time-to-go to the data dictionary.
                data['ttg'] = str(ttg)
                data['ttgc'] = ttgc
                data['ttgt'] = ttgt
                data['ttgf'] = ttgf

                # Save the eta to the data dictionary.
                data['eta'] = eta
                data['etad'] = etad
                data['eta24'] = eta24
                data['etat'] = etat
                data['etautc'] = int(time.mktime(arrivalUTC.timetuple()))

            data['offgnd'] = int(recent_fdata.get('offgnd_utc', '-1'))

            # Convert the heading degrees into a direction.
            # A chart from the following page was used in the caclulations:
            #       http://en.wikipedia.org/wiki/Azimuth
            # Every 22.5 degrees is a new direction.  For example, 0 (or 360)
            # is North, 22.5 is North-Northeast, and 45 is Northeast.  The
            # values used below are the points midway between each heading,
            # so if a heading is 30 degrees, for example, that is closer to
            # 22.5 (North-Northeast) than 45 (Northeast).
            headingOrig = recent_fdata.get('hdg', '0')
            headingStr = ' ' 
            headingAbbrStr = ' ' 
            try:
                heading = float(headingOrig)
                if heading < 0: heading += 360
                elif heading > 360: heading -= 360

                if heading <= 11.25:
                    headingStr = 'North'
                    headingAbbrStr = 'N'
                elif heading <= 33.75:
                    headingStr = 'North-Northeast'
                    headingAbbrStr = 'NE'
                elif heading <= 56.25:
                    headingStr = 'Northeast'
                    headingAbbrStr = 'NE'
                elif heading <= 78.75:
                    headingStr = 'East-Northeast'
                    headingAbbrStr = 'NE'
                elif heading <= 101.25:
                    headingStr = 'East'
                    headingAbbrStr = 'E'
                elif heading <= 123.75:
                    headingStr = 'East-Southeast'
                    headingAbbrStr = 'SE'
                elif heading <= 146.25:
                    headingStr = 'Southeast'
                    headingAbbrStr = 'SE'
                elif heading <= 168.75:
                    headingStr = 'South-Southeast'
                    headingAbbrStr = 'SE'
                elif heading <= 191.25:
                    headingStr = 'South'
                    headingAbbrStr = 'S'
                elif heading <= 213.75:
                    headingStr = 'South-Southwest'
                    headingAbbrStr = 'SW'
                elif heading <= 236.25:
                    headingStr = 'Southwest'
                    headingAbbrStr = 'SW'
                elif heading <= 258.75:
                    headingStr = 'West-Southwest'
                    headingAbbrStr = 'SW'
                elif heading <= 281.25:
                    headingStr = 'West'
                    headingAbbrStr = 'W'
                elif heading <= 303.75:
                    headingStr = 'West-Northwest'
                    headingAbbrStr = 'NW'
                elif heading <= 326.25:
                    headingStr = 'Northwest'
                    headingAbbrStr = 'NW'
                elif heading <= 348.75:
                    headingStr = 'North-Northwest'
                    headingAbbrStr = 'NW'
                else:
                    headingStr = 'North'
                    headingAbbrStr = 'N'
            except:
                pass

            data['headingVal'] = headingOrig
            data['headingStr'] = headingStr
            data['headingAbbrStr'] = headingAbbrStr
            data['headingImage'] = 'url("/project_ext/flight/compass_%s.png")' % headingAbbrStr

            # Save the other values to the data dictionary.
            gspdKnots = float(recent_fdata.get('gspd', '0'))
            data['gspdVal'] = '%d' % int(round(gspdKnots * 1.15077944))                # convert to mph
            data['gspd'] = '%s mph' % formatInt(int(round(gspdKnots * 1.15077944)))    # convert to mph
            data['gspdm'] = '%s km/h' % formatInt(int(round(gspdKnots * 1.8519999)))   # convert to km/h

            altOrig = recent_fdata.get('alt', '0')
            alt = float(altOrig)
            alts = '%s ft' % formatInt(int(alt)) # feet
            data['altVal'] = altOrig
            data['alt'] = alts
            alts = '%s m' % formatInt(int(float(alt) * 0.3048)) # convert to meters
            data['altm'] = alts

            #dtg = recent_fdata.get('dist_togo', '')
            #data['dtg'] = int(round(float(dtg))) # miles
            #data['dtgm'] = int(round(float(dtg) * 1.609)) # convert to km

            # Add the time the data was updated.
            # Note: Always do this last, so that if there is an error above,
            #       the updated key is not added.
            data['updated'] = time.time()
        except Exception, exc:
            log.warn('ERROR: RecentFlightData._getDataFromSystem exception: %s' % str(exc))

        #log.debug('RecentFlightData._getDataFromSystem() returning %s:' % pprint.pformat(data))

        return data

def formatInt(number):
    if number > 1000:
        return '%d %03d' % (number / 1000, number % 1000)
    else:
        return '%d' % number

# Instantiate a single instance of the RecentFlightData class.
theRecentFlightData = RecentFlightData()

