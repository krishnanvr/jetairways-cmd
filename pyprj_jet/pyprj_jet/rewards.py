# -*- coding: utf-8 -*-
### data.py :: module interface to access data ###

import os
import pprint
import logging

import client_utils.device as r44device
try:
    import r44client_swa.rapid_rewards as rapid_rewards
except:
    rapid_rewards = None

from data import get_flight_data, get_device_info


REWARDS_TEST_MODE_FILE = '/var/state/rapid_rewards_test_mode'

log = logging.getLogger('pyprj_airside')


def get_rewards_url(view):
    # Get the current flight data.
    flightData = get_flight_data()

    # Get the device category / type used for the request.
    deviceCategory = r44device.getDeviceCategory(view.request)
    deviceType = r44device.getDeviceType(view.request)

    # Store the user info into a dictionary.
    userInfo = {}
    userInfo['SID'] = view.sessionId
    userInfo['CIP'] = view.ipAddress
    userInfo['MAC'] = view.macAddress
    if deviceCategory in (r44device.DEVICE_CATEGORY_LAPTOP, r44device.DEVICE_CATEGORY_TABLET):
        userInfo['DEV'] = 'L'
    else:
        userInfo['DEV'] = 'M'

    ## Get the Rapid Rewards domain.
    if os.path.exists(REWARDS_TEST_MODE_FILE):
        rrDomain = view.project_settings.get('swaRapidRewardsTestURL', '')
    else:
        rrDomain = view.project_settings.get('swaRapidRewardsURL', '')

    url = rapid_rewards.getRapidRewardsUrl(rrDomain, userInfo, flightData)
    log.debug('getRapidRewards: url = %s' % url)

    # Create values for the confirmation page the user will be redirected
    # back to.
    # TODO
    #payment.createConfirmationValuesForAlist(self)

    return url

