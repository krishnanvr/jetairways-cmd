# -*- coding: utf-8 -*-

import threading
import logging

import portal_svcs.clients_auth.auth_utils as auth_utils

AUTH_OBJECT = None
AUTH_LOCK   = threading.Semaphore(1)

log = logging.getLogger('pyprj_airside')

def getAuthenticationObject():
    global AUTH_LOCK
    global AUTH_OBJECT

    AUTH_LOCK.acquire()
    try:
        if not AUTH_OBJECT:
            AUTH_OBJECT = auth_utils.AuthVideo()

        # Get a value from the auth object to make sure it is okay.
        #dummy = AUTH_OBJECT.getLastUpdated()
    except Exception, exc:
        # If there was an error, try to create the auth object again.
        try:
            AUTH_OBJECT = auth_utils.AuthVideo()
            #dummy = AUTH_OBJECT.getLastUpdated()
        except Exception, exc2:
            log.warning('Error in shared.getAuthenticationObject: %s' % str(exc))
            AUTH_OBJECT = None
    AUTH_LOCK.release()

    return AUTH_OBJECT

