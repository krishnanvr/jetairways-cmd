import json
import random
import data
import client_utils.RAMStore as r44RAMStore
import collections

def get_cached_json(file_path, key):
    data = r44RAMStore.get(key)
    if(data):
        return data
    else:
        file = open(file_path)
        data = json.load(file, object_pairs_hook=collections.OrderedDict)
        file.close()
        r44RAMStore.set(key, data, 20)
        return data

def get_ad_dist():
    return get_cached_json('pyprj_airside/static/pace/ad/dist.json','pi_ad_dist' )

def get_ads():
    return get_cached_json('pyprj_airside/static/pace/ad/ads.json','pi_ad_set' )

def get_all_city_guide_data():
    return get_cached_json('pyprj_airside/static/pace/city-guide/content.json','pi_content' )

def get_dest_ad_data(diata):
    dist_data = get_ad_dist()
    ad_data = get_ads()

    if(dist_data and dist_data.has_key(diata) and ad_data):
        dest_ads = dist_data[diata]    
        #pick a random ad
        ad = dest_ads[random.choice(dest_ads.keys())]
        return ad_data[str(ad)]

def get_dest_city_guide_data(diata):
    all_city_guides = get_all_city_guide_data()
    if(all_city_guides.has_key(diata)):
        return all_city_guides[diata]
    
def get_diata(request):
    if(request.request.GET.has_key('diata')):
        return request.request.GET['diata']
    else:
        flight_data = data.get_test_flight_data()
        return flight_data.get('diata', 'LAX')
