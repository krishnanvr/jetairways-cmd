# -*- coding: utf-8 -*-

import datetime, pytz
import os.path
import threading
import time
import types
import urllib

import pprint
import logging

import xml.dom.minidom
from xml.dom.minidom import Node
from xml.dom.minidom import Text

from pkg_resources import resource_filename

import client_utils.RAMStore as r44RAMStore
import constants

# Lock to make sure only one thread is parsing at a time.
PARSING_LOCK = threading.Semaphore(1)

log = logging.getLogger('pyprj_airside')


PROJECT_SETTINGS = None

def getProjectSettings():
    """ Get the project-wide settings from the project.xml file and any
        "override" files.
    """
    global PROJECT_SETTINGS
    global PARSING_LOCK

    # Only read settings from file(s) one time.
    if PROJECT_SETTINGS:
        return PROJECT_SETTINGS

    # Acquire the lock.
    PARSING_LOCK.acquire()

    try:
        folder = r44RAMStore.get(constants.PROJECT_FOLDER_KEY)

        projectFile = folder + '/project.xml'
        log.info('getProjectSettings: Reading projectFile = %s' % projectFile)

        # Parse the standard project file.
        PROJECT_SETTINGS = _projectParse(projectFile)

        # Set any other values based on any "override" files.  Note that
        # any node in an override file with the same name as a node in the
        # project file will take precedence.
        overridesFolder = folder + '/overrides'
        if os.path.exists(overridesFolder) and os.path.isdir(overridesFolder):
            ufiles = (os.path.join(overridesFolder, fn) for fn in os.listdir(overridesFolder))
            for fn in ufiles:
                if os.path.isfile(fn) and fn.endswith('.xml'):
                    log.info('Found override file %s' % fn)
                    _projectOverridesParse(fn, PROJECT_SETTINGS)
    except Exception, exc:
        log.warning('Error in xmlparsing.getProjectSettings: %s' % str(exc))
        PROJECT_SETTINGS = None

    if not PROJECT_SETTINGS:
        PROJECT_SETTINGS = { 'data': 'error' }

    # Release the lock.
    PARSING_LOCK.release()

    log.info('PROJECT SETTINGS DICTIONARY: %s' % pprint.pformat(PROJECT_SETTINGS))

    return PROJECT_SETTINGS

def _projectParse(projectFile):

    info = {}

    try:
        # If XML project file does not exist, return now.
        #if not os.path.exists(projectFile):
        #    log.warning("Error in _projectParse: %s not found" % projectFile)
        #    return info


        # Parse the XML project file.
        #doc = xml.dom.minidom.parse(projectFile)
        doc = xml.dom.minidom.parse(open(resource_filename(__name__, projectFile)))

        project = getChildWithName(doc, 'project')
        childrenDict = getChildrenDict(project)

        # Process swiftAirMedia node manually.
        swiftAir = getChildWithName(childrenDict, 'swiftAirMedia')
        swiftAirDict = getChildrenDict(swiftAir)
        if swiftAirDict:
            # Get URL.
            url = getDataFromChildWithName(swiftAirDict, 'swiftAirMediaURL')
            if url:
                info['swiftAirMediaURL'] = url
            url = getDataFromChildWithName(swiftAirDict, 'swiftAirMediaTestURL')
            if url:
                info['swiftAirMediaTestURL'] = url

            # Get list of valid destinations.
            destinations = []
            for vItemNode in getChildrenWithName(swiftAirDict, 'swiftAirMediaDest'):
                destinations.append(getDataFromNode(vItemNode))
            if destinations:
                info['swiftAirMediaDests'] = destinations

            # Node has been processed, so remove it.
            del childrenDict['swiftAirMedia']

        # Process naxShopping node manually.
        naxShop = getChildWithName(childrenDict, 'naxShopping')
        naxShopDict = getChildrenDict(naxShop)
        if naxShopDict:
            # Get list of valid originations.
            originations = []
            for vItemNode in getChildrenWithName(naxShopDict, 'naxShoppingOrig'):
                originations.append(getDataFromNode(vItemNode))
            if originations:
                info['naxShoppingOrigs'] = originations

            # Get list of valid destinations.
            destinations = []
            for vItemNode in getChildrenWithName(naxShopDict, 'naxShoppingDest'):
                destinations.append(getDataFromNode(vItemNode))
            if destinations:
                info['naxShoppingDests'] = destinations

            # Node has been processed, so remove it.
            del childrenDict['naxShopping']

        # Process naxSeatwave node manually.
        naxSeat = getChildWithName(childrenDict, 'naxSeatwave')
        naxSeatDict = getChildrenDict(naxSeat)
        if naxSeatDict:
            # Get list of valid destinations.
            destinations = []
            for vItemNode in getChildrenWithName(naxSeatDict, 'naxSeatwaveDest'):
                destinations.append(getDataFromNode(vItemNode))
            if destinations:
                info['naxSeatwaveDests'] = destinations

            # Node has been processed, so remove it.
            del childrenDict['naxSeatwave']

        # Process default ad nodes manually.
        adCount = 0
        defaultAds = []
        for adNode in getChildrenWithName(childrenDict, 'defaultAd'):
            adCount += 1
            adDict = {
                'adImageURL': getDataFromChildWithName(adNode, 'adImageURL'),
                'linkURL': getDataFromChildWithName(adNode, 'linkURL'),
                'adTextPaidIPTV': getDataFromChildWithName(adNode, 'adTextPaidIPTV', False),
                'adText': getDataFromChildWithName(adNode, 'adText', False),
            }
            defaultAds.append(adDict)
        info['defaultAds'] = defaultAds
        #print 'defaultAds = %s' % pprint.pformat(defaultAds)

        if adCount > 0:
            # Default ad nodes have been processed, so remove them.
            del childrenDict['defaultAd']

        # Process currency node(s) manually.
        currencyList = []
        currencyDefaultISO = getDataFromChildWithName(childrenDict, 'currencyDefaultISO')
        currencyDefault = {}
        for node in getChildrenWithName(childrenDict, 'currency'):
            currencyISO = getDataFromChildWithName(node, 'currencyISO')
            c = {
                'currencyISO': currencyISO,
                'currencyFormatInt': getDataFromChildWithName(node, 'currencyFormatInt'),
                'currencyFormatFloat': getDataFromChildWithName(node, 'currencyFormatFloat'),
                'currencyName': getDataFromChildWithName(node, 'currencyName', False),
                'currencyFormatSTZ': getBooleanOrDefaultFromChildWithName(node, 'currencyFormatStripTrailingZero', False),
                'currencyWifiDefaultPrice': float(getDataFromChildWithName(node, 'currencyWifiDefaultPrice')),
                'currencyTVDefaultPrice': float(getDataFromChildWithName(node, 'currencyTVDefaultPrice')),
                'currencyMovieDefaultPrice': float(getDataFromChildWithName(node, 'currencyMovieDefaultPrice')),
            }
            #if altLang and c.get('currencyName', ''):
            #    key = 'currencyName%s' % altLang
            #    c[key] = getDataOrDefaultFromChildWithName(node, key, c['currencyName'])

            currencyList.append(c)
            #log('currency = %s' % pprint.pformat(c))

            if currencyDefaultISO == currencyISO:
                currencyDefault = c

        # Currency nodes have been processed, so remove them.
        del childrenDict['currency']
        del childrenDict['currencyDefaultISO']

        # If no default currency explicitly defined, use the first one defined.
        if not currencyDefault and len(currencyList) > 0:
            currencyDefault = currencyList[0]
            currencyDefaultISO = currencyDefault.get('currencyISO')

        info['currencyList'] = currencyList
        info['currencyDefault'] = currencyDefault
        info['currencyDefaultISO'] = currencyDefaultISO

        # Process disable node manually.
        disable = getChildWithName(childrenDict, 'disable')
        disableDict = getChildrenDict(disable)
        if disableDict:
            for name, nodes in disableDict.items():
                for node in nodes:
                    info[name] = getBooleanFromNode(node)
                    log.debug('%s = %s' % (name, str(info[name])))

            # Disable nodes have been processed, so remove them.
            del childrenDict['disable']

        # Process complimentary node manually.
        comp = getChildWithName(childrenDict, 'complimentary')
        compDict = getChildrenDict(comp)
        if compDict:
            for name, nodes in compDict.items():
                # If name ends with 'Comp', strip it.  For example, 'tvpkgComp' becomes 'tvpkg'.
                if name.endswith('Comp'):
                    name = name[:-4]

                for node in nodes:
                    info['complimentary' + name.lower()] = getBooleanFromNode(node)
                    #print 'complimentary%s = %s' % (name, str(info['complimentary' + name]))

            # Complimentary nodes have been processed, so remove them.
            del childrenDict['complimentary']

        # Process pricing node manually.
        pricing = getChildWithName(childrenDict, 'pricing')
        pricingDict = getChildrenDict(pricing)
        if pricingDict:
            for name, nodes in pricingDict.items():
                for node in nodes:
                    info[name] = getDataFromNode(node)
                    log.debug('%s = %s' % (name, str(info[name])))

            # Pricing nodes have been processed, so remove them.
            del childrenDict['pricing']

        # Iterate through the child nodes and set the corresponding dictionary values.
        # This assumes each dictionary key should match the node name and each value
        # should be a string.  Any node where this is not the case must be processed
        # separately.
        for name, nodes in childrenDict.items():
            for node in nodes:
                info[name] = getDataFromNode(node)
                #print 'name = %s, value = %s' % (name, info[name])

        info['externalLinkTarget'] = getDataOrDefaultFromChildWithName(childrenDict, 'externalLinkTarget', '_self')
        info['externalUseFrame'] = getBooleanOrDefaultFromChildWithName(childrenDict, 'externalUseFrame', False)

        info['headerLinkSignup'] = getBooleanOrDefaultFromChildWithName(childrenDict, 'headerLinkSignup', True)
        info['headerShowHideBanner'] = getBooleanOrDefaultFromChildWithName(childrenDict, 'headerShowHideBanner', False)

        hdrSponsorDisplayList = []
        for vItemNode in getChildrenWithName(childrenDict, 'headerSponsorDisplay'):
            hdrSponsorDisplayList.append(getDataFromNode(vItemNode))
        if hdrSponsorDisplayList:
            info['headerSponsorDisplayList'] = hdrSponsorDisplayList
            del info['headerSponsorDisplay']

        url = info.get('headerSponsorURL', '')
        if url:
            # Also save a url-encoded version of the link.
            info['headerSponsorURLEnc'] = urllib.quote_plus(url)
        url = info.get('headerSingleImageURL', '')
        if url:
            # Also save a url-encoded version of the link.
            info['headerSingleImageURLEnc'] = urllib.quote_plus(url)
        info['headerSponsorLinkTarget'] = getDataOrDefaultFromChildWithName(childrenDict, 'headerSponsorLinkTarget', '_self')
        info['headerSponsorLinkTargetAuth'] = getDataOrDefaultFromChildWithName(childrenDict, 'headerSponsorLinkTargetAuth', '_self')

        info['siteBackgroundIncludeFooter'] = getBooleanOrDefaultFromChildWithName(childrenDict, 'siteBackgroundIncludeFooter', False)
        info['adSectionBackgroundImage'] = getDataOrDefaultFromChildWithName(childrenDict, 'adSectionBackgroundImage', '/skytown_data/spacer.gif')
        info['adSectionBackgroundImageNavheader2'] = getDataOrDefaultFromChildWithName(childrenDict, 'adSectionBackgroundImageNavheader2', '/skytown_data/spacer.gif')
        info['contentSectionBackgroundImage'] = getDataOrDefaultFromChildWithName(childrenDict, 'contentSectionBackgroundImage', '/skytown_data/spacer.gif')
        info['contentSectionBackgroundImageNavheader23'] = getDataOrDefaultFromChildWithName(childrenDict, 'contentSectionBackgroundImageNavheader23', '/skytown_data/spacer.gif')
        info['smallPanelBackgroundImage'] = getDataOrDefaultFromChildWithName(childrenDict, 'smallPanelBackgroundImage', '/skytown_data/spacer.gif')
        info['largePanelBackgroundImage'] = getDataOrDefaultFromChildWithName(childrenDict, 'largePanelBackgroundImage', '/skytown_data/spacer.gif')
        info['ualPanelBackgroundImage'] = getDataOrDefaultFromChildWithName(childrenDict, 'ualPanelBackgroundImage', '/skytown_data/spacer.gif')
        info['halfBannerPanelBackgroundImage'] = getDataOrDefaultFromChildWithName(childrenDict, 'halfBannerPanelBackgroundImage', '/skytown_data/spacer.gif')

        ad1DisplayList = []
        for vItemNode in getChildrenWithName(childrenDict, 'pageAd1Display'):
            ad1DisplayList.append(getDataFromNode(vItemNode))
        if ad1DisplayList:
            info['pageAd1DisplayList'] = ad1DisplayList
            del info['pageAd1Display']

        info['pageAd1LinkTarget'] = getDataOrDefaultFromChildWithName(childrenDict, 'pageAd1LinkTarget', '_self')
        info['pageAd1LinkTargetAuth'] = getDataOrDefaultFromChildWithName(childrenDict, 'pageAd1LinkTargetAuth', '_self')

        ad2DisplayList = []
        for vItemNode in getChildrenWithName(childrenDict, 'pageAd2Display'):
            ad2DisplayList.append(getDataFromNode(vItemNode))
        if ad2DisplayList:
            info['pageAd2DisplayList'] = ad2DisplayList
            del info['pageAd2Display']

        info['pageAd2LinkTarget'] = getDataOrDefaultFromChildWithName(childrenDict, 'pageAd2LinkTarget', '_self')
        info['pageAd2LinkTargetAuth'] = getDataOrDefaultFromChildWithName(childrenDict, 'pageAd2LinkTargetAuth', '_self')

        info['usesWeatherWidget'] = getBooleanOrDefaultFromChildWithName(childrenDict, 'usesWeatherWidget', True)

        if info.get('widgetCombinedFlightTitleOn300x250', '') and info.get('widgetCombinedFlightTitleOff300x250', '') and \
                info.get('widgetCombinedWeatherTitleOn300x250', '') and info.get('widgetCombinedWeatherTitleOff300x250', ''):
            info['widgetCombinedUseTitleImages'] = True
        else:
            info['widgetCombinedUseTitleImages'] = False

        info['surveyUseSquid'] = getBooleanOrDefaultFromChildWithName(childrenDict, 'surveyUseSquid', True)

        info['authUseCaptcha'] = getBooleanOrDefaultFromChildWithName(childrenDict, 'authUseCaptcha', False)

        authCCs = []
        for vItemNode in getChildrenWithName(childrenDict, 'authCreditCard'):
            authCCs.append(getDataFromNode(vItemNode))
        if authCCs:
            info['authCreditCards'] = authCCs
            del info['authCreditCard']

    except Exception, exc:
        log.warning('Error in xmlparsing._projectParse: %s' % str(exc))

    return info

# Called to override values set by _projectParse.
def _projectOverridesParse(projectFile, projectDict):
    try:
        # If XML project file does not exist, return now.
        if not os.path.exists(projectFile):
            log.warning("Error in _projectOverridesParse: %s not found" % projectFile)
            return info

        # Parse the XML project file.
        doc = xml.dom.minidom.parse(projectFile)

        project = getChildWithName(doc, 'project')
        childrenDict = getChildrenDict(project)

        # Iterate through the child nodes and set the corresponding dictionary values.
        # This assumes each dictionary key should match the node name and each value
        # should be a string.  Any node where this is not the case must be processed
        # separately.
        for name, nodes in childrenDict.items():
            for node in nodes:
                projectDict[name] = getDataFromNode(node)
                #print 'XXXX name = %s, value = %s' % (name, projectDict[name])

        # This is a special property that can be set only in an override
        # file.  If true, indicates this is a test site on a computer
        # that is not on an SMU.
        #projectDict['testSite'] = getBooleanOrDefaultFromChildWithName(childrenDict, 'testSite', False)

        #if getChildWithName(childrenDict, 'surveyUseSquid'):
        #    projectDict['surveyUseSquid'] = getBooleanOrDefaultFromChildWithName(childrenDict, 'surveyUseSquid', False)

    except Exception, exc:
        log.warning('Error in _projectOverridesParse: %s' % str(exc))

def _readTextFromFile(filePath):
    txt = ''
    try:
        log.debug('_readTextFromFile: reading text from %s' % filePath)
        f = open(filePath, "r")
        txt = f.read()
        f.close()
    except:
        log.warning("Error in xmlparsing._readTextFromFile: Reading file %s failed" % filePath)

    return txt

# Given a node, returns a dictionary where each key references a list of 
# child nodes with that name.
def getChildrenDict(node):
    children = {}

    if not node:
        return children

    for child in node.childNodes:
        if child.nodeType == child.ELEMENT_NODE:
            childList = children.get(child.nodeName, None)
            if not childList:
                childList = []
                children[child.nodeName] = childList
            childList.append(child)

    return children

# Given a node or a dictionary (from calling getChildrenDict), returns
# a list of all child nodes with the specified name.
def getChildrenWithName(nodeOrDict, name):
    if not nodeOrDict or not name:
        return []

    if isinstance(nodeOrDict, dict):
        children = nodeOrDict.get(name, [])
    else:
        children = []
        for child in nodeOrDict.childNodes:
            if child.nodeName == name:
                children.append(child)

    return children

# Given a node, returns the first child node with the specified name.
def getChildWithName(nodeOrDict, name):
    if not nodeOrDict or not name:
        return None

    if isinstance(nodeOrDict, dict):
        children = nodeOrDict.get(name, [])
        if len(children) >= 1:
            return children[0]
    else:
        for child in nodeOrDict.childNodes:
            if child.nodeName == name:
                return child

    return None

def getDataFromChildWithName(nodeOrDict, name, required = True):
    childNode = getChildWithName(nodeOrDict, name)
    data = (childNode and childNode.firstChild and childNode.firstChild.data) or ''
    data = data.strip()
    if required and not data:
        raise Exception('Required data for %s not found in getDataFromChildWithName.' % name)

    # Check for True / False value.
    booleanStr = data.lower()
    if booleanStr == 'true':
        data = True
    elif booleanStr == 'false':
        data = False

    return data

def getDataOrDefaultFromChildWithName(nodeOrDict, name, defaultValue):
    childNode = getChildWithName(nodeOrDict, name)
    data = (childNode and childNode.firstChild and childNode.firstChild.data) or ''
    data = data.strip()
    if not data:
        return defaultValue

    # Check for True / False value.
    booleanStr = data.lower()
    if booleanStr == 'true':
        data = True
    elif booleanStr == 'false':
        data = False

    return data

def getBooleanOrDefaultFromChildWithName(nodeOrDict, name, defaultValue):
    childNode = getChildWithName(nodeOrDict, name)
    booleanStr = (childNode and childNode.firstChild and childNode.firstChild.data) or ''
    booleanStr = booleanStr.strip().lower()
    if not booleanStr:
        return defaultValue

    # The string should be 'true' or 'false', but assume any word that
    # starts with 'f' is False and anything else is True.
    if booleanStr.startswith('f'):
        return False
    return True

def getDataFromNode(node, required = True):
    data = (node and node.firstChild and node.firstChild.data) or ''
    data = data.strip()
    if required and not data:
        if node:
            nodeName = node.nodeName
        else:
            nodeName = 'unknown'
        raise Exception('Required data not found in node %s.' % nodeName)

    # Check for True / False value.
    booleanStr = data.lower()
    if booleanStr == 'true':
        data = True
    elif booleanStr == 'false':
        data = False

    return data

# Returns an integer.
def getIntFromNode(node, required = True):
    intStr = getDataFromNode(node, required)
    if not intStr:
        return constants.NUMBER_IS_NULL
    return int(intStr)

def getDateFromNode(node, tzName, required = True):
    dateStr = getDataFromNode(node, required)
    if not dateStr: return None

    try:
        tz = pytz.timezone(tzName)
        dateParts = dateStr.split('/')
        dt = datetime.datetime(int(dateParts[2]), int(dateParts[0]), int(dateParts[1]), 0, 0, 0, 0, tz)
        return dt
    except Exception, exc:
        log.warning('Error in xmlparsing.getDateFromNode: %s' % str(exc))
        return None

    #dt = datetime.datetime(*(time.strptime(dateStr + ' 03:00:00', '%m/%d/%Y %H:%M:%S')[0:6]))
    #return dt

# Returns a price in cents.
def getPriceCentsFromNode(node, required = True):
    priceStr = getDataFromNode(node, required)
    if not priceStr:
        return constants.NUMBER_IS_NULL

    parts = priceStr.split('.')
    price = int(parts[0]) * 100
    if len(parts) > 1:
        cents = int(parts[1])
        if cents < 0 or cents > 100:
            raise Exception('Invalid price in node')
        price = price + cents
    if price < 0:
        raise Exception('Invalid price in node.')
    return price

# Returns a boolean value (True or False)
def getBooleanFromNode(node, required = True):
    value = getDataFromNode(node, required)
    if isinstance(value, bool):
        return value

    booleanStr = value.strip().lower()
    if not booleanStr: return False

    # The string should be 'true' or 'false', but assume any word that
    # starts with 'f' is False and anything else is True.
    if booleanStr.startswith('f'):
        return False
    return True

