# -*- coding: utf-8 -*-

from pyramid.view import view_config, notfound_view_config
from pyramid.renderers import get_renderer
from pyramid.response import Response
import pyramid.httpexceptions as pyrexc

import cookies, rewards, video_info
from data import get_flight_data, get_project_settings, get_session_info, get_device_info, get_home_carousel_data, get_test_flight_data, get_sample_forecast

from layout import Layout

import pprint
import logging
log = logging.getLogger('pyprj_airside')

try:
    import json
except:
    import simplejson as json

class Handler(Layout):
    def __init__(self, request):
        self.request = request

        self.project_settings = get_project_settings()
        (self.sessionId, self.ipAddress, self.macAddress) = get_session_info(request)
        (self.deviceCategory, self.deviceType) = get_device_info(request)

        self.page_values = {
             'show_arrConn': False,
             'show_beats': False,
             'mac_address': self.macAddress,
             'show_alist_popup': False,
             'header_a_class': '',
             'footer_a_class': '',
           }
        self.page_values['purchased'] = {}

        self.page_values['purchasedWifi'] = False
        self.page_values['purchasedSMS'] = False

        self.page_values['show_sms'] = True
        self.page_values['sms_comp'] = False
        self.page_values['rzauth'] = False

        self.page_values['nlc'] = self.get_nlc

        self.page_values.update(get_flight_data())

        # TLG TEMPORARY. Remove when iPad is not bombarding server with requests.
        #if self.macAddress == 'A4:D1:D2:3E:7C:0E':
        #    raise pyrexc.HTTPInternalServerError()

        log.debug('XXXXX Got request %s: session id = %s, ip address = %s, mac address = %s, device category = %s, device type = %s' % \
                  (request.path_qs, self.sessionId, self.ipAddress, self.macAddress, self.deviceCategory, self.deviceType))

class Views(Handler):

    def _add_common_page_values(self):
        #self.page_values['omniture'] = self.get_omniture_data()
        #self.page_values['ads'] = self.get_ad_data
        return self.page_values

    @view_config(route_name='angular',renderer='')
    def angular(self):
        self.request.override_renderer = 'angular_templates/%s' % 'index.pt'
        return {}

    @view_config(route_name='angular-directive', renderer='')
    def angular_directive(self):
        template_name = self.request.matchdict.get('directive_name')
        self.request.override_renderer = 'angular_templates/directives/%s.pt' % template_name

        return {}

    @view_config(route_name='home', renderer='templates/home.pt')
    def index_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/home.pt'
        #log.debug('settings = %s' % pprint.pformat(self.request.registry.settings))
        #log.debug('project settings = %s' % pprint.pformat(self.project_settings))
        cook = cookies.getCookieValue(self.request, 'r44test')
        if cook:
            log.debug('XXXXXXXXXXXXXX got cookie value: %s' % cook)
        else:
            log.debug('XXXXXXXXXXXXXX NO cookie value')

        #video_info.getVideoPackagePricing()
        #video_info.getAllMovieVideoInfo()
        #video_info.getAllTVVideoInfo()
        #video_info.getAllTVVideoInfo()
        return self.page_values


    @view_config(renderer='templates/destination.pt', name='destination')
    def destination_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/destination.pt'
        flight_data = get_test_flight_data()
        diata = flight_data.get('diata', 'LAX')
        city = flight_data.get('dcity', '')
        self.page_values['city'] = city
        self.page_values['diata'] = diata
        self.page_values['forecast'] = self.get_destination_forecast

        return self.page_values

    @view_config(renderer='templates/device_check.pt', name='device_check')
    def device_check_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/device_check.pt'
        return get_flight_data()

    @view_config(renderer='templates/city_guide.pt', name='city_guide')
    def city_guide_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/city_guide.pt'
        return get_flight_data()

    @view_config(renderer='templates/flight_tracker.pt', name='flight_tracker')
    def flight_tracker_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/flight_tracker.pt'
        return get_flight_data()

    @view_config(renderer='templates/connection.pt', name='connection')
    def connection_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/connection.pt'
        self.page_values['connections'] = self.get_connecting_flights
        return self.page_values

    @view_config(renderer='templates/epg.pt', name='epg')
    def epg_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/mepg.pt'
        self.page_values['videoData'] = self.get_video_data
        self.page_values['dish_sponsor'] = False
        return self.page_values

    @view_config(renderer='templates/epg_after.pt', name='epg_after')
    def epg_after_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/mepg2.pt'
        return self.page_values

    @view_config(renderer='templates/games.pt', name='games')
    def games_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/games.pt'
        return self.page_values

    @view_config(renderer='templates/games_detail.pt', name='games_detail')
    def game_details_view(self):
	if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/games_detail.pt'
        return self.page_values

    @view_config(renderer='templates/games_play.pt', name='games_play')
    def games_play_view(self):
	if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/games_play.pt'
        return self.page_values


    @view_config(renderer='templates/music.pt', name='music')
    def music_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/music.pt'
        
        self.page_values['songs_max'] = 10;
        self.page_values['songs_left'] = 3;
        self.page_values['beats_intl'] = '';
        self.page_values['unlocked'] = True;
        self.page_values['seen_exit_modal'] = True;
        return self.page_values

    @view_config(renderer='templates/information.pt', name='information')
    def information_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/information.pt'
        return get_flight_data()

    @view_config(renderer='templates/messaging.pt', name='messaging')
    def messaging_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/messaging.pt'
        return self.page_values

    @view_config(renderer='templates/movies.pt', name='movies')
    def movies_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/movies.pt'
        ##self.page_values['purchased'] = purchase.getPurchaseHistory(self, constants.MOVIE_PACKAGE_TYPE)
        ##self.page_values['purchasedIds'] = purchase.getPurchaseHistoryIds(self, constants.MOVIE_PACKAGE_TYPE)
        ##self.page_values['wifiauth'] = self.isAuthorized(constants.WIFI_PRODUCT_ID)
        ##self.page_values['has_player'] = bool(r44cookies.getCookieValue(self.request, 'r44player'))
        self.page_values['purchased'] = []
        self.page_values['purchasedIds'] = {}
        self.page_values['wifiauth'] = False;
        self.page_values['has_player'] = True;

        return self._add_common_page_values()

    @view_config(renderer='templates/movie_details.pt', name='movie_details')
    def movie_details_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/movie_details.pt'
        return self.page_values

    @view_config(renderer='templates/epg_vodtv_instant.pt', name='epg_vodtv_instant')
    def epg_vodtv_instant_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/epg_vodtv_instant.pt'
        return self.page_values

    @view_config(renderer='templates/purchase.pt', name='purchase')
    def purchase_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/purchase.pt'
        self.page_values['useEncryption'] = True;
        self.page_values['keyString'] = '';
        return self.page_values

    @view_config(renderer='templates/confirmation.pt', name='confirmation')
    def confirmation_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/confirmation.pt'
        return self.page_values

    @view_config(renderer='templates/shopping.pt', name='shopping')
    def shopping_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/shopping.pt'
        cookies.setCookieValue(self.request.response, 'r44test', 'shopping', 1)
        return self.page_values

    @view_config(renderer='templates/tv.pt', name='tv')
    def tv_view(self):

        def set_default(obj):
            if isinstance(obj, set):
                return list(obj)
            raise TypeError

        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/tv.pt'
        self.page_values['videoData'] = self.get_video_data
        values = {}
        values['json_video_data'] = json.dumps(self.page_values['videoData'], default=set_default)
        params = self.request.GET
        values['tab'] = params.get('tab', 'iptv')

        values['iptv_cnt'] = 25
        values['vodtv_cnt'] = 35

        values['tvprice'] = self.get_tv_pricing[1]
        values['tvauth'] =True # self.isAuthorized(constants.TV_PRODUCT_ID)
        values['dish_sponsor'] = True# self.is_tv_sponsor_dish
        values['chase_sponsor'] = True# self.is_tv_sponsor_chase
        values['ok_to_stream'] = True #self.is_streaming_allowed
 
        values['rzprice'] = self.get_nflrz_pricing[1]
        values['rzauth'] = True #self.isAuthorized(constants.NFLRZ_PRODUCT_ID)
        values['rzenabled'] = True# self.is_redzone_enabled
 
        values['wifiauth'] = True #self.isAuthorized(constants.WIFI_PRODUCT_ID)
 
        values['iptv_data'] = self.page_values['videoData'].get('iptv_data')
 
        self.page_values['values'] = values
        self.page_values['dish_sponsor'] = values['dish_sponsor']
        return self.page_values

    @view_config(renderer='templates/wifi.pt', name='wifi')
    def wifi_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/wifi.pt'
        return self.page_values

    @view_config(renderer='templates/surfing.pt', name='surfing')
    def surfing_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/surfing.pt'
        return self.page_values

    @view_config(renderer='templates/global_layout.pt', name='test')
    def test_view(self):
        log.debug('logger: %s', get_flight_data())
        log.info('info: %s', get_flight_data())
        log.warning('info: %s', get_flight_data())
        return get_flight_data()

    @view_config(renderer='json', name='current.json')
    def current_json_view(self):
        return self.get_current_info

    @view_config(renderer='json', name='weather.json')
    def weather_json_view(self):
        return self.get_current_weather

    @view_config(renderer='string', name='updates')
    def updates_view(self):
        params = self.request.GET
        log.debug("GET = %s" % pprint.pformat(params))
        log.debug("a = %s" % params.get('a', 'NONE'))
        return 'Hello!'
        #return {'content':'Hello!'}

    @notfound_view_config(renderer='templates/404.pt')
    def not_found_view(self):
        self.request.response.status = 404
        return self._add_common_page_values()

    @view_config(renderer='templates/ebooks.pt', name='ebooks')
    def ebooks_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/ebooks.pt'
        return self.page_values

    @view_config(name='getRapidRewards')
    def rr_view(self):
        url = rewards.get_rewards_url(self)
        raise pyrexc.HTTPFound(location = url)

