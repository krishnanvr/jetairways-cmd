# -*- coding: utf-8 -*-

from pyramid.config import Configurator

import client_utils.RAMStore as r44RAMStore
import constants
import mimetypes

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    #global DATA

    config = Configurator(settings=settings)
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_static_view('static/mobile', 'static/mobile', cache_max_age=3600)
    config.add_static_view('images', 'static/images', cache_max_age=3600)
    config.add_static_view('css', 'static/css', cache_max_age=3600)
    config.add_static_view('js', 'static/js', cache_max_age=3600)
    config.add_static_view('fonts', 'static/fonts', cache_max_age=3600)
    config.add_static_view('mobile', 'static/mobile', cache_max_age=3600)
    config.add_static_view('pace', 'static/pace', cache_max_age=3600)
    config.add_static_view('videos', 'static/videos', cache_max_age=3600)

    config.add_route('home', '/')
    config.add_route('angular', '/angular')
    config.add_route('angular-directive', '/angular-directive/{directive_name}')
    config.scan()

    # Get the value for the project folder and save it.
    pf = settings.get(constants.PROJECT_FOLDER_KEY, '/')
    r44RAMStore.set(constants.PROJECT_FOLDER_KEY, pf, expireSeconds = constants.CACHE_SECONDS_DAY)

    return config.make_wsgi_app()
