# -*- coding: utf-8 -*-

import json
def get_video_data_bk():
    data = json.loads('''
    {
        "bar":
        [
            "baz",
            null,
            1.0,
            2
        ]
    }''')
    return data

def get_video_data():
    data = json.loads('''
    {
    "videoData": {
        "orig_id": "159",
        "iptv_id": "32814763",
        "cindex": 0,
        "iptv_data": [
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1388563200,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 10,
        "server_size": "",
        "id": "32814763",
        "genres": "Sports",
        "title": "FOX Sports 1",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115142/mobileflyout.png",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115142/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1451721599,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115142/laptopflyout.png",
        "studio": "FOX",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "FOX Sports 1",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_21/ch21/ch21_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115142/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/33240509.m3u8",
        "genreList": [
            "Sports"
        ],
        "summary": "FOX Sports 1 is America's new 24 hour sports network that puts fans first with live coverage of the most compelling sporting events in the world, entertaining studio shows and an informative news operation.",
        "image_colorlogo": "/videos/iptv/20115142/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1368082800,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 20,
        "server_size": "",
        "id": "26914435",
        "genres": "Entertainment",
        "title": "NBC 4",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115133/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115133/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115133/laptopflyout.jpg",
        "studio": "NBC",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "NBC 4",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_13/ch13/ch13_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115133/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/33397117.m3u8",
        "genreList": [
            "Entertainment"
        ],
        "summary": "NBC 4 New York is the flagship station of the NBC Owned Television Stations group.  Located in the nation's #1 television market, NBC 4 New York's studio and offices are located in the heart of midtown Manhattan at the historic 30 Rockefeller Plaza.  In addition to carrying the network's award-winning daytime, prime-time and late night programming, NBC 4 New York produces 36 hours of live, local news programming each week.  NBC 4 New York is known for the quality of its daily newscasts, coverage of breaking news and its community outreach.",
        "image_colorlogo": "/videos/iptv/20115133/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1368082800,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 30,
        "server_size": "",
        "id": "26914433",
        "genres": "News",
        "title": "Bravo",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115132/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115132/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115132/laptopflyout.jpg",
        "studio": "NBC",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "Bravo",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_12/ch12/ch12_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115132/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/33240245.m3u8",
        "genreList": [
            "News"
        ],
        "summary": "With more breakout stars and critically acclaimed original series than any other network on cable, Bravo's original programming - from hot cuisine to haute couture - delivers the best in food, fashion, beauty, design and pop culture. Bravo translates buzz into reality with critically-acclaimed breakout creative competition and docu-series, including the Emmy and James Beard Award-nominated Top Chef, and the water cooler sensations that are The Real Housewives of New York City, The Real Housewives of Beverly Hills, and The Real Housewives of Atlanta.",
        "image_colorlogo": "/videos/iptv/20115132/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1393660800,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 40,
        "server_size": "",
        "id": "28220395",
        "genres": "Entertainment",
        "title": "CBS 2",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115137/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115137/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115137/laptopflyout.jpg",
        "studio": "",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "CBS 2",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_17/ch17/ch17_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115137/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/DISH_Southwest_Intro_Next_Room_HD_900K.m3u8",
        "genreList": [
            "Entertainment"
        ],
        "summary": "WCBS-TV / CBS 2 New York is the Flagship Station of the CBS Television Network.  CBS 2 is the most-watched station in the country during primetime and also broadcasts top-rated and award-winning local newscasts.",
        "image_colorlogo": "/videos/iptv/20115137/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1380610800,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 50,
        "server_size": "",
        "id": "31102731",
        "genres": "Entertainment",
        "title": "Discovery",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115138/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115138/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115138/laptopflyout.jpg",
        "studio": "Discovery_Networks",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "Discovery",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_18/ch18/ch18_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115138/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/33238891.m3u8",
        "genreList": [
            "Entertainment"
        ],
        "summary": "Discovery Channel, one of the most widely distributed cable networks in the U.S., is dedicated to creating the highest quality non-fiction content that informs and entertains its consumers about the world in all its wonder, diversity and amazement.  The network offers a signature mix of compelling, high-end production values and vivid cinematography across genres including, science and technology, exploration, adventure, history and in-depth, behind-the-scenes glimpses at the people, places and organizations that shape and share our world.",
        "image_colorlogo": "/videos/iptv/20115138/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1355817600,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 60,
        "server_size": "",
        "id": "20115117",
        "genres": "News",
        "title": "FOX News Channel",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115117/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115117/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "/videos/iptv/20115117/preview.png",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115117/laptopflyout.jpg",
        "studio": "FOX",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "FOX News Channel",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_7/ch7/ch7_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115117/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/33397117.m3u8",
        "genreList": [
            "News"
        ],
        "summary": "FOX News Channel is the most-watched cable news channel in America and the undisputed leader in 24-hour cable news.  With live daily programming of essential breaking events, expert political coverage and insider entertainment news, we bring our viewers the day's most important happenings and the world's most influential people.  Our 17 news bureaus around the world provide the industry's most-respected journalists with the resources they need.",
        "image_colorlogo": "/videos/iptv/20115117/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1355817600,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 70,
        "server_size": "",
        "id": "20115113",
        "genres": "Business News",
        "title": "CNBC",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115113/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115113/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "/videos/iptv/20115113/preview.png",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115113/laptopflyout.jpg",
        "studio": "NBC",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "CNBC",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_5/ch5/ch5_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115113/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/33240513.m3u8",
        "genreList": [
            "Business News"
        ],
        "summary": "CNBC is the recognized world leader in business news providing real-time data, analysis and information to more than 390 million homes worldwide. The network's 16 live hours a day of business programming in North America (weekdays from 4:00 a.m. - 8:00 p.m.) is produced at CNBC's global headquarters in Englewood Cliffs, N.J., and includes reports from CNBC News bureaus worldwide. CNBC.com and CNBC Mobile Web (mobile.cnbc.com) offer real-time stock quotes, charts, analysis and on-demand video.",
        "image_colorlogo": "/videos/iptv/20115113/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1388563200,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 80,
        "server_size": "",
        "id": "20115145",
        "genres": "Entertainment",
        "title": "Mlife",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115145/mobileflyout.png",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115145/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1451635199,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115145/laptopflyout.jpg",
        "studio": "",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "Mlife",
        "hlsUrl": "",
        "release_year": "",
        "image_background": "/videos/iptv/20115145/background.png",
        "hlsPrerollUrl": "",
        "genreList": [
            "Entertainment"
        ],
        "summary": "Mlife TV is your ticket to the hottest entertainment, dining and luxury destinations in Las Vegas.",
        "image_colorlogo": "/videos/iptv/20115145/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1355817600,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 90,
        "server_size": "",
        "id": "24546023",
        "genres": "Entertainment",
        "title": "FOX 5",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115130/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115130/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "/videos/iptv/20115130/preview.png",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115130/laptopflyout.jpg",
        "studio": "FOX",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "FOX 5",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_9/ch9/ch9_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115130/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/36781579.m3u8",
        "genreList": [
            "Entertainment"
        ],
        "summary": "Fox 5 New York is the flagship station of the Fox Network.  Located in the country's largest television market it's home to many of the most popular network, sports and syndicated programs on television.  Fox 5 broadcasts more hours of live, local news per week than any other New York broadcast station and is the home of the iconic 10pm News as well as Good Day New York.",
        "image_colorlogo": "/videos/iptv/20115130/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1371625200,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 100,
        "server_size": "",
        "id": "28220391",
        "genres": "Entertainment",
        "title": "HGTV",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115134/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115134/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115134/laptopflyout.jpg",
        "studio": "Scripps",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "HGTV",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_15/ch15/ch15_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115134/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/35273351.m3u8",
        "genreList": [
            "Entertainment"
        ],
        "summary": "HGTV offers a wide range of lifestyle entertainment that showcases practical advice and fresh ideas from diverse experts in design, building and remodeling, real estate, landscaping and party planning.  HGTV's abundance of expert personalities inspire viewers to reinvent and transform their own homes, communities, workplaces and shared spaces.",
        "image_colorlogo": "/videos/iptv/20115134/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1388563200,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 110,
        "server_size": "",
        "id": "100000119463",
        "genres": "Entertainment",
        "title": "Cartoon Network",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115143/mobileflyout.png",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115143/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454399999,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115143/laptopflyout.png",
        "studio": "IFD",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "Cartoon Network",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_22/ch22/ch22_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115143/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/35273351.m3u8",
        "genreList": [
            "Entertainment"
        ],
        "summary": "A popular youth culture brand around the world, Cartoon Network is home to the world's most unique collection of animation content, plus one-of-a-kind special events aimed at kids 2-14 years old. Touting Emmy-winning and exclusive hit shows such as Regular Show, Adventure Time, The Amazing World of Gumball, Teen Titans Go! and Ninjago, Cartoon Network also champions pro-social causes affecting families, including bullying and childhood obesity prevention.",
        "image_colorlogo": "/videos/iptv/20115143/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1380610800,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 120,
        "server_size": "",
        "id": "31102735",
        "genres": "Entertainment",
        "title": "TLC",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115139/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115139/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115139/laptopflyout.jpg",
        "studio": "Discovery_Networks",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "TLC",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_19/ch19/ch19_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115139/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/33240511.m3u8",
        "genreList": [
            "Entertainment"
        ],
        "summary": "TLC is a global brand that celebrates extraordinary people and relatable life moments through innovative nonfiction programming. A top 10 cable network in key female demos in the U.S., TLC has built successful franchises around the Cake Boss and Say Yes to the Dress brands.  In 2011, TLC had 28 series averaging 1.0 million P2+ viewers or more including Sister Wives, My Strange Addiction, Extreme Couponing, Toddlers &amp; Tiaras, 19 Kids and Counting, Long Island Medium, and What Not To Wear.",
        "image_colorlogo": "/videos/iptv/20115139/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1422777600,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 130,
        "server_size": "",
        "id": "20115146",
        "genres": "Entertainment",
        "title": "Gizmo TV",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115146/mobileflyout.png",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115146/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1451635199,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115146/laptopflyout.png",
        "studio": "",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "Gizmo TV",
        "hlsUrl": "",
        "release_year": "",
        "image_background": "/videos/iptv/20115146/background.png",
        "hlsPrerollUrl": "",
        "genreList": [
            "Entertainment"
        ],
        "summary": "GizmoTV discovers the latest in high tech - exciting electronics, fun gadgets and useful gear for home, work and on the go.",
        "image_colorlogo": "/videos/iptv/20115146/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1363935600,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 140,
        "server_size": "",
        "id": "25777171",
        "genres": "Sports",
        "title": "Golf Channel",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115131/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115131/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "/videos/iptv/20115131/preview.png",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115131/laptopflyout.jpg",
        "studio": "NBC",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "Golf Channel",
        "hlsUrl": "http://stream.southwestwifi.com:8080/iptv/ch11-master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115131/background.png",
        "genreList": [
            "Sports"
        ],
        "summary": "The Golf Channel cable network, was co-founded by Arnold Palmer and is part of the NBC Sports Group.  Exclusive partnerships with the world's top tours allow Golf Channel to feature more live golf coverage than all other networks combined.  In addition to being the leader in golf tournament coverage, GOLF CHANNEL is the home of innovative, insightful and entertaining programming, including news, instruction and original specials and series.",
        "image_colorlogo": "/videos/iptv/20115131/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1371625200,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 150,
        "server_size": "",
        "id": "28220389",
        "genres": "Entertainment",
        "title": "Food Network",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115136/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115136/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115136/laptopflyout.jpg",
        "studio": "Scripps",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "Food Network",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_14/ch14/ch14_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115136/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/33240513.m3u8",
        "genreList": [
            "Entertainment"
        ],
        "summary": "Food Network gives passionate fans an opportunity to explore the world of food in a fresh variety of ways while also giving them the technique-based knowledge they've come to trust. We spark a connection to food through pop culture, competition, adventure, and travel and deliver this connection by entertaining, engaging, and inspiring our viewers on a daily basis. From our entertaining talent to our impressive programming, we continue to redefine the way people think about food.",
        "image_colorlogo": "/videos/iptv/20115136/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1380610800,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 160,
        "server_size": "",
        "id": "31102737",
        "genres": "Entertainment",
        "title": "Animal Planet",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115140/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115140/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115140/laptopflyout.jpg",
        "studio": "Discovery_Networks",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "Animal Planet",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_20/ch20/ch20_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115140/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/33240511.m3u8",
        "genreList": [
            "Entertainment"
        ],
        "summary": "Animal Planet, part of Discovery Communication's Animal Planet Media (APM) business unit, is the world's only entertainment brand that immerses viewers in the full range of life in the animal kingdom with rich, deep content on multiple platforms. APM offers animal lovers and pet owners access to a centralized online, television and mobile community for immersive, engaging, high-quality entertainment, information and enrichment. Programming highlights on Animal Planet include Whale Wars, River Monsters, Fatal Attractions, Finding Bigfoot, Pit Bulls and Parolees and Dogs 101.",
        "image_colorlogo": "/videos/iptv/20115140/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1355817600,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 170,
        "server_size": "",
        "id": "20115119",
        "genres": "Business News",
        "title": "FOX Business Network",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115119/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115119/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "/videos/iptv/20115119/preview.png",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115119/laptopflyout.jpg",
        "studio": "FOX",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "FOX Business Network",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_8/ch8/ch8_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115119/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/33240509.m3u8",
        "genreList": [
            "Business News"
        ],
        "summary": "FOX Business Network breaks down the market and provides viewers with critical information for making more informed financial decisions.  FOX Business Network is the only cable network with an award-winning, well-respected business journalist overseeing on-air content and coverage.",
        "image_colorlogo": "/videos/iptv/20115119/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1371625200,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 180,
        "server_size": "",
        "id": "28220393",
        "genres": "Entertainment",
        "title": "Travel Channel",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115135/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115135/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115135/laptopflyout.jpg",
        "studio": "Scripps",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "Travel Channel",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_16/ch16/ch16_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115135/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/DISH_SWA_Any_Seat_900K.m3u8",
        "genreList": [
            "Entertainment"
        ],
        "summary": "Travel Channel connects people to the power and joy of human journeys that inspire, surprise, and entertain. It's a place to experience great storytelling, shared human connections, and engaging talent that celebrate the surprising encounters that happen right here and right now. Travel Channel is authentic, inquisitive, surprising and fun. It's about embracing the journey and realizing that life is a trip, authentic and real.",
        "image_colorlogo": "/videos/iptv/20115135/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1355817600,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 190,
        "server_size": "",
        "id": "20115111",
        "genres": "Sports",
        "title": "NFL Network",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115111/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115111/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "/videos/iptv/20115111/preview.png",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115111/laptopflyout.jpg",
        "studio": "NFLN",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "NFL Network",
        "hlsUrl": "http://stream.southwestwifi.com:8080/live/CHANNEL_4/ch4/ch4_master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115111/background.png",
        "hlsPrerollUrl": "http://stream.southwestwifi.com:8080/vod/33240245.m3u8",
        "genreList": [
            "Sports"
        ],
        "summary": "NFL Network is the first 24-hour, seven-day-a-week cable television network dedicated solely to the NFL. It is a destination for all that happens around the league, on and off the field - during the season and throughout the off-season.",
        "image_colorlogo": "/videos/iptv/20115111/colorlogo.png",
        "release_date": ""
    },
    {
        "change_minutes": [
            480
        ],
        "display_start_date": 1355817600,
        "packageCode": "TV",
        "defaultLanguage": "en",
        "mediaType": "iptv",
        "currency": "USD",
        "display_index": 200,
        "server_size": "",
        "id": "20115115",
        "genres": "News",
        "title": "MSNBC",
        "airlineList": [
            "SWA"
        ],
        "image_mobileflyout": "/videos/iptv/20115115/mobileflyout.jpg",
        "stars": "",
        "image_whitelogo": "/videos/iptv/20115115/whitelogo.png",
        "availableLanguages": [
            "en"
        ],
        "price": 500,
        "display_end_date": 1454313599,
        "image_pv": "/videos/iptv/20115115/preview.png",
        "director": "",
        "image_laptopflyout": "/videos/iptv/20115115/laptopflyout.jpg",
        "studio": "NBC",
        "routeList": [
            "*-*"
        ],
        "formattedPrice": "$5",
        "jsTitle": "MSNBC",
        "hlsUrl": "http://stream.southwestwifi.com:8080/iptv/ch06-master.m3u8",
        "release_year": "",
        "image_background": "/videos/iptv/20115115/background.png",
        "genreList": [
            "News"
        ],
        "summary": "MSNBC is the destination for fact-based analysis of daily headlines, insightful political commentary and perspective. MSNBC offers a full schedule of live news coverage, political opinions, and award-winning documentary programming - 24 hours a day, 7 days a week.",
        "image_colorlogo": "/videos/iptv/20115115/colorlogo.png",
        "release_date": ""
    }
],
        "tab": "iptv",
        "vodtv_id": "239",
        "vodtv_data": [
            {
                "change_minutes": [
                    480
                ],
                "packageCode": "TV",
                "defaultLanguage": "en",
                "mediaType": "vodtv",
                "currency": "USD",
                "featured": {
                    "display_start_date": 1419321600,
                    "routeList": [
                        "*-*"
                    ],
                    "display_end_date": 1422777599,
                    "airlineList": [
                        "SWA"
                    ],
                    "display_index": 8
                },
                "use_vdrm": true,
                "server_size": "",
                "id": "239",
                "genres": "Sitcom",
                "title": "The League",
                "airlineList": [
                    "SWA"
                ],
                "display_index": 13,
                "stars": "Nick Kroll, Jonathan Lajoie, Stephen Rannazzisi",
                "availableLanguages": [
                    "en"
                ],
                "price": 500,
                "image_mobileflyout": "/videos/vodtv/239/mobileflyout.jpg",
                "image_pv": "",
                "director": "",
                "image_laptopflyout": "/videos/vodtv/239/laptopflyout.jpg",
                "studio": "FOX",
                "seasons": "Season 5",
                "image_title": "/videos/vodtv/239/title.jpg",
                "formattedPrice": "$5",
                "jsTitle": "The League",
                "release_year": "",
                "image_background": "/videos/vodtv/239/background.jpg",
                "genreList": [
                    "Sitcom"
                ],
                "episodes": [
                    {
                        "rating": "TV-MA",
                        "display_start_date": 1419321600,
                        "packageCode": "TV",
                        "number": 7,
                        "mediaType": "vodtvep",
                        "currency": "USD",
                        "year": "",
                        "duration": "30",
                        "use_vdrm": true,
                        "server_size": "",
                        "id": "1532",
                        "title": "The Bringer Show",
                        "vdrm_end_date": 1425196799,
                        "airlineList": [
                            "SWA"
                        ],
                        "display_index": 3,
                        "vdrm_start_date": 1419321600,
                        "details": "TV-MA | 30 min",
                        "trackingId": "1532",
                        "parentId": "239",
                        "ratingImage": "rating_tvma.gif",
                        "season": 5,
                        "price": 500,
                        "display_end_date": 1419839999,
                        "image_pv": "",
                        "vdrmPlayLink": "getconnected.southwestwifi.com/vod/10482321/std/index.m3u8",
                        "vdrmContentId": "10482321",
                        "ratingInt": 8,
                        "seasonInfo2": "Season 5, Episode 7 | ",
                        "seasonInfo1": "Season 5 | Episode 7",
                        "fileTitle": "",
                        "routeList": [
                            "*-*"
                        ],
                        "formattedPrice": "$5",
                        "jsTitle": "The Bringer Show",
                        "summary": "Andre is taking a stand up class. Jenny joins a women's investment club. Pete will play Ruxin with two open slots on his team - the ultimate act of fantasy disrespect."
                    }
                ],
                "summary": "Fantasy football provides an outlet for good-natured competition and camaraderie between friends and colleagues, but that’s not always the case. Deception, one-upmanship, and win-at-all-costs mentality rule the day on the virtual gridiron and then extend into personal relationships, marriages and the workplace.",
                "episodeIds": [
                    "1530",
                    "1531",
                    "1532"
                ],
                "release_date": ""
            }
        ]
    }
}''')
    return data.get('videoData')
