# -*- coding: utf-8 -*-
### data.py :: module interface to access data ###

from xmlparsing import getProjectSettings

def get_project_settings():
    """ Gets the project-wide settings.
    """
    return xmlparsing.getProjectSettings()

def get_flight_data():
    units = {
        'deg_F': u'°F',
    }
    data = {
        'fltnum':'N123',
        'diata':'',
        'tlos':'',
        'ttgs': '',
        'ocity': '',
        'dcity': '',
        'ttgc': '2h 43mn',
        'forecast': '87%s at LAX' % units['deg_F'],
    }
    return the_flight_data

def get_session_info(request):
    return ('TEST_SESSION_ID', '192.168.0.0', '00:00:00:00:00:00')

def get_device_info(request):
    # Laptop
    return ('laptop', 'unkmobile')

    # iPad
    #return ('tablet', 'ipad')

    # iPhone
    #return ('mobile', 'iphone')

def get_faq():
    data = [
                {'title': '''I can't get my internet to work at all, what do I do?''',
                'content':'Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.' },
                {'title': '''I can't get my internet to work at all, what do lorem ipsum dolor?''',
                'content':'Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.' },
                {'title': '''Ullaces evelecusamus reptatu riones minctem peruptur?''',
                'content':'Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.' },
				{'title': '''Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit laboreris?''',
                'content':'Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.' },
				{'title': '''Ullaces evelecusamus reptatu riones minctem peruptur?''',
                'content':'Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.' },


    ]
    return data


def get_home_carousel_data():
    data = [
            {'image_source': '/images/img2.jpg',
             'image_description': 'Welcome Aboard!',
             'box': {'view_text': True,
                     'image_source':'/images/img3.png',
                     'image_description': 'description 3',
                     'text': 'DOZENS OF WAYS TO MAKE TIME FLY!',
                     'sub_text': 'Welcome Aboard!',
                     'sub_text_link': '',
                    }
            },
            {'image_source': '/images/img4.jpg',
             'image_description': 'description 4',
             'box': {'view_more': True,
                     'more': 'View Details',
                     'more_link': '/movies',
                    }
            }
            ]
    return data


def get_movies_featured_data():
    data = [
            {
             'title': 'Life of Pi', 'release_date': '2012-06-19', 'release_year':'2012',
             'image_lg': 'images/img13.jpg',
            },
            {
             'title': 'Pride & Prejudice', 'release_date': '2011-06-19', 'release_year':'2011',
             'image_lg': 'images/img14.jpg',
            },
            {
             'title': 'Couples Retreat', 'release_date': '2013-06-19', 'release_year':'2013',
             'image_lg': 'images/img15.jpg',
            },
            {
             'title': 'Up', 'release_date': '2008-06-19', 'release_year':'2008',
             'image_lg': 'images/img16.jpg',
            },
        ]
    return data

def get_movie_data():
    data = [
            {
            'title': 'Anna Karenina', 'year': '2002',
            'image_source_small': 'img37.jpg', 'image_description_small': 'image description',
            },
            {
            'title': 'Life of Pi', 'year': '2013',
            'image_source_small': 'img39.jpg', 'image_description_small': 'image description',
            },
            {
            'title': 'Up', 'year': '2009',
            'image_source_small': 'img40.jpg', 'image_description_small': 'image description',
            'image_source_large': 'img52.jpg', 'image_description_large': 'image description',
            'image_source_medium': 'img53.jpg', 'image_description_medium': 'image description',
            'mpaa_rating': 'PG',
            'star_rating': '3',
            'duration': '146',
            'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway.',
            },
            {
            'title': 'Diary of a Wimpy Kid', 'year': '2012',
            'image_source_small': 'img41.jpg', 'image_description_small': 'image description',
            },
            {
            'title': 'Gladiator', 'year': '2010',
            'image_source_small': 'img42.jpg', 'image_description_small': 'image description',
            },
            {
            'image_source_small': 'img43.jpg', 'image_description_small': 'image description',
            'title': 'Gnomeo &amp; Juliet', 'year': '2012',
            },
            {
            'title': 'The Little Mermaid', 'year': '1983',
            'image_source_small': 'img44.jpg', 'image_description_small': 'image description',
            },
            {
            'title': 'The Little Mermaid', 'year': '2010',
            'image_source_small': 'img45.jpg', 'image_description_small': 'image description',
            },
            {
            'title': 'Garfield The Movie', 'year': '1999',
            'image_source_small': 'img46.jpg', 'image_description_small': 'image description',
            },
            {
            'title': 'Mary Poppins', 'year': '1985',
            'image_source_small': 'img47.jpg', 'image_description_small': 'image description',
            },
            {
            'title': 'Bee', 'year': '2011',
            'image_source_small': 'img48.jpg', 'image_description_small': 'image description',
            },
            {
            'title': 'Aladdin', 'year': '1991',
            'image_source_small': 'img49.jpg', 'image_description_small': 'image description',
            },
            {
            'title': 'Hoot', 'year': '2013',
            'image_source_small': 'img50.jpg', 'image_description_small': 'image description',
            },
            {
            'title': 'Megamind', 'year': '2012',
            'image_source_small': 'img51.jpg', 'image_description_small': 'image description',
            },
        ]
    return data

def get_tv_shows_carousel_data():
    data = [
            {
             'title': 'The Office', 'sub_title': 'Season 5',
             'image_source': 'img21.jpg', 'image_description': 'image description',
            },
            {
             'title': '30 Rock', 'sub_title': 'Season 3-8',
             'image_source': 'img20.jpg', 'image_description': 'image description',
            },
            {
             'title': 'Parks and Recreation', 'sub_title': 'Season 2',
             'image_source': 'img22.jpg', 'image_description': 'image description',
            },
            {
             'title': 'Law & Order', 'sub_title': 'Season 5-7',
             'image_source': 'img23.jpg', 'image_description': 'image description',
            },
            {
             'title': '30 Rock', 'sub_title': 'Season 3-8',
             'image_source': 'img20.jpg', 'image_description': 'image description',
            },
            {
             'title': 'The Office', 'sub_title': 'Season 5',
             'image_source': 'img21.jpg', 'image_description': 'image description',
            },
            {
             'title': 'Parks and Recreation', 'sub_title': 'Season 2',
             'image_source': 'img22.jpg', 'image_description': 'image description',
            },
            {
             'title': 'Law & Order', 'sub_title': 'Season 5-7',
             'image_source': 'img23.jpg', 'image_description': 'image description',
            },
        ]
    return data


def get_movie_count():
    return 45

def get_shopping_data():
    data = [
            {
            'image_source': 'img63.jpg', 'image_description': 'image description',
            'sub-text': 'I Ship My Pants Onesie',
            },
            {
            'image_source': 'img56.jpg', 'image_description': 'image description',
            'title': 'Women',
            },
            {
            'image_source': 'img55.jpg', 'image_description': 'image description',
            'title': 'Men',
            },
            {
            'image_source': 'img58.jpg', 'image_description': 'image description',
            'title': 'Kids',
            },
            {
            'image_source': 'img57.jpg', 'image_description': 'image description',
            'title': 'Hats',
            },
            {
            'image_source': 'img62.jpg', 'image_description': 'image description',
            'title': 'Top Rated',
            },
            {
            'image_source': 'img61.jpg', 'image_description': 'image description',
            'title': 'Top Sellers',
            },
            {
            'image_source': 'img60.jpg', 'image_description': 'image description',
            'title': 'The Newest',
            },
            {
            'image_source': 'img59.jpg', 'image_description': 'image description',
            'title': 'T-Shirts',
            }
        ]
    return data

def get_ebay_data():
    data = [
            {
            'featured_item': True,
            'image_source': 'img72.jpg', 'image_description': 'image description',
            },
            {
            'image_source': 'img65.jpg', 'image_description': 'image description',
            'title': 'Brookstone',
            },
            {
            'image_source': 'img64.jpg', 'image_description': 'image description',
            'title': 'DeWalt',
            },
            {
            'image_source': 'img67.jpg', 'image_description': 'image description',
            'title': 'Under Armour',
            },
            {
            'image_source': 'img66.jpg', 'image_description': 'image description',
            'title': 'Eastern Mountain Sport',
            },
            {
            'image_source': 'img71.jpg', 'image_description': 'image description',
            'title': 'Timberland',
            },
            {
            'image_source': 'img70.jpg', 'image_description': 'image description',
            'title': 'Sony',
            },
            {
            'image_source': 'img69.jpg', 'image_description': 'image description',
            'title': 'Toys R Us',
            },
            {
            'image_source': 'img68.jpg', 'image_description': 'image description',
            'title': 'Calvin Klein',
            },
        ]
    return data

def get_fab_data():
    data = [
            {
            'image_source': 'img76.jpg', 'image_description': 'image description',
            'title': 'Smartphone Accessories',
            },
            {
            'image_source': 'img75.jpg', 'image_description': 'image description',
            'title': 'BKLN',
            },
            {
            'image_source': 'img74.jpg', 'image_description': 'image description',
            'title': 'Carved',
            },
            {
            'image_source': 'img73.jpg', 'image_description': 'image description',
            'title': 'The Southern Shop',
            },
        ]
    return data

def get_terminal_info(airport):
    data = [
            {
            'image_source': 'img26.jpg', 'image_description': 'image description',
            'name': 'All Terminals',
            },
            {
            'image_source': 'img27.jpg', 'image_description': 'image description',
            'name': 'Bradley International T...',
            },
            {
            'image_source': 'img28.jpg', 'image_description': 'image description',
            'name': 'Terminal 1',
            },
            {
            'image_source': 'img28.jpg', 'image_description': 'image description',
            'name': 'Terminal 1',
            },
            {
            'image_source': 'img29.jpg', 'image_description': 'image description',
            'name': 'Terminal 2',
            },
        ]
    return data

def get_destination():
    destination='LAX'
    return destination


def get_connecting_flights():
    data = [
            {
            'airport': 'Atlanta, GA', 'airport_code': 'ATL',
            'flight': '8674',
            'departure_time': '5:00 pm', 'status': 'ON TIME',
            'gate': 'D41',
            'terminal': '1',
            },
            {
            'airport': 'Austin, TX', 'airport_code': 'aus',
            'flight': '8674',
            'departure_time': '5:20 pm', 'status': 'DELAYED', 'delay_time': '1h 25m',
            'gate': 'E4',
            'terminal': '2',
            },
            {
            'airport': 'Baltimore/ Washington, MD', 'airport_code': 'bwi',
            'flight': '8674',
            'departure_time': '4:45 pm', 'status': 'ON TIME',
            'gate': 'd10',
            'terminal': '2',
            },
            {
            'airport': 'Boise, ID', 'airport_code': 'boi',
            'flight': '8674',
            'departure_time': '8:30 pm', 'status': 'DELAYED', 'delay_time': '1h 25m',
            'gate': 'd19',
            'terminal': '2',
            },
            {
            'airport': 'Greenville/ Spartanburg, SC', 'airport_code': 'gsp',
            'flight': '8674',
            'departure_time': '7:10 pm', 'status': 'ON TIME',
            'gate': 'e3',
            'terminal': '2',
            },
            {
            'airport': 'Portland, OR', 'airport_code': 'pdx',
            'flight': '8674',
            'departure_time': '5:00 pm', 'status': 'DELAYED', 'delay_time': '1h 25m',
            'gate': 'a17',
            'terminal': '1',
            },
            {
            'airport': 'St. Louis, MO', 'airport_code': 'stl',
            'flight': '8674',
            'departure_time': '5:00 pm', 'status': 'ON TIME',
            'gate': 'd2',
            'terminal': '2',
            },
            {
            'airport': 'West Palm Beach, FL', 'airport_code': 'pbi',
            'flight': '8674',
            'departure_time': '5:00 pm', 'status': 'DELAYED', 'delay_time': '1h 25m',
            'gate': 'd8',
            'terminal': '2',
            },
        ]
    return data
