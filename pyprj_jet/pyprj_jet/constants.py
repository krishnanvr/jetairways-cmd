# -*- coding: UTF-8 -*-

# The key in the .ini files for the project folder value.
PROJECT_FOLDER_KEY = 'portal.project_folder'

# The number of seconds before data expires in the RAM cache.
CACHE_SECONDS_DAY = 86400
CACHE_SECONDS_HALF_HOUR = 1800
CACHE_SECONDS_1_HOUR = 3600
CACHE_SECONDS_2_HOURS = 7200
CACHE_SECONDS_DAY = 86400

WIFI_PACKAGE_TYPE  = 'WIFI'
TV_PACKAGE_TYPE    = 'TV'
NFLRZ_PACKAGE_TYPE = 'NFLRZ'
MOVIE_PACKAGE_TYPE = 'MOVIE'
SMS_PACKAGE_TYPE   = 'SMS'

WIFI_PRODUCT_ID   = 'WIFI'
TV_PKG_PRODUCT_ID = 'TVPKG'
NFLRZ_PRODUCT_ID  = 'NFLRZ'
MOVIE_PRODUCT_ID  = 'MOVIE'
TV_PRODUCT_ID = 'TV'
SMS_PRODUCT_ID = 'SMS'

# Marker value to indicate that a number is null (i.e. it was not specified).
# This is used to differentiate between a value of 0 and a value that is null.
NUMBER_IS_NULL = -99999999

FLIGHT_TYPE_REAL = 'real'
FLIGHT_TYPE_TEST = 'test'

DEGREE_SIGN = u'\xb0'

