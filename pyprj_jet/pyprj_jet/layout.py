# -*- coding: utf-8 -*-

from pyramid.renderers import get_renderer
from pyramid.decorator import reify
from pyramid.i18n import get_locale_name

import client_utils.RAMStore as r44RAMStore
import client_utils.device as r44device

import time

import data
import video_mock
import constants
import logging
import pace
import features as r44features

log = logging.getLogger('pyprj_airside')


class Layout(object):

    @reify
    def global_template(self):
        renderer = get_renderer("templates/global_layout.pt")
        return renderer.implementation().macros['layout']


    @reify
    def global_macros(self):
        renderer = get_renderer("templates/wn_macros.pt")
        return renderer.implementation().macros


    @reify
    def global_mobile_template(self):
        renderer = get_renderer("mobile_templates/global_layout.pt")
        return renderer.implementation().macros['layout']


    @reify
    def global_mobile_template_modal(self):
        renderer = get_renderer("mobile_templates/global_layout_modal.pt")
        return renderer.implementation().macros['layout']


    @reify
    def global_mobile_template_modal_clean(self):
        renderer = get_renderer("mobile_templates/global_layout_modal_clean.pt")
        return renderer.implementation().macros['layout']


    @reify
    def global_mobile_macros(self):
        renderer = get_renderer("mobile_templates/wn_macros.pt")
        return renderer.implementation().macros

    @reify
    def get_wififaq_list(self):
         return data.get_wififaq(self)

    @reify
    def get_videofaq_list(self):
         return data.get_videofaq(self)

    @reify
    def get_wrapper_class(self):
        #log.debug('get_wrapper_class: view_name = %s' % self.request.view_name)
        if self.request.view_name == "flight_tracker":
            return "flight"
        if self.request.view_name in ("games", "games_detail"):
            return "games"
        if self.request.view_name in ("games", "games_play"):
            return "games"
        if self.request.view_name == "movies":
            return "movie"
        if self.request.view_name == "movie_details":
            return "movie_details"
        if self.request.view_name in ("epg", "epg_after"):
            return "epg"
        if self.request.view_name == "epg_vodtv_instant":
            return "epg_vodtv_instant"
        if self.request.view_name in ("purchase", "confirmation"):
            return "purchased"
        if self.request.view_name in ("tv", "tvcapacity"):
            return "tv"
        if self.request.view_name == "wifi":
            return "wifi"
        if self.request.view_name == "music":
            return "music"
        if self.request.view_name == "shopping":
            return "shopping"
        if self.request.view_name == "about":
            return "about"
        if self.request.view_name == "messaging":
            return "message"
        if self.request.view_name == "404":
            return "p404"
        if self.request.view_name == "connection":
            return "connection"
        if self.request.view_name == "destination":
            return "destination"
        if self.request.view_name == "surfing":
            return "surfing"
        if self.request.view_name == "information":
            return "information"
        if self.request.view_name == "feedback_confirmation":
            return "feedback_confirmation"
        if self.request.view_name == "ebooks":
            return "ebooks"
        return ""

    @reify
    def is_mobile_only(self):
        #return True # uncomment to force mobile view
        #return False # uncomment to force laptop view
        mob = (self.deviceCategory == r44device.DEVICE_CATEGORY_MOBILE)
        return self.deviceCategory == r44device.DEVICE_CATEGORY_MOBILE


    @reify
    def get_language(self):
        return get_locale_name(self.request)

    @reify
    def get_home_carousel_data(self):
        return data.get_home_carousel_data()

    @reify
    def get_movies_featured_data(self):
        return data.get_movies_featured_data()

    @reify
    def get_books_featured_data(self):
        return data.get_books_featured_data()

    @reify
    def get_news_featured_data(self):
        return data.get_news_featured_data()

    @reify
    def get_tv_shows_carousel_data(self):
        return data.get_tv_shows_carousel_data()

    @reify
    def get_games_carousel_data(self):
        return data.get_games_carousel_data()    

    @reify
    def get_tv_shows_featured_data(self):
        return data.get_tv_shows_featured_data()

    @reify
    def get_movies_count(self):
        return data.get_movies_count()

    @reify
    def get_books_count(self):
        return data.get_books_count()

    @reify
    def get_tv_counts(self):
        return data.get_tv_counts()

    @reify
    def get_movies_genres(self):
        if not self.isLaptopOnly and not self.isAndroid and not self.isIOS:
            # Do not show any movies for non-supported devices.
            return ['all']
        return data.get_movies_genres()

    @reify
    def get_games_genres(self):
        if not self.isLaptopOnly and not self.isAndroid and not self.isIOS:
            # Do not show any movies for non-supported devices.
            return ['all']
        return data.get_games_genres()

    @reify
    def get_movies_lengths(self):
        return data.get_movies_lengths()

    def get_movies_for_genre(self, genre):
        return data.get_movies_for_genre(genre)

    @reify
    def get_movies_data(self):
        if not self.isLaptopOnly and not self.isAndroid and not self.isIOS:
            # Do not show any movies for non-supported devices.
            return []
        return data.get_movies_data()

    @reify
    def get_iptv_data(self):
        return data.get_iptv_data()

    @reify
    def get_all_iptv_data(self):
        return data.get_all_iptv_data()
    
    @reify
    def get_vodtv_data(self):
        return data.get_vodtv_data()
        # TLG FOR TESTING
        # tv = data.get_vodtv_data()
        # bigData = tv + tv + tv + tv + tv + tv + tv + tv + tv
        # return bigData

    @reify
    def get_vodtv_vdrm_data(self):
        return data.get_vodtv_vdrm_data()

    @reify
    def get_tv_sponsor(self):
        return 'chase'

    @reify
    def is_tv_sponsor_dish(self):
        return self.get_tv_sponsor == 'dish'

    @reify
    def is_tv_sponsor_chase(self):
        return self.get_tv_sponsor == 'chase'

    @reify
    def is_tv_sponsor_swa(self):
        return self.get_tv_sponsor == 'swa'

    @reify
    def get_purchased_packages(self):
        return []

    @reify
    def get_movie_count(self):
        return data.get_movie_count()

    @reify
    def get_movie_data(self):
        return data.get_movie_data()

    @reify
    def get_movie_new_releases(self):
        return data.get_movie_data(genre='new')

    @reify
    def get_movie_kids_family(self):
        return data.get_movie_data(genre='kids_family')

    @reify
    def get_video_data(self):
        return video_mock.get_video_data()

    def is_video_authorized(self, video):
        return False

    @reify
    def get_wifi_pricing(self):
        pricing = (12,'12$',True)
        return pricing
        
    @reify
    def get_tv_pricing(self):
        pricing = (33,'33$',True)
        return pricing

    @reify
    def get_nflrz_pricing(self):
        pricing = (44,'44$',True)
        return pricing

    @reify
    def get_movie_pricing(self):
        pricing = (55,'55$',True)
        return pricing

    @reify
    def get_faq_list(self):
        return data.get_faq()

    @reify
    def get_flight_data(self):
        return data.get_flight_data()

    @reify
    def get_shopping_data(self):
        return data.get_shopping_data()

    @reify
    def get_top_products_data(self):
        return data.get_top_products_data()

    @reify
    def get_fab_data(self):
        return data.get_fab_data()

    @reify
    def get_destination(self):
        return data.get_destination()

    @reify
    def get_terminal_info(self):
        airport = self.get_destination
        return data.get_terminal_info(airport)

    @reify
    def get_connecting_flights(self):
        return data.get_connecting_flights()

    @reify
    def show_footer(self):
        if self.request.view_name == "surfing":
            return False
        return True

    @reify
    def get_current_info(self):
        fd = self.get_flight_data

        info = {}

        info['current'] = int(time.time())

        # Get current flight data values.
        info['flight'] = fd.get('fltnum', '')
        info['fltleg'] = fd.get('fltleg', '')
        info['tail'] = fd.get('tailnum', '')
        info['dest_code'] = fd.get('diata', '')
        info['dest_city'] = fd.get('dcity', '')
        info['orig_code'] = fd.get('oiata', '')
        info['orig_city'] = fd.get('ocity', '')
        info['latitude'] = fd.get('lat', '')
        info['longitude'] = fd.get('lon', '')
        info['gspeed'] = fd.get('gspdVal', '')
        info['gspeed_m'] = fd.get('gspd', '')
        info['gspeed_k'] = fd.get('gspdm', '')
        info['altitude'] = fd.get('altVal', '')
        info['altitude_f'] = fd.get('alt', '')
        info['altitude_m'] = fd.get('altm', '')
        info['ttgc'] = fd.get('ttgc', '')
        info['eta'] = fd.get('eta', '')
        info['eta_ampm'] = fd.get('etad', '')
        info['eta_24'] = fd.get('eta24', '')
        info['heading'] = fd.get('headingVal', '')
        try:
            heading = float(info['heading'])
            if heading < 0:
                heading += 360
                info['heading'] = '%1.2f' % heading
            elif heading > 360:
                heading -= 360
                info['heading'] = '%1.2f' % heading
        except:
            pass

        # Location history.
        info['lochist'] = fd.get('lochist', None)

        # NOTE: ttg is mistakenly used as key for distance-remaining-miles.
        #       So until key fixed (and client side mapquest),
        #       Put distance-remain-miles here.
        info['ttg'] = fd.get('dist_remain', '0')

        # Get date departed (offgnd) and estimated date of arrival
        # (eta_utc) as timestamps.
        info['offgnd'] = fd.get('offgnd', 0)
        if info['offgnd'] < 0: info['offgnd'] = 0
        info['eta_utc'] = fd.get('etautc', 0)
        if info['eta_utc'] < 0: info['eta_utc'] = 0

        # Add weather info.
        info.update(self.get_current_weather)

        return info

    @reify
    def get_current_weather(self):
        key = "Current_Weather_Info"

        # If weather data is cached, return it.
        info = r44RAMStore.get(key)
        if info: return info

        info = {}

        fd = self.get_flight_data

        cityCode = fd.get('diata', '')

        dw = fd.get('dest_weather', {})

        cc = dw.get('current_conditions', {})

        # Get display for header in portal pages.
        tempC = cc.get('temp_c', '')
        tempF = cc.get('temp_f', '')
        if cityCode and tempC:
            info['currentC'] = '%s%sC at %s' % (str(tempC), constants.DEGREE_SIGN, cityCode)
        if cityCode and tempF:
            info['currentF'] = '%s%sF at %s' % (str(tempF), constants.DEGREE_SIGN, cityCode)

        # Add current and forecast data.
        info['wth_condition'] = cc.get('condition', '')
        info['wth_tempc'] = tempC
        info['wth_tempf'] = tempF
        info['wth_icon'] = cc.get('icon', '')

        for i in (1, 2, 3, 4):
            fc = dw.get('forecast_conditions_%d' % i, {})
            if fc:
                info['wth_fc%d_condition' % i] = fc.get('condition', '')
                info['wth_fc%d_dow' % i] = fc.get('day_of_week', '')
                info['wth_fc%d_highc' % i] = fc.get('high_c', '')
                info['wth_fc%d_highf' % i] = fc.get('high_f', '')
                info['wth_fc%d_lowc' % i] = fc.get('low_c', '')
                info['wth_fc%d_lowf' % i] = fc.get('low_f', '')
                info['wth_fc%d_icon' % i] = fc.get('icon', '')

        # Weather updates do not change very often, so cache results for a half hour.
        r44RAMStore.set(key, info, expireSeconds = constants.CACHE_SECONDS_HALF_HOUR)

        return info

    @reify
    def get_destination_forecast(self):
        info = data.get_sample_forecast()
        return info

    @reify
    def get_nlc(self):
        news = data.get_nlc()
        return news

    @reify
    def is_sms_enabled(self):
        if not self.isIOS and not self.isAndroid:
            return False

        smsType = self.project_settings.get('smsType', '').lower()
        if smsType not in ('imessage', 'all'): smsType = ''

        if smsType == 'imessage' and not self.isIOS:
            return False

        return smsType

    @reify
    def is_sms_enabled_paid(self):
        if not self.is_sms_enabled:
            return False

        return not data.isPackageCodeComplimentary(constants.SMS_PACKAGE_TYPE, self.project_settings, self.get_flight_data)

    @reify
    def is_sms_enabled_comp(self):
        if not self.is_sms_enabled:
            return False

        return data.isPackageCodeComplimentary(constants.SMS_PACKAGE_TYPE, self.project_settings, self.get_flight_data)

    @reify
    def is_epgad_enabled(self):
        return r44features.is_feature_enabled('epgad', False)

    @reify
    def is_roadblockad_enabled(self):
        return r44features.is_feature_enabled('roadblockad', False)

    @reify
    def is_arrConn_enabled(self):
        return r44features.is_feature_enabled('arrConn', False)

    @reify
    def is_redzone_enabled(self):
        return False

    @reify
    def is_destDeals_enabled(self):
        return r44features.is_feature_enabled('destDeals', False)

    @reify
    def get_wifi_pricing(self):
        return data.get_pricing(constants.WIFI_PRODUCT_ID, '')

    @reify
    def get_tv_pricing(self):
        return data.get_pricing(constants.TV_PRODUCT_ID, '')

    @reify
    def get_nflrz_pricing(self):
        return data.get_pricing(constants.NFLRZ_PRODUCT_ID, '')

    @reify
    def get_movie_pricing(self):
        return data.get_pricing(constants.MOVIE_PACKAGE_TYPE, '')

    @reify
    def get_sms_pricing(self):
        return data.get_pricing(constants.SMS_PRODUCT_ID, '')

    # Called from the home page to see which messaging image to display.
    @reify
    def get_homepage_tile_sms_image(self):
        priceInfo = self.get_sms_pricing
        if priceInfo[2]:
            # Service is complimentary
            if self.is_sms_enabled == 'imessage':
                return 'images/imessage_comp_372x268.jpg'
            else:
                return 'images/home_tile_messaging_free.jpg'
        if priceInfo[0] in (2, 200):
            if self.is_sms_enabled == 'imessage':
                return 'images/imessage_homepage.jpg'
            else:
                return 'images/home_tile_messaging_2.jpg'
        if priceInfo[0] in (3, 300):
            return 'images/home_tile_messaging_3.jpg'
        if priceInfo[0] in (4, 400):
            return 'images/home_tile_messaging_4.jpg'
        if priceInfo[0] in (5, 500):
            return 'images/home_tile_messaging_5.jpg'
        return ''

    @reify
    def isIOS(self):
        return self.deviceType in (r44device.DEVICE_TYPE_IPAD, r44device.DEVICE_TYPE_IPOD, r44device.DEVICE_TYPE_IPHONE)

    @reify
    def isAndroid(self):
        return self.deviceType == r44device.DEVICE_TYPE_ANDROID

    @reify
    def isSilk(self):
        if self.deviceType != r44device.DEVICE_TYPE_ANDROID:
            return False

        return (self.request.user_agent.lower().find('silk') >= 0)

    @reify
    def isBlackBerry(self):
        return self.deviceType == r44device.DEVICE_TYPE_BLACKBERRY

    @reify
    def isWindows(self):
        return self.deviceType == r44device.DEVICE_TYPE_WINDOWS

    @reify
    def isMac(self):
        return self.deviceType == r44device.DEVICE_TYPE_MAC


    @reify
    def isLaptopOnly(self):
        return self.deviceCategory == r44device.DEVICE_CATEGORY_LAPTOP


    @reify
    def isTabletOnly(self):
        return self.deviceCategory == r44device.DEVICE_CATEGORY_TABLET


    @reify
    def isWifiAuthorized(self):
        return False

    def get_ad_for_slot(self, slotName):
        return None
