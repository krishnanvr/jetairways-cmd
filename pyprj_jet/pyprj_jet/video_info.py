# -*- coding: UTF-8 -*-

import datetime, time
import logging
import pprint
import re
import threading
import traceback

import client_utils.RAMStore as r44RAMStore
import portal_svcs.clients_auth.auth_utils as auth_utils
from portal_svcs.clients_auth import CHANGE_MINUTES_PREFIX, EPG_PREFIX, PRICING_KEY
from portal_svcs.clients_auth import IPTV_TYPE, VODTV_TYPE, MOVIE_TYPE

import constants
import flight_data
import portal_utils
import shared
import xmlparsing

# Locks to make sure only one thread is updating data at a time.
LOCK1 = threading.Semaphore(1)
LOCK2 = threading.Semaphore(1)

MIN_START_DATE = time.mktime(datetime.datetime(1970, 1, 1, 0, 0, 0, 0).utctimetuple())
MAX_END_DATE = time.mktime(datetime.datetime(2100, 1, 1, 0, 0, 0, 0).utctimetuple())

log = logging.getLogger('pyprj_airside')


def getVideoPackagePricing(packageCode = constants.TV_PKG_PRODUCT_ID, currencyISO = ''):
    global LOCK1
    log.debug('HHHHHHHHHHHHH calling getVideoPackage Pricing %s, %s' % (packageCode, currencyISO))

    flightData = flight_data.getCurrentFlightData()
    projectSettings = xmlparsing.getProjectSettings()
    if not currencyISO:
        currencyISO = projectSettings.get('currencyDefaultISO')
    key = 'VideoPackagePricing_%s%s%s%s' % (packageCode, currencyISO, flightData.get('fltnum', ''), flightData.get('oicao', ''))

    # Try to get the price from the RAM cache.
    priceInfo = r44RAMStore.get(key)
    if priceInfo:
        log.debug('HHHHHHHHHHHHH returned priceInfo from cache 1: %s' % pprint.pformat(priceInfo))
        return priceInfo

    needUpdate = True

    # Acquire the lock.
    LOCK1.acquire()
    try:
        # Try to get the price from the RAM cache again, in case it was added while we
        # were waiting to acquire the lock.
        priceInfo = r44RAMStore.get(key)
        if priceInfo:
            log.debug('HHHHHHHHHHHHH returned priceInfo from cache 2: %s' % pprint.pformat(priceInfo))
            needUpdate = False
            return priceInfo

        if isPackageCodeComplimentary(projectSettings, packageCode):
            # Package is free.
            priceInfo = (0, portal_utils.formatPrice(0))
        else:
            # Get the price from the shared object.
            try:
                videoAuth = shared.getAuthenticationObject()
                pricing = videoAuth.getPricing(packageCode)
                log.debug('HHHHHHHHHHHHH getting pricing from videoAuth shared object: %s' % pprint.pformat(pricing))
                price = pricing.get('%s,%s' % (packageCode, currencyISO))

                if price:
                    priceInfo = (price, portal_utils.formatPrice(price, currencyISO))
            except Exception, exc2:
                log.warning("video.getVideoPackagePricing WARNING: %s" % traceback.format_exc())

            if not priceInfo:
                log.warning("getVideoPackagePricing WARNING: videoAuth shared object missing pricing for %s, using default." % packageCode)

                # Get the default price info.
                ci = portal_utils.getCurrencyInfo(currencyISO)
                if packageCode == constants.MOVIE_PRODUCT_ID:
                    defPrice = ci.get('currencyMovieDefaultPrice')
                    priceInfo = (defPrice, portal_utils.formatPrice(defPrice, currencyISO))
                else:
                    defPrice = ci.get('currencyTVDefaultPrice')
                    priceInfo = (defPrice, portal_utils.formatPrice(defPrice, currencyISO))
    except Exception, exc:
        log.warning("video.getVideoPackagePricing ERROR: %s" % traceback.format_exc())
        priceInfo = (-1, '?')
    finally:
        try:
            if needUpdate:
                # Cache the price info.
                r44RAMStore.set(key, priceInfo, expireSeconds = constants.CACHE_SECONDS_DAY)
        except Exception, exc2:
            log.warning('getVideoPackagePricing WARNING: Caching pricing info failed.')

        # Release the lock.
        LOCK1.release()

    log.debug('HHHHHHHHHHHHH returned non-cached priceInfo: %s' % pprint.pformat(priceInfo))
    return priceInfo

def isPackageCodeComplimentary(projectSettings, packageCode):
    key = 'complimentary' + packageCode.lower()
    comp = projectSettings.get(key, False)

    log.debug('video_info:isPackageCodeComplimentary(%s) is %s' % (packageCode, str(comp)))
    return comp

PROCESSED_VIDEO_DATA = {}

# TODO: Handle re-loading of data when a "changeMinutes" is crossed.
def getAllMovieVideoInfo(sortBy = 'default', genre = 'all'):
    global PROCESSED_VIDEO_DATA
    global LOCK2

    projectSettings = xmlparsing.getProjectSettings()

    # Acquire the lock.
    LOCK2.acquire()

    try:
        sortBy = sortBy.strip().lower()
        genre = genre.strip().lower()

        nowSeconds = time.time()
        nowDate = datetime.datetime.utcfromtimestamp(nowSeconds)
        nowMinute = nowDate.hour * 60 + nowDate.minute

        log.debug('XYZXYZ getAllMovieVideoInfo: sortBy = %s, genre = %s, date = %s' % (sortBy, genre, pprint.pformat(nowDate)))

        airlineAbbrev = projectSettings.get('airlineICAO').lower()

        flightData = flight_data.getCurrentFlightData()
        oicao = flightData.get('oicao', 'XXX').lower()
        oiata = flightData.get('oiata', 'XXX').lower()
        dicao = flightData.get('dicao', 'XXX').lower()
        diata = flightData.get('diata', 'XXX').lower()
        
        # See if the movie data has already been processed.
        key = 'getAllMovieVideoInfo_%s%s' % (sortBy, genre)

        videoInfo = PROCESSED_VIDEO_DATA.get(key, None)
        if videoInfo:
            log.debug('getAllMovieVideoInfo: Returning from cache:\n%s' % pprint.pformat(videoInfo))
            return videoInfo

        log.debug('getAllMovieVideoInfo: Movie data NOT found in cache.')

        genreDict = {}

        # Get the dictionary with all the video info.
        allVideoInfo = _getAllVideoInfo()

        # Get the list of movies and loop through them, seeing
        # if any should be discarded.
        videoIds = allVideoInfo.get(MOVIE_TYPE, [])
        realList = []
        featuredList = []
        videoDict = {}
        for id in videoIds:
            movieDict = allVideoInfo.get(id, {})
            if not movieDict:
                log.debug('getAllMovieVideoInfo Error: Could not load video dict for vodmov %s' % id)
                continue

            # Make sure there are valid URLs for the video.
            if not movieDict.get('hlsUrl', ''):
                log.debug('getAllMovieVideoInfo Error: No hlsUrl for vodmov %s' % id)
                continue
            if not movieDict.get('rtmpeUrl', ''):
                log.debug('getAllMovieVideoInfo Error: No rtmpeUrl for vodmov %s' % id)
                continue
            if not _validateGenre(genre, movieDict):
                log.debug('getAllMovieVideoInfo Error: Could not validate genre for vodmov %s' % id)
                continue

            if not _validateValues(nowDate, nowSeconds, airlineAbbrev, oicao, oiata, dicao, diata, movieDict):
                log.debug('getAllMovieVideoInfo Error: Could not validate values for vodmov %s' % id)
                continue

            for g in movieDict.get('genreList', []):
                if g and g != '*' and g.lower() != 'all':
                    genreDict[g] = g

            realList.append(movieDict)
            videoDict[id] = movieDict

            featuredDict = movieDict.get('featured', {})
            if featuredDict:
                log.debug('getAllMovieVideoInfo: Movie %s is featured: %s' % (id, pprint.pformat(featuredDict)))
                if _validateValues(nowDate, nowSeconds, airlineAbbrev, oicao, oiata, dicao, diata, featuredDict):
                    log.debug('getAllMovieVideoInfo: Adding movie %s to featured list' % id)
                    movieDict['featured_display_index'] = featuredDict['display_index']
                    featuredList.append(movieDict)

        # Sort the list.
        if sortBy == 'title':
            realList.sort(_compareVideoDictsByTitle)
        elif sortBy == 'rating':
            realList.sort(_compareVideoDictsByRating)
        else:
            realList.sort(_compareVideoDictsByDisplayIndex)

        # Sort the featured list.
        featuredList.sort(_compareVideoDictsByFeaturedDisplayIndex)

        # Create a sorted list of the genres.
        allGenresList = []
        for g in genreDict.keys():
            allGenresList.append(g)
        allGenresList.sort()

        #log.debug('getAllMovieVideoInfo: sortBy = %s, genre = %s, date = %s,\nvodmovList = %s' % (sortBy, genre, pprint.pformat(nowDate), pprint.pformat(realList)))

        videoInfo = {
            'all': realList,
            'allPages': [],
            'allGenresList': allGenresList,
            'featuredList': featuredList,
            'dict': videoDict,
        }

        # Break up the 'all' list into "pages" with 10 movies each.
        # This is to aid in the display on the 'movies' page.
        pageNum = 0
        keepGoing = True
        while keepGoing:
           start = pageNum * 10
           end = start + 10
           if end >= len(realList):
               end = len(realList)
               keepGoing = False

           videoInfo['allPages'].append(realList[start:end])

           pageNum = pageNum + 1

        log.debug('XYZXYZ getAllMovieVideoInfo: videoInfo = \n%s' % pprint.pformat(videoInfo))

        # Cache the video info.
        PROCESSED_VIDEO_DATA[key] = videoInfo

        return videoInfo
    except Exception, exc:
        log.warning("getAllMovieVideoInfo ERROR: %s" % traceback.format_exc())
        return {}
    finally:
        # Release the lock.
        LOCK2.release()

# TODO: Handle re-loading of data when a "changeMinutes" is crossed.
def getAllTVVideoInfo(sortBy = 'default', genre = 'all', packageCode = None, idsToIgnore = None):
    global PROCESSED_VIDEO_DATA
    global LOCK2

    projectSettings = xmlparsing.getProjectSettings()

    # Acquire the lock.
    LOCK2.acquire()

    try:
        sortBy = sortBy.strip().lower()
        genre = genre.strip().lower()

        if not packageCode: packageCode = ''
        packageCode = packageCode.strip().lower()

        nowSeconds = time.time()
        nowDate = datetime.datetime.utcfromtimestamp(nowSeconds)
        nowMinute = nowDate.hour * 60 + nowDate.minute

        log.debug('XYZXYZ getAllTVVideoInfo: sortBy = %s, genre = %s, packageCode = %s, date = %s' % (sortBy, genre, packageCode, pprint.pformat(nowDate)))

        airlineAbbrev = projectSettings.get('airlineICAO').lower()

        flightData = flight_data.getCurrentFlightData()
        oicao = flightData.get('oicao', 'XXX').lower()
        oiata = flightData.get('oiata', 'XXX').lower()
        dicao = flightData.get('dicao', 'XXX').lower()
        diata = flightData.get('diata', 'XXX').lower()
 
        # See if the tv data has already been processed.
        key = 'getAllTVVideoInfo_%s%s%s' % (sortBy, genre, packageCode)
        if idsToIgnore:
            key = key + '_ign'
            for id in idsToIgnore:
                key = key + str(id) + '_'

        videoInfo = PROCESSED_VIDEO_DATA.get(key, None)
        if videoInfo:
            log.debug('getAllTVVideoInfo: Returning from cache:\n%s' % pprint.pformat(videoInfo))
            return videoInfo

        log.debug('getAllMovieVideoInfo: TV data NOT found in cache.')

        genreDict = {}
        videoDict = {}
        featuredList = []

        # Get the dictionary with all the video info.
        allVideoInfo = _getAllVideoInfo()

        # Get the list of iptv channels and loop through them, seeing
        # if any should be discarded.
        videoIds = allVideoInfo.get(IPTV_TYPE, [])
        iptvList = []
        for id in videoIds:
            iptvDict = allVideoInfo.get(id, {})
            if not iptvDict:
                log.debug('getAllTVVideoInfo Error: Could not load video dict for iptv %s' % id)
                continue

            if idsToIgnore and id in idsToIgnore:
                log.debug('getAllTVVideoInfo: iptv %s in list of ids to ignore' % str(id))
                continue

            if not _validatePackageCode(packageCode, iptvDict):
                log.debug('getAllTVVideoInfo Error: Could not validate package %s code for iptv %s' % (packageCode, id))
                continue

            if not _validateGenre(genre, iptvDict):
                log.debug('getAllTVVideoInfo Error: Could not validate genre for iptv %s' % id)
                continue

            if not _validateValues(nowDate, nowSeconds, airlineAbbrev, oicao, oiata, dicao, diata, iptvDict):
                log.debug('getAllTVVideoInfo Error: Could not validate values for iptv %s' % id)
                continue

            for g in iptvDict.get('genreList', []):
                if g and g != '*' and g.lower() != 'all':
                    genreDict[g] = g

            iptvList.append(iptvDict)
            videoDict[id] = iptvDict

            featuredDict = iptvDict.get('featured', {})
            if featuredDict and _validateValues(nowDate, nowSeconds, airlineAbbrev, oicao, oiata, dicao, diata, featuredDict):
                iptvDict['featured_display_index'] = featuredDict['display_index']
                featuredList.append(iptvDict)

        # Get the list of tv shows and loop through them, seeing
        # if any should be discarded.
        videoIds = allVideoInfo.get(VODTV_TYPE, [])
        vodtvList = []
        for id in videoIds:
            tvshowDict = allVideoInfo.get(id, {})
            if not tvshowDict:
                log.debug('getAllTVVideoInfo Error: Could not load video dict for vodtv %s' % id)
                continue

            if idsToIgnore and id in idsToIgnore:
                log.debug('getAllTVVideoInfo: vodtv %s in list of ids to ignore' % str(id))
                continue

            if not _validatePackageCode(packageCode, tvshowDict):
                log.debug('getAllTVVideoInfo Error: Could not validate package code %s for vodtv %s' % (packageCode, id))
                continue

            if not _validateGenre(genre, tvshowDict):
                log.debug('getAllTVVideoInfo Error: Could not validate genre for vodtv %s' % id)
                continue

            episodes = []
            for eid in tvshowDict.get('episodeIds', []):
                # If there is no episode, skip this.
                episode = allVideoInfo.get(eid, {})
                if not episode:
                    log.debug('getAllTVVideoInfo Error: Could not load episode dict for episode %s, vodtv %s' % (eid, id))
                    continue

                if idsToIgnore and eid in idsToIgnore:
                    log.debug('getAllTVVideoInfo: episode %s in list of ids to ignore' % str(eid))
                    continue

                # Make sure there are valid URLs for the video.
                if not episode.get('hlsUrl', ''):
                    log.debug('getAllTVVideoInfo Error: No hlsUrl for episode %s, vodtv %s' % (eid, id))
                    continue
                if not episode.get('rtmpeUrl', ''):
                    log.debug('getAllTVVideoInfo Error: No rtmpeUrl for episode %s, vodtv %s' % (eid, id))
                    continue
                if not _validateValues(nowDate, nowSeconds, airlineAbbrev, oicao, oiata, dicao, diata, episode):
                    log.debug('getAllTVVideoInfo Error: Could not validate values for episode %s, vodtv %s' % (eid, id))
                    continue

                episodes.append(episode)
                videoDict[eid] = episode

            epLength = len(episodes)
            if epLength == 0:
                # No valid episodes were found, so do not include
                # this show.
                log.debug('getAllTVVideoInfo Error: No valid episodes for vodtv %s' % id)
                continue

            # Sort the episodes by season / episode number (latest first).
            episodes.sort(_compareVideoDictsByEpisode)

            tvshowDict['episodes'] = episodes

            # Break up the 'episodes' list into "pages" with 10 episodes each.
            # This is to aid in the display on the 'channel' page.
            #tvshowDict['episodePages'] = []
            #pageNum = 0
            #keepGoing = True
            #while keepGoing:
            #   start = pageNum * 10
            #   end = start + 10
            #   if end >= len(episodes):
            #       end = len(episodes)
            #       keepGoing = False
            #
            #   tvshowDict['episodePages'].append(episodes[start:end])
            #
            #   pageNum = pageNum + 1

            # To help the screen display, get the first page of episodes and
            # the number of pages.
            #if epLength > 10:
            #    tvshowDict['firstPage'] = episodes[0:10]
            #else:
            #    tvshowDict['firstPage'] = episodes

            #numPages = epLength / 10
            #if epLength % 10: numPages += 1
            #tvshowDict['pages'] = range(1, 1 + numPages)

            # Create a list of seasons/episodes for the dropdown box on
            # the 'channel' page.
            #seasonList = []
            #currentSeasonList = []
            #currentSeason = -1
            #for e in episodes:
            #    season = e['season']
            #    if season != currentSeason:
            #        currentSeason = season
            #        sd = { 'season'  : currentSeason, 
            #               'episodes': []
            #             }
            #        seasonList.append(sd)
            #        currentSeasonList = sd['episodes']
            #    sd = { 'episode': e['number'],
            #           'id'     : e['id'],
            #           'title'  : e['title'],
            #         }
            #    currentSeasonList.append(sd)
            #tvshowDict['seasons'] = seasonList

            for g in tvshowDict.get('genreList', []):
                if g and g != '*' and g.lower() != 'all':
                    genreDict[g] = g

            vodtvList.append(tvshowDict)
            videoDict[id] = tvshowDict

            featuredDict = tvshowDict.get('featured', {})
            if featuredDict and _validateValues(nowDate, nowSeconds, airlineAbbrev, oicao, oiata, dicao, diata, featuredDict):
                tvshowDict['featured_display_index'] = featuredDict['display_index']
                featuredList.append(tvshowDict)

        # Combine the two lists together.
        videoList = []
        if iptvList:
            videoList.extend(iptvList)
        if vodtvList:
            videoList.extend(vodtvList)

        # Sort the lists.
        if sortBy == 'title':
            iptvList.sort(_compareVideoDictsByTitle)
            vodtvList.sort(_compareVideoDictsByTitle)
            videoList.sort(_compareVideoDictsByTitle)
        else:
            iptvList.sort(_compareVideoDictsByDisplayIndex)
            vodtvList.sort(_compareVideoDictsByDisplayIndex)
            videoList.sort(_compareVideoDictsByDisplayIndex)

        # Sort the featured list.
        featuredList.sort(_compareVideoDictsByFeaturedDisplayIndex)

        # Create a sorted list of the genres.
        allGenresList = []
        for g in genreDict.keys():
            allGenresList.append(g)
        allGenresList.sort()

        log.debug('getAllTVVideoInfo: sortBy = %s, genre = %s, packageCode = %s, date = %s,\niptvList = %s\nvodtvList = %s' % (sortBy, genre, packageCode, pprint.pformat(nowDate), pprint.pformat(iptvList), pprint.pformat(vodtvList)))

        videoInfo = {
            'all': videoList,
            'iptv': iptvList,
            'vodtv': vodtvList,
            'firstPage': [],
            'pages': [],
            'iptvFirstPage': [],
            'iptvPages': [],
            'vodtvFirstPage': [],
            'vodtvPages': [],
            'dict': videoDict,
            'featuredList': featuredList,
            'allGenresList': allGenresList,
        }

        # To help the screen display, get the first page of channels and
        # the number of pages.
        videoLength = len(videoList)
        if videoLength > 0:
            if videoLength > 10:
                videoInfo['firstPage'] = videoList[0:10]
            else:
                videoInfo['firstPage'] = videoList

            numPages = videoLength / 10
            if videoLength % 10: numPages += 1
            videoInfo['pages'] = range(1, 1 + numPages)

        videoLength = len(iptvList)
        if videoLength > 0:
            if videoLength > 10:
                videoInfo['iptvFirstPage'] = iptvList[0:10]
            else:
                videoInfo['iptvFirstPage'] = iptvList

            numPages = videoLength / 10
            if videoLength % 10: numPages += 1
            videoInfo['iptvPages'] = range(1, 1 + numPages)

        videoLength = len(vodtvList)
        if videoLength > 0:
            if videoLength > 10:
                videoInfo['vodtvFirstPage'] = vodtvList[0:10]
            else:
                videoInfo['vodtvFirstPage'] = vodtvList

            numPages = videoLength / 10
            if videoLength % 10: numPages += 1
            videoInfo['vodtvPages'] = range(1, 1 + numPages)

        log.debug('XYZXYZ getAllTVVideoInfo videoInfo =\n%s' % pprint.pformat(videoInfo))

        # Cache the video info.
        PROCESSED_VIDEO_DATA[key] = videoInfo

        return videoInfo
    except Exception, exc:
        log.warning("getAllTVVideoInfo ERROR: %s" % traceback.format_exc())
        return {}
    finally:
        # Release the lock.
        LOCK2.release()


# ----------------------------------------- #

ALL_VIDEO_LAST_CACHED = -1
ALL_VIDEO_DATA = None

def _getAllVideoInfo():
    global ALL_VIDEO_LAST_CACHED
    global ALL_VIDEO_DATA

    try:
        lastUpdated = auth_utils.getLastUpdated()

        # TLG TODO
        #advideoEnabled = not ed.isFeatureDisabled('advideo')
        advideoEnabled = True

        # If the data we have is current, return now.
        if ALL_VIDEO_LAST_CACHED == lastUpdated and ALL_VIDEO_DATA:
            return ALL_VIDEO_DATA

        videoAuth = shared.getAuthenticationObject()
        log.debug('XYZXYZ video_info._getAllVideoInfo: loadProducts')
        allInfo = videoAuth.loadProducts()

        projectSettings = xmlparsing.getProjectSettings()
        currencyISO = projectSettings.get('currencyDefaultISO')

        ALL_VIDEO_DATA = {}

        # Save all the pricing, change minutes, and epg info.
        for key in allInfo.keys():
            if key == PRICING_KEY or \
               key.startswith(CHANGE_MINUTES_PREFIX) or \
               key.startswith(EPG_PREFIX):
                ALL_VIDEO_DATA[key] = allInfo.get(key, '')

        # Save the video info.
        for mediaType in (IPTV_TYPE, VODTV_TYPE, MOVIE_TYPE):
            videos = allInfo.get(mediaType, [])
            videoList = []

            # Get the pricing for this media type.
            if mediaType == MOVIE_TYPE:
                mediaPrice, formattedMediaPrice = getVideoPackagePricing(constants.MOVIE_PRODUCT_ID, currencyISO)
            else:  # mediaType == IPTV_TYPE or VODTV_TYPE
                mediaPrice, formattedMediaPrice = getVideoPackagePricing(constants.TV_PKG_PRODUCT_ID, currencyISO)

            for video in videos:
                id = video.get('id', '')
                if not id: continue

                # Videos are displayed in a special web page, so replace the URL
                # with the that page (with the original URL as a parameter).
                url = video.get('rtmpeUrl', '')
                if url:
                    adUrl = (advideoEnabled and video.get('rtmpePrerollUrl', '')) or ''
                    video['rtmpeUrl'] = _generateFullRtmpeUrl(url, adUrl, splashImage = video.get('image_pv', ''))
                url = video.get('hlsUrl', '')
                if url:
                    adUrl = (advideoEnabled and video.get('hlsPrerollUrl', '')) or ''
                    video['hlsUrl'] = _generateFullHlsUrl(url, adUrl, splashImage = video.get('image_pv', ''))

                # For use in Javascript.
                video['jsTitle'] = re.sub('\'', '\\\'', video.get('title', ''))

                # See if a price has been set for this particular asset. 
                price = video.get('price', -1)
                if price < 0:
                    # There is no price, so use the price for the media type.
                    video['price'] = mediaPrice
                    video['formattedPrice'] = formattedMediaPrice
                    video['currency'] = currencyISO
                else:
                    # There is a price, so save its formatted value.
                    currency = video.get('currency', currencyISO)
                    video['formattedPrice'] = portal_utils.formatPrice(price, currency)

                # Save the video info by its id.
                ALL_VIDEO_DATA[id] = video

                videoList.append(id)

                # If this asset has episodes, process them.
                for eid in video.get('episodeIds', []):
                    episode = allInfo.get(eid, None)
                    if not episode: continue

                    # Videos are displayed in a special web page, so replace the URL
                    # with the that page (with the original URL as a parameter).
                    url = episode.get('rtmpeUrl', '')
                    if url:
                        adUrl = (advideoEnabled and episode.get('rtmpePrerollUrl', '')) or ''
                        episode['rtmpeUrl'] = _generateFullRtmpeUrl(url, adUrl, splashImage = episode.get('image_pv', ''))
                    url = episode.get('hlsUrl', '')
                    if url:
                        adUrl = (advideoEnabled and episode.get('hlsPrerollUrl', '')) or ''
                        episode['hlsUrl'] = _generateFullHlsUrl(url, adUrl, splashImage = episode.get('image_pv', ''))

                    # Add other needed information to the episode.
                    _addCombinedInfo(episode, video)

                    # For use in Javascript.
                    episode['jsTitle'] = re.sub('\'', '\\\'', episode.get('title', ''))

                    # See if a price has been set for this particular asset. 
                    price = episode.get('price', -1)
                    if price < 0:
                        # There is no price, so use the price for the media type
                        episode['price'] = mediaPrice
                        episode['formattedPrice'] = formattedMediaPrice
                        episode['currency'] = currencyISO
                    else:
                        # There is a price, so save its formatted value.
                        currency = episode.get('currency', currencyISO)
                        episode['formattedPrice'] = portal_utils.formatPrice(price, currency)

                    # Save the episode info by its id.
                    ALL_VIDEO_DATA[eid] = episode

            # For this media type, save the list of ids.
            ALL_VIDEO_DATA[mediaType] = videoList

    except Exception, exc:
        log.warning("video._getAllVideoInfo ERROR: %s" % traceback.format_exc())

    log.debug('XYZXYZ ALL_VIDEO_DATA keys = %s' % pprint.pformat(ALL_VIDEO_DATA.keys()))
    #log.debug('XYZXYZ ALL_VIDEO_DATA = %s' % pprint.pformat(ALL_VIDEO_DATA))
    ALL_VIDEO_LAST_CACHED = lastUpdated

    return ALL_VIDEO_DATA

def _generateFullRtmpeUrl(videoUrl, adUrl, splashImage = '', autoplay = True, width = 512, height = 324):
    if not videoUrl and not adUrl: return ''
    if videoUrl.find('flv.secure.iframe.html') > 0: return videoUrl

    # Hard-code some values.
    playerSrc = '/shared/flv.secure.iframe.html?'
    volume = '75'
    if autoplay:
        ap = 'true'
    else:
        ap = 'false'

    url = playerSrc
    if videoUrl:
        url = url + 'url=%s&' % videoUrl
    if adUrl:
        url = url + 'adURL=%s&adType=preroll&' % adUrl
    if splashImage:
        url = url + 'poster=%s&' % splashImage
    url = url + 'width=%s&height=%s&volume=%s&autoplay=%s' % \
                (str(width), str(height), str(volume), ap)
    return url

def _generateFullHlsUrl(videoUrl, adUrl, splashImage = '', autoplay = True, width = 512, height = 324):
    if not videoUrl and not adUrl: return ''
    if videoUrl:            # This causes the iOS native player to be used, but also
        return videoUrl     # means an ad canot be played before the video.
    if videoUrl.find('vidResume2.html') > 0: return videoUrl

    # Hard-code some values.
    playerSrc = '/project_ext/vidResume2.html?'
    volume = '75'
    if autoplay:
        ap = 'true'
    else:
        ap = 'false'

    url = playerSrc
    if videoUrl:
        url = url + 'url=%s&' % videoUrl
    if adUrl:
        url = url + 'adURL=%s&adType=preroll&' % adUrl
    if splashImage:
        url = url + 'poster=%s&' % splashImage
    url = url + 'width=%s&height=%s&volume=%s&autoplay=%s' % \
                (str(width), str(height), str(volume), ap)
    return url

def _validatePackageCode(packageCode, itemDict):
    #log('SSSSS _validatePackageCode: packageCode = %s' % packageCode)
    if packageCode:
        packageCodeList = itemDict.get('packageCode', '').lower()
        #log('    SSSSS _validatePackageCode: packageCodeList %s' % packageCodeList)
        return (packageCode == packageCodeList)

    # No package code was defined, so return True.
    return True

def _validateGenre(genre, itemDict):
    if genre != 'all':
        # Make sure at least one genre value matches.
        genreList = itemDict.get('genreList', ['*'])
        ok = False
        for g in genreList:
            g = g.lower()
            if g == genre or g == '*' or g == 'all':
                ok = True
                break
        if not ok:
            log.debug('    EEEE skipping %s because of non-matching genre' % itemDict.get('id'))
            return False
    return True

def _validateValues(nowDate, nowSeconds, airlineAbbrev, oicao, oiata, dicao, diata, itemDict):
    #print '\nOOOOOOOOOOOOOOOOOOOOOOOOOO _validateValues nowSeconds = %s' % pprint.pformat(nowSeconds)
    # Make sure the start date of this item is not in the future.
    startDate = itemDict.get('display_start_date', MIN_START_DATE)
    #print '    OOOOOO startDate = %s' % pprint.pformat(startDate)
    if startDate > nowSeconds:
        log.debug('    EEEE skipping %s because start date in future' % itemDict.get('id'))
        return False

    # Make sure the end date of this item is not in the past.
    endDate = itemDict.get('display_end_date', MAX_END_DATE)
    #print '    OOOOOO endDate = %s' % pprint.pformat(endDate)
    if endDate < nowSeconds:
        log.debug('    EEEE skipping %s because end date in past' % itemDict.get('id'))
        return False

    # If available times were specified, make sure the current time is within one of the
    # available time ranges.
    availTimes = itemDict.get('availableTimes', None)
    if availTimes:
        #print 'DDDDDDDDDDDDD Available times: %s' % pprint.pformat(availTimes)
        timesList = availTimes.get(nowDate.weekday(), None)
        if not timesList:
            #print 'DDDDDDDDDDDDD No available times for weekday %d, return False' % nowDate.weekday()
            return False
        nowMinute = nowDate.hour * 60 + nowDate.minute
        #print 'DDDDDDDDDDDDD Now time = %02d:%02d, minutes = %d' % (nowDate.hour, nowDate.minute, nowMinute)
        foundMatch = False
        for timeEntry in timesList:
            if nowMinute >= timeEntry.get('start_minute', 1440) and nowMinute <= timeEntry.get('end_minute', -1):
                foundMatch = True
                break
        if not foundMatch:
            #print 'DDDDDDDDDDDDD No matching times found for weekday %d, return False' % nowDate.weekday()
            return False

    # Make sure at least one airline value matches.
    airlineList = itemDict.get('airlineList', ['*'])
    ok = False
    for a in airlineList:
        a = a.lower()
        if a in (airlineAbbrev, '*', 'all'):
            ok = True
            break
    if not ok:
        log.debug('    EEEE skipping %s because of non-matching airline' % itemDict.get('id'))
        return False

    # Make sure at least one route value matches.
    routeList = itemDict.get('routeList', ['*-*'])
    ok = False
    for route in routeList:
        parts = route.split('-')
        orig = parts[0].lower()
        dest = parts[1].lower()

        ok1 = (orig == oicao or orig == oiata or orig == '*' or orig == 'all')
        ok2 = (dest == dicao or dest == diata or dest == '*' or dest == 'all')
        if ok1 and ok2:
            ok = True
            break
    if not ok:
        log.debug('    EEEE skipping %s because of non-matching route' % itemDict.get('id'))
        return False

    return True

# Combines some of the data in an episode for easier viewing.
def _addCombinedInfo(episode, tvshow):
    pkgCode = tvshow.get('packageCode', '')
    episode['packageCode'] = pkgCode

    season = episode.get('season', 0)
    number = episode.get('number', 0)

    if season > 0 and number > 0:
        episode['seasonInfo1'] = 'Season %d | Episode %d' % (season, number)
        episode['seasonInfo2'] = 'Season %d, Episode %d | ' % (season, number)
    elif season > 0:
        episode['seasonInfo1'] = 'Season %d ' % season
        episode['seasonInfo2'] = 'Season %d | ' % season
    elif number > 0:
        episode['seasonInfo1'] = 'Episode %d ' % number
        episode['seasonInfo2'] = 'Episode %d | ' % number
    else:
        episode['seasonInfo1'] = ''
        episode['seasonInfo2'] = ''

    rating = episode.get('rating', '')
    dur = episode.get('duration', '')

    details = ''
    if rating:
        details = details + rating + ' | '
    if dur:
        details = details + dur + ' min' + ' | '
    if details:
        details = details[:-3]
    episode['details'] = details

def _compareVideoDictsByTitle(d1, d2):
    return cmp(d1['title'], d2['title'])

def _compareVideoDictsByRating(d1, d2):
    val = cmp(d1['ratingInt'], d2['ratingInt'])
    if val: return val

    return cmp(d1['title'], d2['title'])

def _compareVideoDictsByDisplayIndex(d1, d2):
    val = cmp(d1['display_index'], d2['display_index'])
    if val: return val

    return cmp(d1['title'], d2['title'])

def _compareVideoDictsByEpisode(d1, d2):
    # Multiply by -1 so array will be sorted last to first.
    val = -1 * cmp(d1.get('season', 0), d2.get('season', 0))
    if val: return val

    val = -1 * cmp(d1.get('number', 0), d2.get('number', 0))
    if val: return val

    return cmp(d1['title'], d2['title'])

def _compareVideoDictsByFeaturedDisplayIndex(d1, d2):
    val = cmp(d1['featured_display_index'], d2['featured_display_index'])
    if val: return val

    return cmp(d1['title'], d2['title'])

