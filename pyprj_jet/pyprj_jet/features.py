
def is_feature_enabled(feature , defaultValue = False):
    if feature ==  'epgad':
        return False

    if feature ==  'roadblockad':
        return False

    if feature ==  'arrConn':
        return False

    if feature ==  'destDeals':
        return False

    return defaultValue
