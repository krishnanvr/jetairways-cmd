# -*- coding: utf-8 -*-

import logging
import pprint

import constants
import xmlparsing

log = logging.getLogger('pyprj_airside')

def formatPrice(price, currency = ''):
    log.debug('portal_utils.formatPrice(%s, %s)' % (str(price), str(currency)))
    if price <= 0.0: price = 0

    currencyInfo = getCurrencyInfo(currency)
    if not currencyInfo:
        log.warning('pricing.formatPrice ERROR: Could not find currency to use.')
        return '?'

    currency = currencyInfo.get('currencyISO')
    if currency == 'USD' and price >= 100:
        # Price is in cents; convert to dollars.
        price = price / 100.0

    intPrice = int(price)
    if intPrice == price:
        format = currencyInfo.get('currencyFormatInt')
        return format % intPrice
    else:
        cents = int((price - intPrice) * 100)
        format = currencyInfo.get('currencyFormatFloat')
        retval = format % (intPrice, cents)
        if retval[-1] == '0' and currencyInfo.get('currencyFormatSTZ', False):
            # This takes something like '7,50' and makes it '7,5'.  It assumes
            # there will only be one 0 at the end, since if there were two, the
            # price would have been an integer and handled above anyway.
            retval = retval[:-1]
        return retval

def getCurrencyInfo(currency = ''):
    projectSettings = xmlparsing.getProjectSettings()

    # If no currency passed in or the default currency passed in, return the default currency info.
    if not currency or currency == projectSettings.get('currencyDefaultISO'):
        currencyInfo = projectSettings.get('currencyDefault', {})
        log.debug('getCurrencyInfo: Returning currencyDefault %s' % pprint.pformat(currencyInfo))
        return currencyInfo 

    key = 'CURRENCY_INFO_%s' % currency

    # If this currency has already been found, return it.
    currencyInfo = r44RAMStore.get(key)
    if currencyInfo:
        return currencyInfo

    currencyInfo = {}

    # Loop through the currency list until we find the one that
    # matches the name of the currency passed in.
    currencyList = projectSettings.get('currencyList', [])
    for info in currencyList:
        if info.get('currencyISO', '') == currency:
            currencyInfo = info
            break

    if currencyInfo:
        r44RAMStore.set(key, currencyInfo, expireSeconds = constants.CACHE_SECONDS_DAY)

    return currencyInfo

