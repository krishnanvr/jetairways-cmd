# -*- coding: UTF-8 -*-

import datetime
import time
import logging

log = logging.getLogger('pyprj_airside')

# Retrieves the value of the specified cookie, if it exists, from the
# HTTP request.  If there is no such cookie, or there is an error, an
# empty String is returned.
#
# Parameters:
#    request : The HTTP Request
#    cookieId: The name of the cookie value to retrieve (String)
#
# Returns: (String)
#    The value of the specified cookie; or
#    An empty String if specified cookie not set or there is an
#        invalid parameter
def getCookieValue(request, cookieId):

    if not request:
        log.warning('ERROR: cookies.getCookieValue: Called with no request.')
        return ''

    if not cookieId:
        log.warning('ERROR: cookies.getCookieValue: Called with no cookieId.')
        return ''

    value = ''

    try:
        value = request.cookies.get(str(cookieId), '')
        if value: log.debug('cookies.getCookieValue: cookie %s has value %s' % (str(cookieId), value))
    except Exception, exc:
        log.warning('ERROR: cookies.getCookieValue exception: %s' % str(exc))

    return str(value).strip()

# Sets the specified cookie to a specified value in the HTTP response.
#
# If the optional expireDays parameter is not passed (it is None), a
# session cookie is set, otherwise the cookie is set to expire in the
# number of days specified.  Fractions of days can be specified.  For
# example, 0.5 would be 12 hours.
#
# If the expireDays parameter is a negative number, the cookie is
# removed.
#
# Parameters:
#    response   : The HTTP Response
#    cookieId   : The name of the cookie value to retrieve (String)
#    cookieValue: The value to set the cookie (String)
#    expireDays : The number of days until the cookie expires (Float) -
#                 optional
#
# Returns: (Boolean)
#    True if cookie is set; or
#    False if there is an invalid parameter (cookie not set)
def setCookieValue(response, cookieId, cookieValue, expireDays = None):

    log.debug('cookies.setCookieValue: id = %s, value = %s, expireDays = %s ' % (cookieId, cookieValue, str(expireDays)))

    if not response:
        log.warning('ERROR: cookies.setCookieValue: Called with no response.')
        return False

    if not cookieId:
        log.warning('ERROR: cookies.setCookieValue: Called with no cookieId.')
        return False

    if not cookieValue:
        cookieValue = ''

    cookieId = str(cookieId)
    cookieValue = str(cookieValue)

    expires = None
    try:
        if expireDays:
            expires = datetime.datetime.utcnow() + datetime.timedelta(days = expireDays)

        # Set the cookie in the HTTP response.
        response.set_cookie(cookieId, cookieValue, path='/', expires = expires, overwrite = True)

        return True
    except Exception, exc:
        log.warning('ERROR: cookies.setCookieValue exception: %s' % str(exc))
        return False

