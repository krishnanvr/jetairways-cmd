# -*- coding: utf-8 -*-

from views import *
from pyramid.decorator import reify
import pace
import data

class PaceViews(Views):

    @view_config(renderer='templates/city-guide.pt', name='city-guide')
    def cityguide_view(self):
        if self.is_mobile_only: self.request.override_renderer = 'mobile_templates/city-guide.pt'
        diata = pace.get_diata(self)
        page_values = {
            'diata' : diata,
            'ad' : pace.get_dest_ad_data(diata),
            'city_guide' : pace.get_dest_city_guide_data(diata)
        }
        return page_values

    @view_config(renderer='mobile_templates/mobile_test.pt', name='mobile_test')
    def mobile_test_view(self):
        diata = pace.get_diata(self)
        page_values = {
            'diata' : diata,
            'ad' : pace.get_dest_ad_data(diata),
            'city_guide' : pace.get_dest_city_guide_data(diata)
        }
        return page_values

    @reify
    def get_diata(self):
        return pace.get_diata(self)

    @reify
    def get_test_flight_data(self):
        return data.get_test_flight_data()

    @reify
    def get_city_guide_data(self):
        return pace.get_city_guide_data()
        
    @reify
    def get_dest_ad_data(self):
        return pace.get_dest_ad_data()
