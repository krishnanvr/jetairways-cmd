import datetime, time, pytz
import os
import string
import threading
import types
import copy
import logging
import pprint
import sys, traceback

import xml.dom.minidom
from xml.dom.minidom import Node
from xml.dom.minidom import Text

import flight_data

try:
    import client_utils.RAMStore as r44RAMStore
    from portal_svcs.clients_auth import DEFAULT_TZ, UTC_TZ, HOUR_START
#    from skytown.site.browser import ed
    HAS_PYRAMID = True
except:
    DEFAULT_TZ = pytz.timezone('US/Central')
    UTC_TZ     = pytz.utc
    HOUR_START = 2
    HAS_PYRAMID = False

DEBUG = HAS_PYRAMID and False

# The ROOT_DIR is the physical location of the carousel data folder.
# The URL_DIR is the alias (defined in httpd.conf) for the folder.
ROOT_DIR_HOME = "/opt/portal/vendors/swa/home_carousel/"
URL_DIR_HOME  = "/swa/home_carousel"

ROOT_DIR_USED = ""
URL_DIR_USED = ""

DATA_FILE = 'data.xml'

NUMBER_IS_NULL = -999999

log = logging.getLogger('pyprj_airside')

# Lock to make sure only one thread is parsing at a time.
PARSING_LOCK = threading.Semaphore(1)

def getHomeCarouselList():
    global PARSING_LOCK

    log.debug('getHomeCarouselList')

    flightData = flight_data.getCurrentFlightData()
    destICAO = flightData.get('dicao', '')
    # TODO: If status change changes, need to invalidate cache
    #vfs = video_info.getVideoFeaturesStatuses(viewPage)

    # Acquire the lock.
    PARSING_LOCK.acquire()

    try:
        key = '%s_%s' % (ROOT_DIR_HOME, destICAO)
        items = r44RAMStore.get(key)
        if items:
            log.debug('   ** Returning carousel list from cache')
            return items

        (origItems, lastMod, maxDisplay) = _parseAndValidateData(ROOT_DIR_HOME, URL_DIR_HOME, destICAO)
        items = []
        count = 0
        for item in origItems:
            keepIt = True
            funcName = item.get('include_function', '')
            if funcName:
                try:
                    func = getattr(viewPage, funcName)
                    keepIt = func() 
                except:
                    keepIt = False
                    log.debug('   ** getDisplayList ERROR: Invalid "include function" %s specified.' % funcName)
            if not keepIt:
                log.debug('   ** Skipping item because of include function %s' % funcName)
                continue

            if not _validateDisableClass(item.get('disable_class', '')):
                log.debug('   ** Skipping item because of disable class')
                continue

            log.debug('   ** Keeping item because "include function" %s returned True' % funcName)

            link = item.get('link', '')
            if link.startswith('function:'):
                item = copy.copy(item)
                item['link'] = ''
                funcName = link[9:]
                log.debug('   ** link starts with function: name is %s' % funcName)
                if funcName:
                   try:
                       func = getattr(viewPage, funcName)
                       item['link'] = func() 
                   except:
                       log.debug('   ** getDisplayList ERROR: Invalid "link function" %s specified.' % funcName)
                #else: print('   XXXXXX no function name')

            # Append the item to the final item list.
            items.append(item)

            # If we have already reached the maximum number of
            # items, stop now.
            count += 1
            log.debug('   ** Added item, count = %d' % count)
            if count >= maxDisplay:
                log.debug('   ** Reached maxDisplay %d, break' % maxDisplay)
                break

        # Cache for 5 minutes.
        r44RAMStore.set(key, items, expireSeconds = 300)

        return items
    except Exception, exc:
        log.warning("getHomeCarouselList ERROR: %s" % traceback.format_exc())

        return []
    finally:
        # Release the lock.
        PARSING_LOCK.release()

##################################

def getLastUpdated(path):
    lastUpdated = 0

    if os.path.exists(path) and os.path.isfile(path):
        statbuf = os.stat(path)
        lastUpdated = statbuf.st_mtime

    return lastUpdated

# Given a node, returns the first child node with the specified name.
def getChildWithName(nodeOrDict, name):
    if not nodeOrDict or not name:
        return None

    if isinstance(nodeOrDict, dict):
        children = nodeOrDict.get(name, [])
        if len(children) >= 1:
            return children[0]
    else:
        for child in nodeOrDict.childNodes:
            if child.nodeName == name:
                return child

    return None

# Given a node, returns a dictionary where each key references a list of
# child nodes with that name.
def getChildrenDict(node):
    children = {}

    if not node:
        return children

    for child in node.childNodes:
        if child.nodeType == child.ELEMENT_NODE:
            childList = children.get(child.nodeName, None)
            if not childList:
                childList = []
                children[child.nodeName] = childList
            childList.append(child)

    return children

# Given a node or a dictionary (from calling getChildrenDict), returns
# a list of all child nodes with the specified name.
def getChildrenWithName(nodeOrDict, name):
    if not nodeOrDict or not name:
        return []

    if isinstance(nodeOrDict, dict):
        children = nodeOrDict.get(name, [])
    else:
        children = []
        for child in nodeOrDict.childNodes:
            if child.nodeName == name:
                children.append(child)

    return children

def getDataFromChildWithName(nodeOrDict, name, required = True):
    childNode = getChildWithName(nodeOrDict, name)
    data = (childNode and childNode.firstChild and childNode.firstChild.data) or ''
    data = data.strip()
    if required and not data:
        raise Exception('Required data for %s not found in getDataFromChildWithName.' % name)
    return data

def getDataOrDefaultFromChildWithName(nodeOrDict, name, defaultValue):
    childNode = getChildWithName(nodeOrDict, name)
    data = (childNode and childNode.firstChild and childNode.firstChild.data) or ''
    data = data.strip()
    if not data:
        return defaultValue

    return data

def getAssetPathFromChildWithName(nodeOrDict, name, required = True):
    global URL_DIR_USED

    asset = getDataFromChildWithName(nodeOrDict, name, required)
    if not asset:
        return ''

    if asset.startswith('/') or asset.startswith('http://') or asset.startswith('https://'):
        return asset

    return URL_DIR_USED + '/' + asset

def getLinkFromChildWithName(nodeOrDict, name, required = True):
    link = getDataFromChildWithName(nodeOrDict, name, required)
    if not link:
        return ''

    if link.startswith('/') or link.startswith('http://') or link.startswith('https://') or link.startswith('function:'):
        return link

    return '/%s' % link

def getDataFromNode(node, required = True):
    data = (node and node.firstChild and node.firstChild.data) or ''
    data = data.strip()
    if required and not data:
        if node:
            nodeName = node.nodeName
        else:
            nodeName = 'unknown'
        raise Exception('Required data not found in node %s.' % nodeName)
    return data

# Returns an integer.
def getIntFromNode(node, required = True):
    intStr = getDataFromNode(node, required)
    if not intStr:
        return NUMBER_IS_NULL
    return int(intStr)

# Returns a boolean value (True or False)
def getBooleanFromNode(node, required = True):
    booleanStr = getDataFromNode(node, required).strip().lower()
    if not booleanStr:
        return False

    # The string should be 'true' or 'false', but assume any word that
    # starts with 'f' is False and anything else is True.
    if booleanStr.startswith('f'):
        return False
    return True

# Returns the timestamp value for a String date.
# Handles format: mm/dd/yyyy
#    For example: 03/07/2012
def getTimestampFromDateNode(node, required = True):
    dateStr = getDataFromNode(node, required)
    if not dateStr: return None

    try:
        if DEBUG: log('getTimestampFromDateNode: %s' % dateStr)
        # Create "localized" datetime.  Note that HOUR_START is passed for the hour
        # since airlines can begin their days at any hour.
        dateParts = dateStr.split('/')
        dt = datetime.datetime(int(dateParts[2]), int(dateParts[0]), int(dateParts[1]), HOUR_START)
        dt = DEFAULT_TZ.localize(dt)
        if DEBUG: log('    localized date = %s' % dt.strftime('%Y-%m-%d %H:%M:%S %Z%z'))

        # Convert datetime to UTC.
        utcDt = UTC_TZ.normalize(dt.astimezone(UTC_TZ))
        if DEBUG: log('    utc date = %s' % utcDt.strftime('%Y-%m-%d %H:%M:%S %Z%z'))

        # Get and return the timestamp from the UTC datetime.
        seconds = time.mktime(utcDt.utctimetuple())
        if DEBUG: log('    timestamp = %d' % seconds)
        return seconds
    except Exception, exc:
        log.warning('carousel.getTimestampFromDateNode ERROR: %s' % traceback.format_exc())
        return None

##################################

def _validateAirline(airlineId):
    airlineId = airlineId.lower()

    if airlineId == properties.getSkytownAirlineAbbreviation().lower():
        return True

    if airlineId == properties.getSkytownAirlineIata().lower():
        return True

    return False

def _validateDates(startDate, endDate):
    nowSeconds = time.time()

    if startDate and startDate > nowSeconds:
        #print '_validateDates: startDate is in future'
        return False

    if endDate and nowSeconds > endDate:
        #print '_validateDates: endDate is in past'
        return False

    return True

def _validateDestination(destCity, destList):
    if not destList:
        return True

    return destCity.lower() in destList

def _validateTailNumber(tailNumber, tailList):
    if not tailList:
        return True

    return tailNumber.lower() in tailList

def _validateDisableClass(disableClassFilter):
    if disableClassFilter == '':
        return True

    # TODO: Create the ed class
    #return not ed.isFeatureDisabled(disableClassFilter, viewPage)
    return True

def _parseData(dataFile):
    log.debug('_parseData %s' % dataFile)

    maxDisplay = 0
    itemList = []

    try:
        # Parse the XML list file.
        doc = xml.dom.minidom.parse(dataFile)

        itemsNode = getChildWithName(doc, 'Items')
        if not itemsNode:
            if DEBUG: log('<Items> node missing from file %s.' % dataFile)
            return (maxDisplay, itemList)

        itemsNodeDict = getChildrenDict(itemsNode)

        # TODO: Make sure airline matches current airline.
        airline = getDataFromChildWithName(itemsNodeDict, 'Airline')
        log.debug('airline = %s' % airline)

        node = getChildWithName(itemsNodeDict, 'Max_Display')
        maxDisplay = getIntFromNode(node, True)
        log.debug('maxDisplay = %d' % maxDisplay)

        itemNodes = getChildrenWithName(itemsNodeDict, 'Item')
        for itemNode in itemNodes:
            itemDict = {}

            destFilter = getDataOrDefaultFromChildWithName(itemNode, 'Destination_Filter', '')
            if destFilter == '*': destFilter = ''
            if destFilter:
                itemDict['dest_filters'] = destFilter.replace(' ', '').lower().split(',')
                log.debug('   item destFilter = %s' % itemDict['dest_filters'])

            node = getChildWithName(itemNode, 'Start_Date')
            itemDict['start_date'] = getTimestampFromDateNode(node, False)
            log.debug('   item start_date = %s' % itemDict['start_date'])

            node = getChildWithName(itemNode, 'End_Date')
            itemDict['end_date'] = getTimestampFromDateNode(node, False)
            log.debug('   item end_date = %s' % itemDict['end_date'])

            itemDict['disable_class'] = getDataOrDefaultFromChildWithName(itemNode, 'Disable_Class', '')
            log.debug('   item disable_class = %s' % itemDict['disable_class'])

            itemDict['include_function'] = getDataOrDefaultFromChildWithName(itemNode, 'Include_Function', '')
            log.debug('   item include function = %s' % itemDict['include_function'])

            itemDict['image_source'] = getAssetPathFromChildWithName(itemNode, 'Image_Source')
            log.debug('   item image_source = %s' % itemDict['image_source'])

            itemDict['image_description'] = getDataOrDefaultFromChildWithName(itemNode, 'Image_Description', '')
            log.debug('   item image_description = %s' % itemDict['image_description'])

            boxNode = getChildWithName(itemNode, 'Box')
            if boxNode:
                boxDict = {}

                node = getChildWithName(boxNode, 'View_Text')
                boxDict['view_text'] = getBooleanFromNode(node, False)
                log.debug('   item view_text = %s' % str(boxDict['view_text']))

                boxDict['image_source'] = getAssetPathFromChildWithName(boxNode, 'Image_Source', False)
                log.debug('   item image_source = %s' % boxDict['image_source'])

                boxDict['image_description'] = getDataOrDefaultFromChildWithName(boxNode, 'Image_Description', '')
                log.debug('   item image_description = %s' % boxDict['image_description'])

                boxDict['text'] = getDataOrDefaultFromChildWithName(boxNode, 'Text', '')
                log.debug('   item text = %s' % boxDict['text'])

                boxDict['sub_text'] = getDataOrDefaultFromChildWithName(boxNode, 'Sub_Text', '')
                log.debug('   item sub_text = %s' % boxDict['sub_text'])

                boxDict['sub_text_link'] = getLinkFromChildWithName(boxNode, 'Sub_Text_Link', False)
                log.debug('   item sub_text_link = %s' % boxDict['sub_text_link'])

                boxDict['sub_text_target'] = getDataOrDefaultFromChildWithName(boxNode, 'Sub_Text_Target', '_self')
                log.debug('   item sub_text_target = %s' % boxDict['sub_text_target'])

                node = getChildWithName(boxNode, 'View_More')
                boxDict['view_more'] = getBooleanFromNode(node, False)
                log.debug('   item view_more = %s' % str(boxDict['view_more']))

                boxDict['more'] = getDataOrDefaultFromChildWithName(boxNode, 'More', '')
                log.debug('   item more = %s' % boxDict['more'])

                boxDict['more_link'] = getLinkFromChildWithName(boxNode, 'More_Link', False)
                log.debug('   item more_link = %s' % boxDict['more_link'])

                itemDict['box'] = boxDict

            itemList.append(itemDict)
    except Exception, exc:
        log.warning("_parseData ERROR: %s" % traceback.format_exc())
        maxDisplay = 0
        itemList = []

    return (maxDisplay, itemList)

def _parseAndValidateData(rootDir, urlDir, destICAO):
    global ROOT_DIR_USED
    global URL_DIR_USED

    log.debug('_parseAndValidateData(%s, %s, %s)' % (rootDir, urlDir, destICAO))

    try:
        ROOT_DIR_USED = rootDir
        URL_DIR_USED = urlDir

        dataFile = ROOT_DIR_USED + DATA_FILE
        if not getLastUpdated(dataFile):
            log.debug('XML data file %s does not exist' % dataFile)
            return ([], 0, 0)

        statbuf = os.stat(dataFile)
        lastModified = statbuf.st_mtime

        # TODO: If status change changes, need to invalidate cache
        #vfs = video_info.getVideoFeaturesStatuses(viewPage)

        key = '%s_%s_raw' % (dataFile, destICAO)
        carouselInfo = r44RAMStore.get(key)
        if carouselInfo:
            log.debug('** Found carousel info in cache with timestamp %d' % carouselInfo[1])
            if lastModified == carouselInfo[1]:
                log.debug('** Returning carousel info from cache')
                return carouselInfo

        maxDisplay, allItems = _parseData(dataFile)
        log.debug('** Not in cache -- allItems: %s' % pprint.pformat(allItems))

        itemsList = []
        for item in allItems:
            if not _validateDates(item.get('start_date', None), item.get('end_date', None)):
                log.debug('** Skipping item because of invalid date')
                continue

            if not _validateDestination(destICAO, item.get('dest_filters', [])):
                log.debug('** Skipping item because of destination')
                continue

            # Add the item to the list.
            itemsList.append(item)

        log.debug('** Final carousel list: %s' % pprint.pformat(itemsList))

        carouselInfo = (itemsList, lastModified, maxDisplay)

        # Cache for 8 hours.
        r44RAMStore.set(key, carouselInfo, expireSeconds = 8 * 3600)

        return carouselInfo
    except Exception, exc:
        log.warning("_parseAndValidateData ERROR: %s" % traceback.format_exc())

        return (0, 0)

##################################
if __name__ == '__main__':
    itemsList = _parseData(ROOT_DIR_HOME + DATA_FILE)
    pprint.pprint(itemsList)

