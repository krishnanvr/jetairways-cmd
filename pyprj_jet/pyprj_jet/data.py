# -*- coding: utf-8 -*-
### data.py :: module interface to access data ###

import client_utils.session as r44session
import client_utils.device as r44device

import carousel, flight_data, video_info
import xmlparsing
import constants

import pprint
import logging
log = logging.getLogger('pyprj_airside')


units={
        'deg_F': u'°F',
}

def get_project_settings():
    """ Gets the project-wide settings.
    """
    return xmlparsing.getProjectSettings()

def get_flight_data():
    return flight_data.getCurrentFlightData()

def get_session_info(request):
    return r44session.getSessionInfo(request)

def get_device_info(request):
    # Get the device category / type used for the request.
    deviceCategory = r44device.getDeviceCategory(request)
    deviceType = r44device.getDeviceType(request)
    return (deviceCategory, deviceType)

def get_test_flight_data():
    data={
        'fltnum':'N123',
        'diata':'LAX',
        'tlos':'',
        'ttgs': '',
        'ocity': 'MDW',
        'dcity': 'Los Angeles',
        'orig_code': 'MDW',
        'dest_code': 'LAX',
        'time_to_destination': '2h 43mn',
        'forecast': '87%s at LAX' % (units['deg_F'],),
        'purchased': ['1','2','3','4',],
    }       
    return data


def get_faq():
    data = [
                {'title': '''I can't get my internet to work at all, what do I do?''',
                'content':'Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.' },
                {'title': '''I can't get my internet to work at all, what do lorem ipsum dolor?''',
                'content':'Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.' },
                {'title': '''Ullaces evelecusamus reptatu riones minctem peruptur?''',
                'content':'Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.' },
				{'title': '''Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit laboreris?''',
                'content':'Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.' },
				{'title': '''Ullaces evelecusamus reptatu riones minctem peruptur?''',
                'content':'Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.' },
    ]
    return data


def get_home_carousel_data():
    data = carousel.getHomeCarouselList()
    return data


def get_movies_featured_data():
    #movies = video_info.getAllMovieVideoInfo()
    #data = movies.get('featuredList', ())
    data = get_movie_carousel_data()
    return data

def get_movies_for_genre(genre, lang = 'en'):
    #movies = video_info.getAllMovieVideoInfo(lang = lang, genre = genre)
    #data = movies.get('all', {})
    data = ALL_MOVIES
    #newList = []
    #for d in ALL_MOVIES:
     #     newList.append(d)
    return data
    

def get_movies_count():
    return len(get_movie_carousel_data())

def get_games_count():
    return len(get_games_carousel_data())

def get_movies_genres(lang = 'en'):
    data = ALL_GENRES
    return data

def get_games_genres(lang = 'en'):
    data = ALL_GAMES_GENRES
    return data

def get_movies_lengths():
    data = ('1-2 hours', '2+ hours')
    return data

def get_books_featured_data():
    data = get_book_carousel_data()
    return data

def get_books_count():
    return len(get_book_carousel_data())

def get_tv_dict():
    data = {}
    return data

def get_tv_shows_featured_data():
    data = get_tv_shows_carousel_data()
    return data

def get_tv_counts():
    return (0,0,0,0,)

def get_movie_carousel_data():
    data = [
            {'title': 'Spider Man 2', 'release_date': '2012-06-19', 'release_year':'2014',
             'image_lg': 'posters/movie01.jpg', 'image_description': 'image description',
            },
            {
             'title': 'Maleficient', 'release_date': '2011-06-19', 'release_year':'2014',
             'image_lg': 'posters/movie02.jpg', 'image_description': 'image description',
            },      
            {   
             'title': 'X-man - Days of Future Past', 'release_date': '2013-06-19', 'release_year':'2012',
             'image_lg': 'posters/movie03.jpg', 'image_description': 'image description',
            },
            {
             'title': 'The Fault in our Stars', 'release_date': '2008-06-19', 'release_year':'2002',
             'image_lg': 'posters/movie04.jpg', 'image_description': 'image description',
            },
            {'title': 'Life of Pi', 'release_date': '2012-06-19', 'release_year':'2012',
             'image_lg': 'img13.jpg', 'image_description': 'image description',
            },
            {
             'title': 'Pride & Prejudice', 'release_date': '2011-06-19', 'release_year':'2011',
             'image_lg': 'img14.jpg', 'image_description': 'image description',
            },      
            {   
             'title': 'Couples Retreat', 'release_date': '2013-06-19', 'release_year':'2013',
             'image_lg': 'img15.jpg', 'image_description': 'image description',
            },
            {
             'title': 'Up', 'release_date': '2008-06-19', 'release_year':'2008',
             'image_lg': 'img16.jpg', 'image_description': 'image description',
            },
        ]
    return data 


def get_games_carousel_data():
    data = [
            {
             'title': 'Clusterz', 'sub_title': 'Arcade','id': 1001,
             'image_source': 'game8.jpg', 'image_description': 'image description',
             'description' :'When a bolt of lightning sends you back to the days of King Arthur, you trade in your farmers hat for a suit of armor and set off on a journey to find the Fountain of Youth!1',
            },
            {
             'title': 'Fitz', 'sub_title': 'Board','id': 1002,
             'image_source': 'game9.jpg', 'image_description': 'image description',
              'description' :'When a bolt of lightning sends you back to the days of King Arthur, you trade in your farmers hat for a suit of armor and set off on a journey to find the Fountain of Youth!2',  
            },
            {
             'title': 'Hotel Mogul', 'sub_title': 'Time Management','id': 1003,
             'image_source': 'game10.jpg', 'image_description': 'image description',
             'description' :'When a bolt of lightning sends you back to the days of King Arthur, you trade in your farmers hat for a suit of armor and set off on a journey to find the Fountain of Youth3!',    
            },
            {
             'title': 'Mahjongg Artifacts 2', 'sub_title': 'Puzzle','id': 1004,
             'image_source': 'game6.jpg', 'image_description': 'image description',
             'description' :'When a bolt of lightning sends you back to the days of King Arthur, you trade in your farmers hat for a suit of armor and set off on a journey to find the Fountain of Youth!4',  
            },
            {
             'title': 'New Yankee 2', 'sub_title': 'Time Management','id': 1005,
             'image_source': 'game3.jpg', 'image_description': 'image description',
             'description' :'When a bolt of lightning sends you back to the days of King Arthur, you trade in your farmers hat for a suit of armor and set off on a journey to find the Fountain of Youth!5',
            },
            {
             'title': 'Space Monkey', 'sub_title': 'Logic','id': 1006,
             'image_source': 'game4.jpg', 'image_description': 'image description',
             'description' :'When a bolt of lightning sends you back to the days of King Arthur, you trade in your farmers hat for a suit of armor and set off on a journey to find the Fountain of Youth!6',    
            },
            {
             'title': 'The Treasures of Montezuma 3', 'sub_title': 'Puzzle','id': 1007,
             'image_source': 'game5.jpg', 'image_description': 'image description',
             'description' :'When a bolt of lightning sends you back to the days of King Arthur, you trade in your farmers hat for a suit of armor and set off on a journey to find the Fountain of Youth!7',      
            },
            {
             'title': 'Sudoku', 'sub_title': 'Logic','id': 1008,
             'image_source': 'img104.png', 'image_description': 'image description',
             'description' :'When a bolt of lightning sends you back to the days of King Arthur, you trade in your farmers hat for a suit of armor and set off on a journey to find the Fountain of Youth!8',     
            },
        ]
    return data

def get_book_carousel_data():
    data = [
            {'title': u'Pilgrim’s Wilderness', 'release_date': '2012-06-19', 'release_year':'2014', 'author':'Tom Kizzia',
             'image_lg': 'assets/posters/eBooks/1.jpg', 'image_description': 'image description',
            },
            {
             'title': u'In a Sunburned Country', 'release_date': '2011-06-19', 'release_year':'2014', 'author':'Bill Bryson',
             'image_lg': 'assets/posters/eBooks/2.jpg', 'image_description': 'image description',
            },      
            {   
             'title': u'The Longest Road', 'release_date': '2013-06-19', 'release_year':'2012', 'author':'Philip Caputo',
             'image_lg': 'assets/posters/eBooks/3.jpg', 'image_description': 'image description',
            },
            {
             'title': u'Little Princes', 'release_date': '2008-06-19', 'release_year':'2002', 'author':'Conor Grennan',
             'image_lg': 'assets/posters/eBooks/4.jpg', 'image_description': 'image description',
            },
            {'title': u'A Game of Thrones', 'release_date': '2012-06-19', 'release_year':'2012', 'author':'George R. R. Martin',
             'image_lg': 'assets/posters/eBooks/5.jpg', 'image_description': 'image description',
            },
            {
             'title': u'The Expats', 'release_date': '2011-06-19', 'release_year':'2011', 'author':'Chris Pavone',
             'image_lg': 'assets/posters/eBooks/6.jpg', 'image_description': 'image description',
            },      
            {   
             'title': u'World War Z', 'release_date': '2013-06-19', 'release_year':'2013', 'author':'Max Brooks',
             'image_lg': 'assets/posters/eBooks/7.jpg', 'image_description': 'image description',
            },
            {
             'title': u'The Cuckoo’s Calling', 'release_date': '2008-06-19', 'release_year':'2008', 'author':'Robert Gabraith',
             'image_lg': 'assets/posters/eBooks/8.jpg', 'image_description': 'image description',
            },
        ]
    return data 

def get_news_featured_data():
    data = [
            {
             'title': u'U.S. Stocks Pare Losses, Treasuries Erase Gains on Fed', 
             'language': 'English', 
             'channel': 'Bloomberg',
             'pubDate': '2015-03-31 11:00:00',
             'id': 100,
             'flags' : {
                'new': True
             },
             'image_lg': 'assets/posters/news/1.jpg', 'image_description': 'image description',
            },
            {
             'title': u'Bullard Says Fed Should Consider Delay in Ending QE (Transcript)', 
             'language': 'English', 
             'channel': 'Fox',
             'pubDate': '2015-03-31 11:00:00',
             'id': 101,
             'flags' : {
                'new': False
             },
             'image_lg': 'assets/posters/news/2.jpg', 'image_description': 'image description',
            },
            {   
             'title': u'U.S. 2-Year Yield Rises From 17-Month Low on Bullard QE Comment', 
             'language': 'English', 
             'channel': 'Headlines',
             'pubDate': '2015-03-31 11:00:00',
             'id': 102,
             'flags' : {
                'new': True
             },
             'image_lg': 'assets/posters/news/5.jpg', 'image_description': 'image description',
            },
            {
             'title': u'Bullard Says Fed Should Consider Delay in Ending QE', 
             'language': 'English', 
             'channel': 'Bloomberg',
             'pubDate': '2015-03-31 11:00:00',
             'id': 103,
             'flags' : {
                'new': False
             },
             'image_lg': 'assets/posters/news/6.jpg', 'image_description': 'image description',
            },
            {'title': u'Debt Spree Exposes Philippine Companies as World Outlook Dims', 
             'language': 'English', 
             'channel': 'Fox',
             'pubDate': '2015-03-31 11:00:00',
             'id': 104,
             'flags' : {
                'new': False
             },
             'image_lg': 'assets/posters/news/7.jpg', 'image_description': 'image description',
            },
            {
             'title': u'2014 Stanley Cup Finals, the Rangers bested the defending champions', 
             'language': 'English', 
             'channel': 'Bloomberg',
             'pubDate': '2015-03-31 11:00:00',
             'id': 106,
             'flags' : {
                'new': False
             },
             'image_lg': 'assets/posters/news/8.jpg', 'image_description': 'image description',
            },      
            {   
             'title': u'French-born Batum dons ‘Je Suis Charlie’ T-shirt before Blazers’ game', 
             'language': 'English', 
             'channel': 'Fox',
             'pubDate': '2015-03-31 11:00:00',
             'id': 107,
             'flags' : {
                'new': True
             },
             'image_lg': 'assets/posters/news/9.jpg', 'image_description': 'image description',
            },
            {
             'title': u'Former Dolphins running back swims 9 miles to shore after falling off boat', 
             'language': 'English', 
             'channel': 'Headlines',
             'pubDate': '2015-03-31 11:00:00',
             'id': 108,
             'flags' : {
                'new': False
             },
             'image_lg': 'assets/posters/news/10.jpg', 'image_description': 'image description',
            },
        ]
    return data

def get_movie_data(genre=all):
    data = [
            {
            'title': 'Anna Karenina', 'year': '2002',
            'image_source_small': 'img37.jpg', 'image_description_small': 'image description',
            'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway1.',
             'mpaa_rating': 'PG1',
             'duration': '146',
             'id':'1'
            },
            {
            'title': 'Life of Pi', 'year': '2013',
            'image_source_small': 'img39.jpg', 'image_description_small': 'image description',
            'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway2.',
             'mpaa_rating': 'PG2',
             'duration': '146',
              'id':'2'
            },
            {
            'title': 'Up', 'year': '2009',
            'image_source_small': 'img40.jpg', 'image_description_small': 'image description',
            'image_source_large': 'img52.jpg', 'image_description_large': 'image description',
            'image_source_medium': 'img53.jpg', 'image_description_medium': 'image description',
            'mpaa_rating': 'PG3',
            'star_rating': '3',
            'duration': '146',
            'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway3.',
             'id':'3'
            },
            {
            'title': 'Diary of a Wimpy Kid', 'year': '2012',
            'image_source_small': 'img41.jpg', 'image_description_small': 'image description',
            'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway4.',
             'mpaa_rating': 'PG4',
             'duration': '146',
             'id':'4'
            },
            {
            'title': 'Gladiator', 'year': '2010',
            'image_source_small': 'img42.jpg', 'image_description_small': 'image description',
            'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway5.',
             'mpaa_rating': 'PG5',
             'duration': '146',
             'id':'5'
    
            },
            {
            'image_source_small': 'img43.jpg', 'image_description_small': 'image description',
            'title': 'Gnomeo &amp; Juliet', 'year': '2012',
            'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway6.',
            'mpaa_rating': 'PG6',
            'duration': '146',
            'id':'6'
            },
            {
            'title': 'The Little Mermaid', 'year': '1983',
            'image_source_small': 'img44.jpg', 'image_description_small': 'image description',
             'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway7.',
            'mpaa_rating': 'PG7',
            'duration': '146',
            'id':'7'
            },
            {
            'title': '''Charlotte's Web''', 'year': '2010',
            'image_source_small': 'img45.jpg', 'image_description_small': 'image description',
             'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway8.',
            'mpaa_rating': 'PG8',
            'duration': '146',
            'id':'8'  
            },
            {
            'title': 'Garfield The Movie', 'year': '1999',
            'image_source_small': 'img46.jpg', 'image_description_small': 'image description',
            'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway9.',
            'mpaa_rating': 'PG9',
            'duration': '146',
            'id':'9'
            },
            {
            'title': 'Mary Poppins', 'year': '1985',
            'image_source_small': 'img47.jpg', 'image_description_small': 'image description',
           'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway10.',
            'mpaa_rating': 'PG10',
            'duration': '146',
              'id':'10'
 
            },
            {
            'title': 'Bee', 'year': '2011',
            'image_source_small': 'img48.jpg', 'image_description_small': 'image description',
            'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway11.',
            'mpaa_rating': 'PG11',
            'duration': '146',
              'id':'11'

            },
            {
            'title': 'Aladdin', 'year': '1991',
            'image_source_small': 'img49.jpg', 'image_description_small': 'image description',
            'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway12.',
            'mpaa_rating': 'PG12',
            'duration': '146',
               'id':'12'
            },
            {
            'title': 'Hoot', 'year': '2013',
            'image_source_small': 'img50.jpg', 'image_description_small': 'image description',
            'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway13.',
            'mpaa_rating': 'PG13',
            'duration': '146',
               'id':'13'

            },
            {
            'title': 'Megamind', 'year': '2012',
            'image_source_small': 'img51.jpg', 'image_description_small': 'image description',
            'description': 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway14.',
            'mpaa_rating': 'PG14',
            'duration': '146',     
               'id':'14'
            }
        ]

    #new_releases_titles=['Anna Karenina', 'Life of Pi', 'Up', 'Diary of a Wimpy Kid', 'Gladiator', 'Gnomeo &amp; Juliet']

    kids_family_titles=['The Little Mermaid','''Charlotte's Web''', 'Garfield The Movie', 'Mary Poppins', 'Bee', 'Aladdin', 'Hoot', 'Megamind']

    #if genre == 'new':
        #return  [ m for t in new_releases_titles for m in data if t==m['title'] ]

    if genre == 'kids_family':
        return  [ m for t in kids_family_titles for m in data if t==m['title'] ]
        
    #log.debug(data);
        
    return data

def get_tv_shows_carousel_data():
    data = [
            {
			 'title': 'Law & Order', 'sub_title': 'Season 5-7',
             'image_source': 'img23.jpg', 'image_description': 'Law & Order',
             'season': '5', 'episode': '2',
             'title_synopsis': 'the office', 'year':'2005', 'channel':'TV-PG', 'time':'41 min',
             'detail':'Andy quits his job to pursue his acting career full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship',
             'About':'About Law & Order','about_synopsis':'Andy quits his job to pursue his acting career full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship', 'cast':'Spencer Treat Clark, Carl Fredricksen David Hemmings, Charles Muntz Tommy Flanagan, Russell Sven-Ole Thorsen, Dug / Alpha Omid Djalili, Beta',
             'videoimage': '/images/assets/posters/news/1.jpg',
             'videolink': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/100.mp4'
            },
            {
             'title': 'Top chef', 'sub_title': 'Season 5',
             'image_source': 'img20.jpg', 'image_description': 'Top chef',
             'season': '[3,4,5,6,7,8]', 'episode': '5',
             'title_synopsis': '30 Rock', 'year':'2002', 'channel':'TV', 'time':'41 min',
             'detail':' full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship',
             'About':'About 30 Rock','about_synopsis':' full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship', 'cast':'Charles Muntz Tommy Flanagan, Russell Sven-Ole Thorsen, Dug / Alpha Omid Djalili, Beta',
             'videoimage': '/images/assets/posters/news/2.jpg',
             'videolink': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/101.mp4'
            },
            {
             'title': '30 Rock', 'sub_title': 'Season 5-7',
             'image_source': 'img22.jpg', 'image_description': '30 Rock',
             'season': '5', 'episode': '6',
             'title_synopsis': 'the office', 'year':'2003', 'channel':'PG', 'time':'41 min',
             'detail':' and Pam reignite the sparks in their relationship',
             'About':'About Parks and Recreation', 'about_synopsis':' and Pam reignite the sparks in their relationship', 'cast':' Russell Sven-Ole Thorsen, Dug / Alpha Omid Djalili, Beta',
             'videoimage': '/images/assets/posters/news/3.jpg',
             'videolink': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC1.mp4'
            },
            {
             'title': 'Parks and Recreation', 'sub_title': 'Season 5-7',
             'image_source': 'img23.jpg', 'image_description': 'Parks and Recreation',
             'season': '5', 'episode': '7',
             'title_synopsis': 'the office', 'year':'2004', 'channel':'TV-PG', 'time':'41 min',
             'detail':'Andy quits his job to pursue his acting career full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship',
             'About':'About Law & Order','about_synopsis':'Andy quits his job to pursue his acting career full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship', 'cast':' Dug / Alpha Omid Djalili, Beta',
             'videoimage': '/images/assets/posters/news/4.jpg',
             'videolink': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4'
            },
            {
				
			'title': 'The Office', 'sub_title': 'Season 5',
             'image_source': 'img21.jpg', 'image_description': 'The Office',
             'season': '5', 'episode': '9',
             'title_synopsis': 'the office', 'year':'2006', 'channel':'TV-PG', 'time':'41 min',
             'detail':'Andy quits his job to pursue his acting career full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship',
             'About':'About The Office','about_synopsis':'Andy quits his job to pursue his acting career full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship', 'cast':'Spencer Treat Clark, Carl Fredricksen David Hemmings, Charles Muntz Tommy Flanagan, Russell Sven-Ole Thorsen, Dug / Alpha Omid Djalili, Beta',
             'videoimage': '/images/assets/posters/news/1.jpg',
             'videolink': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/100.mp4'
            },
            {
             'title': 'Downtown Abbey', 'sub_title': 'Season 1-3',
             'image_source': 'img20.jpg', 'image_description': '30 Rock',
             'season': '5', 'episode': '8',
             'title_synopsis': 'the office', 'year':'2005', 'channel':'TV-PG', 'time':'41 min',
             'detail':'Andy quits his job to pursue his acting career full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship',
             'About':'About 30 Rock','about_synopsis':'Andy quits his job to pursue his acting career full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship', 'cast':'Spencer Treat Clark, Carl Fredricksen David Hemmings, Charles Muntz Tommy Flanagan, Russell Sven-Ole Thorsen, Dug / Alpha Omid Djalili, Beta',
             'videoimage': '/images/assets/posters/news/5.jpg',
             'videolink': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/100.mp4'
            },
            {
             'title': 'Parenthood', 'sub_title': 'Season 2',
             'image_source': 'img22.jpg', 'image_description': 'Parenthood',
             'season': '5', 'episode': '3',
             'title_synopsis': 'Parks and Recreation', 'year':'2007', 'channel':'TV-PG', 'time':'41 min',
             'detail':'Andy quits his job to pursue his acting career full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship',
             'About':'About Parks and Recreation','about_synopsis':'Andy quits his job to pursue his acting career full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship', 'cast':'Spencer Treat Clark, Carl Fredricksen David Hemmings, Charles Muntz Tommy Flanagan, Russell Sven-Ole Thorsen, Dug / Alpha Omid Djalili, Beta',
             'videoimage': '/images/assets/posters/news/1.jpg',
             'videolink': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/100.mp4'
            },
            {
             'title': 'House', 'sub_title': 'Season 1-3',
             'image_source': 'img23.jpg', 'image_description': 'House',
             'season': '5', 'episode': '1',
             'title_synopsis': 'the office', 'year':'2009', 'channel':'TV-PG', 'time':'41 min',
             'detail':'Andy quits his job to pursue his acting career full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship',
             'About':'About the office','about_synopsis':'Andy quits his job to pursue his acting career full time; Dwight finally realizes a lifelong ambition, and Jim and Pam reignite the sparks in their relationship', 'cast':'Spencer Treat Clark, Carl Fredricksen David Hemmings, Charles Muntz Tommy Flanagan, Russell Sven-Ole Thorsen, Dug / Alpha Omid Djalili, Beta',
             'videoimage': '/images/assets/posters/news/1.jpg',
             'videolink': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/100.mp4'

            },
        ]
    return data


def get_movie_count():
    #movies = video_info.getAllMovieVideoInfo()
    #data = movies.get('all', ())
    data = get_movie_data()
    return len(data)

def get_shopping_data():
    data = [
            {
            'featured_item': True,
            'image_source': 'assets/shopping_featured_item.jpg', 'image_description': 'image description',
            },
            {
            'image_source': 'assets/shopping_logo1.png', 'image_description': 'image description',
            'title': 'Apple', 'sub-title': '2 points | $1',
            },
            {
            'image_source': 'assets/shopping_logo2.png', 'image_description': 'image description',
            'title': 'Calvin Klein', 'sub-title': '2 points | $1',
            },
            {
            'image_source': 'assets/shopping_logo3.png', 'image_description': 'image description',
            'title': 'Mont Blanc', 'sub-title': '2 points | $1',
            },
            {
            'image_source': 'assets/shopping_logo4.png', 'image_description': 'image description',
            'title': 'Swarovski', 'sub-title': '2 points | $1',
            },
            {
            'image_source': 'assets/shopping_logo5.png', 'image_description': 'image description',
            'title': 'Walmart', 'sub-title': '2 points | $1',
            },
            {
            'image_source': 'assets/shopping_logo6.png', 'image_description': 'image description',
            'title': 'Beverly Hills Polo Club', 'sub-title': '2 points | $1',
            },
            {
            'image_source': 'assets/shopping_logo7.png', 'image_description': 'image description',
            'title': 'Sony', 'sub-title': '2 points | $1',
            },
            {
            'image_source': 'assets/shopping_logo8.png', 'image_description': 'image description',
            'title': 'Juicy Couture', 'sub-title': '2 points | $1',
            },
        ]
    return data

def get_top_products_data():
    data = [
            {
            'image_source': 'assets/shopping_product1.png', 'image_description': 'image description',
            'title': 'Apple iPad Mini 32GB', 'sub-title': 'Wi-Fi Tablet, Black',
            },
            {
            'image_source': 'assets/shopping_product2.png', 'image_description': 'image description',
            'title': 'Omega James Bond', 'sub-title': 'Limited Edition Watch',
            },
            {
            'image_source': 'assets/shopping_product3.png', 'image_description': 'image description',
            'title': 'Swarovski Hello Kitty', 'sub-title': 'Mini Pendant',
            },
            {
            'image_source': 'assets/shopping_product4.png', 'image_description': 'image description',
            'title': 'Furla Women\'s Purse', 'sub-title': '',
            },
        ]
    return data

def get_fab_data():
    data = [
            {
            'image_source': 'img76.jpg', 'image_description': 'image description',
            'title': 'Smartphone Accessories',
            },
            {
            'image_source': 'img75.jpg', 'image_description': 'image description',
            'title': 'BKLN',
            },
            {
            'image_source': 'img74.jpg', 'image_description': 'image description',
            'title': 'Carved',
            },
            {
            'image_source': 'img73.jpg', 'image_description': 'image description',
            'title': 'The Southern Shop',
            },
        ]
    return data

def get_terminal_info(airport):
    data = [
            {
            'image_source': 'img26.jpg', 'image_description': 'image description',
            'name': 'All Terminals',
            },
            {
            'image_source': 'img27.jpg', 'image_description': 'image description',
            'name': 'Bradley International T...',
            },
            {
            'image_source': 'img28.jpg', 'image_description': 'image description',
            'name': 'Terminal 1',
            },
            {
            'image_source': 'img28.jpg', 'image_description': 'image description',
            'name': 'Terminal 1',
				},
            {
            'image_source': 'img29.jpg', 'image_description': 'image description',
            'name': 'Terminal 2',
            },
        ]
    return data

def get_destination():
    destination='LAX'
    return destination


def get_connecting_flights():
    data = {'fltnum': '3919', 'status': 'ON TIME', 'est_arrival_datetime': '10:26 AM EST', 'code': 200, 'fetch_time': 1425422896, 'err': None, 'dest': 'KBUF', 'success': True, u'connections': [{'fltnum': u'SWA510', 'status': 'ON TIME', 'dest_city': 'Baltimore', 'filed_departure_datetime': '11:10 AM EST', 'est_departure_datetime': '11:20 AM EST', u'gate_orig': u'', u'destination': u'BWI', u'terminal_orig': u'', 'delay': '', u'filed_departuretime': 1425485400, u'estimated_departuretime': 1425486000}, {'fltnum': u'SWA1212', 'status': 'ON TIME', 'dest_city': 'Orlando, FL', 'filed_departure_datetime': '12:10 PM EST', 'est_departure_datetime': '12:20 PM EST', u'gate_orig': u'', u'destination': u'MCO', u'terminal_orig': u'', 'delay': '', u'filed_departuretime': 1425489000, u'estimated_departuretime': 1425489600}], 'delay': '', 'terminal_dest': u'', 'show_connecting_terminals': False, 'gate_dest': u'', 'filed_arrival_datetime': '10:13 AM EST'} 
    return data

def get_pricing(productId, currencyISO = ''):
    priceInfo = (8.0, u'$8', False)
    if productId == constants.SMS_PRODUCT_ID:
        priceInfo = (2.0, u'$2', False)
    return priceInfo

def get_wififaq(viewPage):
    WIFI_FAQ = [
        {
        'title': '''Ullaces evelecusamus reptatu riones minctem peruptur?''',
        'content':'''Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit  laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ  ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.'''
        },
        {
        'title': '''Udam remquat enecesequi audam, sanduntis molorem quatem veliquia sitaten ihilit laboreris?''',
        'content':'''Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit  laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ  ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.'''
        },
        {
        'title': '''Ullaces evelecusamus reptatu riones minctem peruptur?''',
        'content':'''Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit  laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ  ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.'''
        }
    ]
    return WIFI_FAQ

def get_videofaq(viewPage):
    VIDEO_FAQ = [
        {
        'title': '''Ullaces evelecusamus reptatu riones minctem peruptur?''',
        'content':'''Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit  laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ  ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.'''
        },
        {
        'title': '''Udam remquat enecesequi audam, sanduntis molorem quatem veliquia sitaten ihilit laboreris?''',
        'content':'''Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit  laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ  ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.'''
        },
        {
        'title': '''Ullaces evelecusamus reptatu riones minctem peruptur?''',
        'content':'''Ut event, ut latempere prepereptati repudis eumet laut alia cusam delescietur molorporem etur, conseque est, verferc imporae nimi, senihiliquis velesto doluptat. Udam remquat enecesequi audam, sanduntis molorem quatem a prae reptaec totatus molupta veliquia sitaten ihilit  laboreris re noneces ullectiorio beri bea es sitis estiam sim consequ  ibearunt quisi rernam ex expernam, as accum dolut ab idel illaut hitions enimincius dolor modigende con nobis quam ventem coneture ma doles exped magnat.'''
        }
    ]
    return VIDEO_FAQ

def get_nlc():
    news = [
        {
          "news": {
            "articles": [
              {
                'id': 100,
                'order': 1,
                'headline': u'''U.S. Stocks Pare Losses, Treasuries Erase Gains on Fed''',
                'description': u'''U.S. Stocks Pare Losses, Treasuries Erase Gains on Fed''',
                'pubDate': '2015-03-31 08:00:00',
                'channel': 300,
                'language': 'English',
                'image': '/images/assets/posters/news/1.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/100.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 101,
                'order': 2,
                'headline': u'''Bullard Says Fed Should Consider Delay in Ending QE (Transcript)''',
                'description': u'''Bullard Says Fed Should Consider Delay in Ending QE (Transcript)''',
                'pubDate': '2015-03-31 11:00:00',
                'channel': 301,
                'language': 'English',
                'image': '/images/assets/posters/news/2.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/101.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 102,
                'order': 1,
                'headline': u'''U.S. 2-Year Yield Rises From 17-Month Low on Bullard QE Comment''',
                'description': u'''U.S. 2-Year Yield Rises From 17-Month Low on Bullard QE Comment''',
                'pubDate': '2015-03-31 08:00:00',
                'channel': 302,
                'language': 'English',
                'image': '/images/assets/posters/news/5.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC1.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 103,
                'order': 3,
                'headline': u'''Bullard Says Fed Should Consider Delay in Ending QE''',
                'description': u'''Bullard Says Fed Should Consider Delay in Ending QE''',
                'pubDate': '2015-03-31 11:00:00',
                'channel': 300,
                'language': 'English',
                'image': '/images/assets/posters/news/6.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4',
                'flags': {
                  'home': True,
                  'ticker': False,
                  'new': True
                }
              },
              {
                'id': 104,
                'order': 4,
                'headline': u'''Debt Spree Exposes Philippine Companies as World Outlook Dims''',
                'description': u'''Debt Spree Exposes Philippine Companies as World Outlook Dims''',
                'pubDate': '2015-03-31 08:00:00',
                'channel': 301,
                'language': 'English',
                'image': '/images/assets/posters/news/7.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 105,
                'order': 5,
                'headline': u'''U.S. Stocks Pare Losses, Treasuries Erase Gains on Fed''',
                'description': u'''U.S. Stocks Pare Losses, Treasuries Erase Gains on Fed''',
                'pubDate': '2015-03-31 11:00:00',
                'channel': 302,
                'language': 'English',
                'image': '/images/assets/posters/news/8.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 106,
                'order': 6,
                'headline': u'''2014 Stanley Cup Finals, the Rangers bested the defending champions''',
                'description': u'''2014 Stanley Cup Finals, the Rangers bested the defending champions''',
                'pubDate': '2015-03-31 08:00:00',
                'channel': 300,
                'language': 'English',
                'image': '/images/assets/posters/news/9.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 107,
                'order': 7,
                'headline': u'''French-born Batum dons ‘Je Suis Charlie’ T-shirt before Blazers’ game''',
                'description': u'''French-born Batum dons ‘Je Suis Charlie’ T-shirt before Blazers’ game''',
                'pubDate': '2015-03-31 11:00:00',
                'channel': 301,
                'language': 'English',
                'image': '/images/assets/posters/news/10.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 108,
                'order': 8,
                'headline': u'''Former Dolphins running back swims 9 miles to shore after falling off boat''',
                'description': u'''Former Dolphins running back swims 9 miles to shore after falling off boat''',
                'pubDate': '2015-03-31 08:00:00',
                'channel': 302,
                'language': 'English',
                'image': '/images/assets/posters/news/11.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 109,
                'order': 9,
                'headline': u'''Ryan Newman unveils 2015 No. 31 Caterpillar Chevrolet look''',
                'description': u'''Ryan Newman unveils 2015 No. 31 Caterpillar Chevrolet look''',
                'pubDate': '2015-03-31 11:00:00',
                'channel': 300,
                'language': 'English',
                'image': '/images/assets/posters/news/12.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 110,
                'order': 10,
                'headline': u'''Debt Spree Exposes Philippine Companies as World Outlook Dims''',
                'description': u'''Debt Spree Exposes Philippine Companies as World Outlook Dims''',
                'pubDate': '2015-03-31 08:00:00',
                'channel': 301,
                'language': 'English',
                'image': '/images/assets/posters/news/13.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 111,
                'order': 11,
                'headline': u'''2014 Stanley Cup Finals, the Rangers bested the defending champions''',
                'description': u'''2014 Stanley Cup Finals, the Rangers bested the defending champions''',
                'pubDate': '2015-03-31 11:00:00',
                'channel': 302,
                'language': 'English',
                'image': '/images/assets/posters/news/14.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 112,
                'order': 11,
                'headline': u'''Amidst the hurry burry city life almost all of us need a vacation''',
                'description': u'''Amidst the hurry burry city life almost all of us need a vacation''',
                'pubDate': '2015-03-31 11:00:00',
                'channel': 300,
                'language': 'English',
                'image': '/images/assets/posters/news/15.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 113,
                'order': 11,
                'headline': u'''People’s Choice Awards 2015: This year’s winners list''',
                'description': u'''People’s Choice Awards 2015: This year’s winners list''',
                'pubDate': '2015-03-31 11:00:00',
                'channel': 301,
                'language': 'English',
                'image': '/images/assets/posters/news/16.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              },
              {
                'id': 114,
                'order': 11,
                'headline': u'''The Keystone XL pipeline is priority No. 1 for U.S. Congress''',
                'description': u'''The Keystone XL pipeline is priority No. 1 for U.S. Congress''',
                'pubDate': '2015-03-31 11:00:00',
                'channel': 302,
                'language': 'English',
                'image': '/images/assets/posters/news/17.jpg',
                'video': 'http://softdevelopment.s3.amazonaws.com/gee-portals/video/BBC2.mp4',
                'flags': {
                  'home': True,
                  'ticker': True,
                  'new': True
                }
              }
            ],
            'channels': [
              {
                'id': 300,
                'order': 1,
                'name': 'Bloomberg News',
                'images': {
                  'colorLogo': '/images/assets/tv_and_news/logo_news_2.png',
                  'monochromeLogo': '/images/assets/tv_and_news/logo_news_2.png'
                }
              },
              {
                'id': 301,
                'order': 2,
                'name': 'Fox Sports',
                'images': {
                  'colorLogo': '/images/assets/news_ticker/logo_1.png',
                  'monochromeLogo': '/images/assets/tv_and_news/logo_news_1.png'
                }
              },
              {
                'id': 302,
                'order': 3,
                'name': 'Headlines',
                'images': {
                  'colorLogo': '/images/assets/tv_and_news/logo_news_3.png',
                  'monochromeLogo': '/images/assets/tv_and_news/logo_news_3.png'
                }
              }
            ]
          }
        }
    ]
    return news

def get_sample_forecast():
    info = [{'code': u'119',
          'condition': u'Cloudy',
          'date': u'2015-02-24',
          u'day_icon': u'wsymbol_0003_white_cloud',
          'day_of_week': 'Tue',
          u'description': u'Cloudy',
          'high_c': u'-4',
          'high_f': u'25',
          u'image_gray': u'cloudy_gray.png',
          u'image_purple': u'cloudy_purple.png',
          u'image_white': u'cloudy_white.png',
          'low_c': u'-14',
          'low_f': u'7',
          'moonset': u'No moonset',
          u'night_icon': u'wsymbol_0004_black_low_cloud',
          'sunrise': u'06:34 AM',
          'sunset': u'05:35 PM'},
         {'code': u'119',
          'condition': u'Cloudy',
          'date': u'2015-02-25',
          u'day_icon': u'wsymbol_0003_white_cloud',
          'day_of_week': 'Wed',
          u'description': u'Cloudy',
          'high_c': u'-9',
          'high_f': u'16',
          u'image_gray': u'cloudy_gray.png',
          u'image_purple': u'cloudy_purple.png',
          u'image_white': u'cloudy_white.png',
          'low_c': u'-14',
          'low_f': u'7',
          'moonset': u'12:30 AM',
          u'night_icon': u'wsymbol_0004_black_low_cloud',
          'sunrise': u'06:32 AM',
          'sunset': u'05:37 PM'},
         {'code': u'326',
          'condition': u'Light snow',
          'date': u'2015-02-26',
          u'day_icon': u'wsymbol_0011_light_snow_showers',
          'day_of_week': 'Thu',
          u'description': u'Snow',
          'high_c': u'-9',
          'high_f': u'16',
          u'image_gray': u'snow_gray.png',
          u'image_purple': u'snow_purple.png',
          u'image_white': u'snow_white.png',
          'low_c': u'-21',
          'low_f': u'-6',
          'moonset': u'01:29 AM',
          u'night_icon': u'wsymbol_0027_light_snow_showers_night',
          'sunrise': u'06:31 AM',
          'sunset': u'05:38 PM'},
         {'code': u'113',
          'condition': u'Clear/Sunny',
          'date': u'2015-02-27',
          u'day_icon': u'wsymbol_0001_sunny',
          'day_of_week': 'Fri',
          u'description': u'Sunny',
          'high_c': u'-13',
          'high_f': u'9',
          u'image_gray': u'sunny_gray.png',
          u'image_purple': u'sunny_purple.png',
          u'image_white': u'sunny_white.png',
          'low_c': u'-20',
          'low_f': u'-5',
          'moonset': u'02:22 AM',
          u'night_icon': u'wsymbol_0008_clear_sky_night',
          'sunrise': u'06:29 AM',
          'sunset': u'05:39 PM'},
         {'code': u'113',
          'condition': u'Clear/Sunny',
          'date': u'2015-02-28',
          u'day_icon': u'wsymbol_0001_sunny',
          'day_of_week': 'Sat',
          u'description': u'Sunny',
          'high_c': u'-11',
          'high_f': u'12',
          u'image_gray': u'sunny_gray.png',
          u'image_purple': u'sunny_purple.png',
          u'image_white': u'sunny_white.png',
          'low_c': u'-21',
          'low_f': u'-5',
          'moonset': u'03:10 AM',
          u'night_icon': u'wsymbol_0008_clear_sky_night',
          'sunrise': u'06:28 AM',
          'sunset': u'05:40 PM'}]
    return info

ALL_GENRES = [   (u'New Releases', u'New Releases'),
    (u'Family', u'family'),
    (u'Drama', u'drama'),
    (u'Adventure', u'adventure'),
    (u'Animation', u'animation'),
    (u'Comedy', u'comedy'),
    (u'Action', u'action'),
    (u'Fantasy', u'fantasy'),
    (u'Musical', u'musical'),
    (u'Romance', u'romance'),
    (u'Science Fiction', u'sciencefiction'),
    (u'Thriller', u'thriller')]

ALL_GAMES_GENRES = [   (u'New Releases', u'New Releases'),
    (u'Board Games', u'boardgames'),
    (u'Casual Games', u'casualgames')]

ALL_MOVIES = [   {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'10867527',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Graham Annable, Anthony Stacchi',
        'display_end_date': 1435733999.0,
        'display_index': 2,
        'display_start_date': 1422259200.0,
        'duration': u'97',
        'featured': {   'airlineList': ['SWA'],
                        'display_end_date': 1435733999.0,
                        'display_index': 2,
                        'display_start_date': 1422259200.0,
                        'routeList': [u'*-*']},
        'featured_display_index': 2,
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all adventure action comedy',
        'genreList': [u'Adventure', u'Action', u'Comedy'],
        'genres': u'Adventure, Action, Comedy',
        'id': u'1583',
        'image_background': u'/videos/vodmov/1254/background.jpg',
        'image_laptopflyout': '',
        'image_mobileflyout': u'/videos/vodmov/1254/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/mobile/images/img-04.jpg',
        'jsTitle': u'The Boxtrolls',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/10867527/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG',
        'ratingImage': 'rating_pg.gif',
        'ratingInt': 2,
        'release_date': u'2014-09-26',
        'release_year': u'2014',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Ben Kingsley, Isaac Hempstead Wright, Elle Fanning, Dee Bradley Baker, Steve Blum',
        'studio': u'UNI',
        'summary': u'A young orphaned boy raised by underground cave-dwelling trash collectors tries to save his friends from an evil exterminator.',
        'title': u'The Boxtrolls',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'10867528',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Wes Ball',
        'display_end_date': 1435733999.0,
        'display_index': 3,
        'display_start_date': 1421654400.0,
        'duration': u'115',
        'featured': {   'airlineList': ['SWA'],
                        'display_end_date': 1435733999.0,
                        'display_index': 3,
                        'display_start_date': 1421654400.0,
                        'routeList': [u'*-*']},
        'featured_display_index': 3,
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all action thriller',
        'genreList': [u'Action', u'Thriller'],
        'genres': u'Action, Thriller',
        'id': u'1584',
        'image_background': u'/videos/vodmov/1255/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1255/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1255/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1255/title.jpg',
        'jsTitle': u'The Maze Runner',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/10867528/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG-13',
        'ratingImage': 'rating_pg13.gif',
        'ratingInt': 3,
        'release_date': u'2014-09-19',
        'release_year': u'2014',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u"Dylan O'Brien, Kaya Scodelario, Will Poulter, Thomas Brodie-Sangster, Aml Ameen",
        'studio': u'FOX',
        'summary': u'Thomas is deposited in a community of boys after his memory is erased; they\'re all trapped in a maze that will require him to join forces with fellow "runners" for a shot at escape.',
        'title': u'The Maze Runner',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'11206833',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Clyde Geronimi',
        'display_end_date': 1451635199.0,
        'display_index': 7,
        'display_start_date': 1423555200.0,
        'duration': u'75',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all adventure action family',
        'genreList': [u'Adventure', u'Action', u'Family'],
        'genres': u'Adventure, Action, Family',
        'id': u'1670',
        'image_background': u'/videos/vodmov/1263/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1263/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1263/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1263/title.jpg',
        'jsTitle': u'Sleeping Beauty',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/11206833/std/index.m3u8',
        'price': 500.0,
        'rating': u'G',
        'ratingImage': 'rating_g.gif',
        'ratingInt': 1,
        'release_date': u'1959-02-06',
        'release_year': u'1959',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Mary Costa, Bill Shirley, Eleanor Audley',
        'studio': u'BVP',
        'summary': u'After being snubbed by the royal family, a malevolent fairy places a curse on a princess which only a prince can break, along with the help of three good fairies.',
        'title': u'Sleeping Beauty',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en', 'zh'],
        'change_minutes': set([480]),
        'contentId': u'10482315',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Matt Reeves',
        'display_end_date': 1433141999.0,
        'display_index': 16,
        'display_start_date': 1420099200.0,
        'duration': u'130',
        'featured': {   'airlineList': ['SWA'],
                        'display_end_date': 1433141999.0,
                        'display_index': 10,
                        'display_start_date': 1420099200.0,
                        'routeList': [u'*-*']},
        'featured_display_index': 10,
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all action drama sciencefiction',
        'genreList': [u'Action', u'Drama', u'Science Fiction'],
        'genres': u'Action, Drama, Science Fiction',
        'id': u'1501',
        'image_background': u'/videos/vodmov/1235/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1235/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1235/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1235/title.jpg',
        'jsTitle': u'Dawn of the Planet of the Apes',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/10482315/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG-13',
        'ratingImage': 'rating_pg13.gif',
        'ratingInt': 3,
        'release_date': u'2014-07-11',
        'release_year': u'2014',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Gary Oldman, Keri Russell, Andy Serkis',
        'studio': u'FOX',
        'summary': u'10 Years after a disease that spread around the globe, Apes who have survived the disease go head-to-head with a group of human survivors and a traitor who is hell bent on destroying humans and apes, triggering the unbalanced war for peace.',
        'title': u'Dawn of the Planet of the Apes',
        'use_vdrm': True,
        'vodType': u'vdrm',
        'zh_director': u'\u9a6c\u7279\xb7\u91cc\u592b\u65af',
        'zh_duration': u'130',
        'zh_genreList': [u'\u52a8\u4f5c'],
        'zh_genres': u'\u52a8\u4f5c',
        'zh_image_background': '',
        'zh_image_laptopflyout': '',
        'zh_image_mobileflyout': '',
        'zh_image_pv': '',
        'zh_image_title': '',
        'zh_rating': u'PG-13',
        'zh_ratingImage': 'rating_pg13.gif',
        'zh_ratingInt': 3,
        'zh_release_date': u'2014-07-11',
        'zh_release_year': u'2014',
        'zh_stars': u'\u52a0\u91cc\xb7\u5965\u5fb7\u66fc\uff0c\u51ef\u4e3d\xb7\u62c9\u585e\u5c14\uff0c\u5b89\u8fea\xb7\u745f\u91d1\u65af',
        'zh_studio': u'FOX',
        'zh_summary': u'10\u5e74\u7684\u75be\u75c5\u904d\u5e03\u5168\u7403\uff0c\u733f\u8c01\u5e78\u5b58\u4e0b\u6765\u7684\u75be\u75c5\u662f\u5934\u5bf9\u5934\u4e0e\u4e00\u7fa4\u4eba\u7684\u751f\u8fd8\u8005\u548c\u53db\u5f92\u662f\u8c01\u4e00\u610f\u5b64\u884c\u6467\u6bc1\u4eba\u7c7b\u548c\u7c7b\u4eba\u733f\uff0c\u5f15\u53d1\u4e0d\u5e73\u8861\u6218\u4e89\u548c\u5e73\u4e4b\u540e\u3002',
        'zh_title': u'\u4eba\u733f\u661f\u7403\u7684\u9ece\u660e'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'9889655',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Bryan Singer',
        'display_end_date': 1427871599.0,
        'display_index': 39,
        'display_start_date': 1413270000.0,
        'duration': u'131',
        'featured': {   'airlineList': ['SWA'],
                        'display_end_date': 1427871599.0,
                        'display_index': 18,
                        'display_start_date': 1413270000.0,
                        'routeList': [u'*-*']},
        'featured_display_index': 18,
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all action',
        'genreList': [u'Action'],
        'genres': u'Action',
        'id': u'1346',
        'image_background': u'/videos/vodmov/1217/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1217/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1217/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1217/title.jpg',
        'jsTitle': u'X-Men: Days of Future Past',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/9889655/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG-13',
        'ratingImage': 'rating_pg13.gif',
        'ratingInt': 3,
        'release_date': u'2014-05-23',
        'release_year': u'2014',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Patrick Stewart, Ian McKellen, Hugh Jackman',
        'studio': u'FOX',
        'summary': u'The X-Men send Wolverine to the past in a desperate effort to change history and prevent an event that results in doom for both humans and mutants.',
        'title': u'X-Men: Days of Future Past',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'9889656',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Dean DeBlois',
        'display_end_date': 1427871599.0,
        'display_index': 41,
        'display_start_date': 1415692800.0,
        'duration': u'102',
        'featured': {   'airlineList': ['SWA'],
                        'display_end_date': 1427871599.0,
                        'display_index': 20,
                        'display_start_date': 1415692800.0,
                        'routeList': [u'*-*']},
        'featured_display_index': 20,
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all adventure action family',
        'genreList': [u'Adventure', u'Action', u'Family'],
        'genres': u'Adventure, Action, Family',
        'id': u'1368',
        'image_background': u'/videos/vodmov/1221/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1221/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1221/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1221/title.jpg',
        'jsTitle': u'How to Train Your Dragon 2',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/9889656/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG',
        'ratingImage': 'rating_pg.gif',
        'ratingInt': 2,
        'release_date': u'2014-06-13',
        'release_year': u'2014',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Jay Baruchel, Cate Blanchett, Gerard Butler',
        'studio': u'FOX',
        'summary': u'The thrilling second chapter of the epic How to Train Your Dragon trilogy returns to the fantastical world of the heroic Viking Hiccup and his faithful dragon Toothless. The inseparable duo must protect the peace \u2013 and save the future of men and dragons from the power-hungry Drago.',
        'title': u'How to Train Your Dragon 2',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['zh', 'en'],
        'change_minutes': set([480]),
        'contentId': u'7726758',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Gavin Hood',
        'display_end_date': 1430463599.0,
        'display_index': 59,
        'display_start_date': 1398927600.0,
        'duration': u'114',
        'featured': {   'airlineList': ['SWA'],
                        'display_end_date': 1430463599.0,
                        'display_index': 54,
                        'display_start_date': 1398927600.0,
                        'routeList': [u'*-*']},
        'featured_display_index': 54,
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all action sciencefiction',
        'genreList': [u'Action', u'Science Fiction'],
        'genres': u'Action, Science Fiction',
        'id': u'1180',
        'image_background': u'/videos/vodmov/1199/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1199/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1199/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1199/title.jpg',
        'jsTitle': u'Enders Game',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/7726758/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG-13',
        'ratingImage': 'rating_pg13.gif',
        'ratingInt': 3,
        'release_date': u'2013-10-25',
        'release_year': u'2013',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Harrison Ford, Asa Butterfield, Hailee Steinfeld',
        'studio': u'EIM',
        'summary': u'When hostile aliens called the Formics attack Earth, only the legendary heroics of Mazer Rackham manage to attain a victory. To prepare for the next attack, Col. Hyrum Graff and his cohorts initiate a military program to find the next Mazer.',
        'title': u'Enders Game',
        'use_vdrm': True,
        'vodType': u'vdrm',
        'zh_director': u'\u9a6c\u7279\xb7\u91cc\u592b\u65af',
        'zh_duration': u'130',
        'zh_genreList': [u'\u52a8\u4f5c'],
        'zh_genres': u'\u52a8\u4f5c',
        'zh_image_background': '',
        'zh_image_laptopflyout': '',
        'zh_image_mobileflyout': '',
        'zh_image_pv': '',
        'zh_image_title': '',
        'zh_rating': u'PG-13',
        'zh_ratingImage': 'rating_pg13.gif',
        'zh_ratingInt': 3,
        'zh_release_date': u'2014-07-11',
        'zh_release_year': u'2014',
        'zh_stars': u'\u52a0\u91cc\xb7\u5965\u5fb7\u66fc\uff0c\u51ef\u4e3d\xb7\u62c9\u585e\u5c14\uff0c\u5b89\u8fea\xb7\u745f\u91d1\u65af',
        'zh_studio': u'FOX',
        'zh_summary': u'10\u5e74\u7684\u75be\u75c5\u904d\u5e03\u5168\u7403\uff0c\u733f\u8c01\u5e78\u5b58\u4e0b\u6765\u7684\u75be\u75c5\u662f\u5934\u5bf9\u5934\u4e0e\u4e00\u7fa4\u4eba\u7684\u751f\u8fd8\u8005\u548c\u53db\u5f92\u662f\u8c01\u4e00\u610f\u5b64\u884c\u6467\u6bc1\u4eba\u7c7b\u548c\u7c7b\u4eba\u733f\uff0c\u5f15\u53d1\u4e0d\u5e73\u8861\u6218\u4e89\u548c\u5e73\u4e4b\u540e\u3002',
        'zh_title': u'\u4eba\u733f\u661f\u7403\u7684\u9ece\u660e'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'8286766',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Tim Story',
        'display_end_date': 1430463599.0,
        'display_index': 60,
        'display_start_date': 1401606000.0,
        'duration': u'100',
        'featured': {   'airlineList': ['SWA'],
                        'display_end_date': 1430463599.0,
                        'display_index': 49,
                        'display_start_date': 1402556400.0,
                        'routeList': [u'*-*']},
        'featured_display_index': 49,
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all action comedy',
        'genreList': [u'Action', u'Comedy'],
        'genres': u'Action, Comedy',
        'id': u'1183',
        'image_background': u'/videos/vodmov/1202/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1202/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1202/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1202/title.jpg',
        'jsTitle': u'Ride Along',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/8286766/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG-13',
        'ratingImage': 'rating_pg13.gif',
        'ratingInt': 3,
        'release_date': u'2014-01-17',
        'release_year': u'2014',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Ice Cube, Kevin Hart, John Leguizamo',
        'studio': u'UNI',
        'summary': u"For two years, security guard Ben has tried to convince James, a veteran cop, that he is worthy of James' sister, Angela. When Ben is finally accepted into the police academy, James decides to test his mettle by inviting him along on a shift deliberately designed to scare the trainee.",
        'title': u'Ride Along',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'8286764',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Dean Parisot',
        'display_end_date': 1430463599.0,
        'display_index': 61,
        'display_start_date': 1401606000.0,
        'duration': u'116',
        'featured': {   'airlineList': ['SWA'],
                        'display_end_date': 1430463599.0,
                        'display_index': 47,
                        'display_start_date': 1402556400.0,
                        'routeList': [u'*-*']},
        'featured_display_index': 47,
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all action comedy',
        'genreList': [u'Action', u'Comedy'],
        'genres': u'Action, Comedy',
        'id': u'1181',
        'image_background': u'/videos/vodmov/1200/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1200/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1200/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1200/title.jpg',
        'jsTitle': u'Red 2',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/8286764/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG-13',
        'ratingImage': 'rating_pg13.gif',
        'ratingInt': 3,
        'release_date': u'2013-07-18',
        'release_year': u'2013',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Bruce Willis, John Malkovich, Mary-Louise Parker',
        'studio': u'EIM',
        'summary': u'Former CIA black-ops agent Frank Moses and his team battle assassins, terrorists and power-hungry government officials as they try to retrieve a lethal device that could change the balance of world power.',
        'title': u'Red 2',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'7634966',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Martin Campbell',
        'display_end_date': 1427871599.0,
        'display_index': 65,
        'display_start_date': 1395990000.0,
        'duration': u'145',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all adventure action',
        'genreList': [u'Adventure', u'Action'],
        'genres': u'Adventure, Action',
        'id': u'887',
        'image_background': u'/videos/vodmov/1179/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1179/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1179/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1179/title.jpg',
        'jsTitle': u'Casino Royale',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/7634966/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG-13',
        'ratingImage': 'rating_pg13.gif',
        'ratingInt': 3,
        'release_date': u'2006-11-17',
        'release_year': u'2006',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Daniel Craig, Judy Dench, Eva Green, Mads Mikkelsen',
        'studio': u'Terry_Steiner_International',
        'summary': u'After receiving a license to kill, British Secret Service agent James Bond heads to Madagascar, where he uncovers a link to Le Chiffre, a man who finances terrorist organizations. Learning that Le Chiffre plans to raise money in a high-stakes poker game, MI6 sends Bond to play against him, gambling that their newest "00" operative will topple the man\'s organization.',
        'title': u'Casino Royale',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'7634960',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Paul Greengrass',
        'display_end_date': 1435733999.0,
        'display_index': 70,
        'display_start_date': 1395990000.0,
        'duration': u'115',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all action drama',
        'genreList': [u'Action', u'Drama'],
        'genres': u'Action, Drama',
        'id': u'819',
        'image_background': u'/videos/vodmov/1159/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1159/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1159/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1159/title.jpg',
        'jsTitle': u'The Bourne Ultimatum',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/7634960/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG-13',
        'ratingImage': 'rating_pg13.gif',
        'ratingInt': 3,
        'release_date': u'2007-08-03',
        'release_year': u'2007',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Matt Damon, Joan Allen, Julia Stiles, Scott Glenn',
        'studio': u'UNI',
        'summary': u'Jason Bourne dodges a ruthless CIA official and his agents from a new assassination program while searching for the origins of his life as a trained killer.',
        'title': u'The Bourne Ultimatum',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'7275996',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Doug Liman',
        'display_end_date': 1435733999.0,
        'display_index': 79,
        'display_start_date': 1395990000.0,
        'duration': u'118',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all action thriller',
        'genreList': [u'Action', u'Thriller'],
        'genres': u'Action, Thriller',
        'id': u'79',
        'image_background': u'/videos/vodmov/1057/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1057/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1057/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1057/title.jpg',
        'jsTitle': u'The Bourne Identity',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/7275996/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG-13',
        'ratingImage': 'rating_pg13.gif',
        'ratingInt': 3,
        'release_date': u'2002-06-04',
        'release_year': u'2002',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Matt Damon, Chris Cooper, Franka Potente, Julia Stiles, Clive Owen',
        'studio': u'UNI',
        'summary': u'A blockbuster hit, The Bourne Identity stars Academy Award winner Matt Damon as a man fished out of the ocean by an Italian boat who has a complete loss of memory. Why are assassins after him? Why is he a master of martial arts and fluent in several languages? The answers will thrill and astonish you! Featuring amazing action scenes, taut direction, and beautiful European locations, The Bourne Identity is the opening introduction to a hugely popular action espionage franchise.',
        'title': u'The Bourne Identity',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'7275997',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Paul Greengrass',
        'display_end_date': 1435733999.0,
        'display_index': 84,
        'display_start_date': 1395990000.0,
        'duration': u'109',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all action thriller',
        'genreList': [u'Action', u'Thriller'],
        'genres': u'Action, Thriller',
        'id': u'81',
        'image_background': u'/videos/vodmov/1058/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1058/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1058/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1058/title.jpg',
        'jsTitle': u'The Bourne Supremacy',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/7275997/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG-13',
        'ratingImage': 'rating_pg13.gif',
        'ratingInt': 3,
        'release_date': u'2004-07-23',
        'release_year': u'2004',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Matt Damon, Joan Allen, Julia Stiles, Franka Potente, Brian Cox, Karl Urban',
        'studio': u'UNI',
        'summary': u'When the Chinese vice-premier is brutally murdered, the CIA starts a manhunt for their prime suspect - Jason Bourne! Still haunted by fragmented memories of his past, Jason (Oscar-winner Matt Damon) must battle his way through the treacherous waters of international espionage to prove his innocence, recover his past, and save the woman he loves. Also starring Oscar-nominee Joan Allen, Brian Cox, and Julia Stiles.',
        'title': u'The Bourne Supremacy',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'7275998',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Steven Spielberg',
        'display_end_date': 1435733999.0,
        'display_index': 85,
        'display_start_date': 1395990000.0,
        'duration': u'127',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all adventure action sciencefiction thriller',
        'genreList': [   u'Adventure',
                         u'Action',
                         u'Science Fiction',
                         u'Thriller'],
        'genres': u'Adventure, Action, Science Fiction, Thriller',
        'id': u'87',
        'image_background': u'/videos/vodmov/1063/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1063/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1063/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1063/title.jpg',
        'jsTitle': u'Jurassic Park',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/7275998/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG',
        'ratingImage': 'rating_pg.gif',
        'ratingInt': 2,
        'release_date': u'1993-06-11',
        'release_year': u'1993',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Sam Neill, Laura Dern, Jeff Goldblum, Samuel Jackson, Richard Attenborough',
        'studio': u'UNI',
        'summary': u"Academy Award-winner Steven Spielberg's classic vision of a theme park featuring genetically-engineered dinosaurs. Who will survive when the dinosaurs run amok? An all-star cast brings the unforgettable characters of Michael Crichton's best-seller to brilliant life. Academy Award-winner Richard Attenborough stars as misguided tycoon, John Hammond. Sam Neill, Academy Award-nominees Laura Dern, Jeff Goldblum, and Samuel L. Jackson shine in the role of quirky experts brought to analyze the nuances of the new park.",
        'title': u'Jurassic Park',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'7634962',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Justin Lin',
        'display_end_date': 1435733999.0,
        'display_index': 86,
        'display_start_date': 1395990000.0,
        'duration': u'109',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all action drama',
        'genreList': [u'Action', u'Drama'],
        'genres': u'Action, Drama',
        'id': u'900',
        'image_background': u'/videos/vodmov/1165/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1165/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1165/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1165/title.jpg',
        'jsTitle': u'Fast & Furious (2009)',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/7634962/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG-13',
        'ratingImage': 'rating_pg13.gif',
        'ratingInt': 3,
        'release_date': u'2009-04-03',
        'release_year': u'2009',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Vin Diesel, Paul Walker, Michelle Rodriguez',
        'studio': u'UNI',
        'summary': u"When a crime brings them back to the mean streets of Los Angeles, fugitive ex-convict Dom Toretto (Vin Diesel) and agent Brian O'Conner (Paul Walker) reignite their high-octane feud. However, when a common enemy rears his head, Dom and Brian must learn how to work together and trust one another in order to defeat him.",
        'title': u'Fast & Furious (2009)',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'7634965',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Martin Campbell',
        'display_end_date': 1427871599.0,
        'display_index': 90,
        'display_start_date': 1395990000.0,
        'duration': u'130',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all adventure action',
        'genreList': [u'Adventure', u'Action'],
        'genres': u'Adventure, Action',
        'id': u'886',
        'image_background': u'/videos/vodmov/1178/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1178/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1178/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1178/title.jpg',
        'jsTitle': u'Goldeneye',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/7634965/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG-13',
        'ratingImage': 'rating_pg13.gif',
        'ratingInt': 3,
        'release_date': u'1995-11-17',
        'release_year': u'1995',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Pierce Brosnan, Sean Bean, Izabella Scorupco',
        'studio': u'Terry_Steiner_International',
        'summary': u"When a powerful satellite system falls into the hands of Alec Trevelyan, AKA Agent 006, a former ally-turned-enemy, only James Bond can save the world from an awesome space weapon that - in one short pulse - could destroy the earth! As Bond squares off against his former compatriot, he also battles Trevelyan's stunning ally, Xenia Onatopp, an assassin who uses pleasure as her ultimate weapon.",
        'title': u'Goldeneye',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'7634964',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Guy Hamilton',
        'display_end_date': 1427871599.0,
        'display_index': 92,
        'display_start_date': 1395990000.0,
        'duration': u'110',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all adventure action',
        'genreList': [u'Adventure', u'Action'],
        'genres': u'Adventure, Action',
        'id': u'885',
        'image_background': u'/videos/vodmov/1177/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1177/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1177/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1177/title.jpg',
        'jsTitle': u'Goldfinger',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/7634964/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG',
        'ratingImage': 'rating_pg.gif',
        'ratingInt': 2,
        'release_date': u'1964-09-17',
        'release_year': u'1964',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Sean Connery, Gert Frobe, Honor Blackman',
        'studio': u'Terry_Steiner_International',
        'summary': u"Special agent 007 comes face to face with one of the most notorious villains of all time, and now he must outwit and outgun the powerful tycoon to prevent him from cashing in on a devious scheme to raid Fort Knox - and obliterate the world's economy.",
        'title': u'Goldfinger',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'7634956',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Robert Rodriguez',
        'display_end_date': 1427871599.0,
        'display_index': 93,
        'display_start_date': 1395990000.0,
        'duration': u'84',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all adventure action comedy family',
        'genreList': [u'Adventure', u'Action', u'Comedy', u'Family'],
        'genres': u'Adventure, Action, Comedy, Family',
        'id': u'805',
        'image_background': u'/videos/vodmov/1153/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1153/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1153/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1153/title.jpg',
        'jsTitle': u'Spy Kids 3: Game Over',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/7634956/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG',
        'ratingImage': 'rating_pg.gif',
        'ratingInt': 2,
        'release_date': u'2003-07-25',
        'release_year': u'2003',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Antonio Banderas, Carla Gugino, Alexa Vega',
        'studio': u'Terry_Steiner_International',
        'summary': u'Pint-sized kid spy Juni Cortez faces his biggest challenge yet when he confronts the Toymaker, a ruthless villain sentenced to virtual prison by the Organization of Super Spies. The Toymaker has captured Juni\'s sister, OSS agent Carmen, and is holding her inside a virtual reality environment called "Game Over." Now Juni must use his cunning to advance to the nearly impossible "Level Five" in order to rescue his sister.',
        'title': u'Spy Kids 3: Game Over',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'7634967',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Terence Young',
        'display_end_date': 1427871599.0,
        'display_index': 94,
        'display_start_date': 1395990000.0,
        'duration': u'130',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all adventure action',
        'genreList': [u'Adventure', u'Action'],
        'genres': u'Adventure, Action',
        'id': u'889',
        'image_background': u'/videos/vodmov/1180/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1180/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1180/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1180/title.jpg',
        'jsTitle': u'Thunderball',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/7634967/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG',
        'ratingImage': 'rating_pg.gif',
        'ratingInt': 2,
        'release_date': u'1965-12-20',
        'release_year': u'1965',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Sean Connery, Claudine Auger, Adolfo Celi',
        'studio': u'Terry_Steiner_International',
        'summary': u"Led by one-eyed evil mastermind Emilio Largo, the terrorist group SPECTRE hijacks two warheads from a NATO plane and threatens widespread nuclear destruction to extort 100 million pounds. The dashing Agent 007, James Bond, is sent to recover the warheads from the heart of Largo's lair in the Bahamas, facing underwater attacks from sharks and men alike. He must also convince the enchanting Domino, Largo's mistress, to become a key ally.",
        'title': u'Thunderball',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'7634969',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Lewis Gilbert',
        'display_end_date': 1427871599.0,
        'display_index': 97,
        'display_start_date': 1395990000.0,
        'duration': u'126',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all adventure action',
        'genreList': [u'Adventure', u'Action'],
        'genres': u'Adventure, Action',
        'id': u'937',
        'image_background': u'/videos/vodmov/1181/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/1181/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/1181/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/1181/title.jpg',
        'jsTitle': u'The Spy Who Loved Me',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'getconnected.southwestwifi.com/vod/7634969/std/index.m3u8',
        'price': 500.0,
        'rating': u'PG',
        'ratingImage': 'rating_pg.gif',
        'ratingInt': 2,
        'release_date': u'1977-07-07',
        'release_year': u'1977',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Roger Moore, Barbara Bach, Curt Jurgens',
        'studio': u'Terry_Steiner_International',
        'summary': u"In a globe-trotting assignment that has him skiing off the edges of cliffs and driving a car deep underwater, British super-spy James Bond unites with sexy Russian agent Anya Amasova to defeat megalomaniac shipping magnate Karl Stromberg, who is threatening to destroy New York City with nuclear weapons. Bond's most deadly adversary on the case is Stromberg's henchman, Jaws, a seven-foot giant with terrifying steel teeth.",
        'title': u'The Spy Who Loved Me',
        'use_vdrm': True,
        'vodType': u'vdrm'},
    {   'airlineList': ['SWA'],
        'availableLanguages': ['en'],
        'change_minutes': set([480]),
        'contentId': u'12345',
        'currency': u'USD',
        'defaultLanguage': 'en',
        'director': u'Test Director',
        'display_end_date': 1546329599.0,
        'display_index': 99,
        'display_start_date': 1391241600.0,
        'duration': u'116',
        'fileTitle': '',
        'formattedPrice': u'$5',
        'genreClasses': u'all action comedy',
        'genreList': [u'Action', u'Comedy'],
        'genres': u'Action, Comedy',
        'id': u'12345',
        'image_background': u'/videos/vodmov/12345/background.jpg',
        'image_laptopflyout': u'/videos/vodmov/12345/laptopflyout.jpg',
        'image_mobileflyout': u'/videos/vodmov/12345/mobileflyout.jpg',
        'image_pv': '',
        'image_title': u'/videos/vodmov/12345/title.jpg',
        'jsTitle': u'Test Movie',
        'mediaType': 'movie',
        'packageCode': '',
        'playlink': u'http://stream.southwestwifi.com:8080/hls/12345/12345.m3u8',
        'price': 500.0,
        'rating': u'G',
        'ratingImage': 'rating_g.gif',
        'ratingInt': 1,
        'release_date': u'2015-01-01',
        'release_year': u'2015',
        'routeList': [u'*-*'],
        'server_size': '',
        'stars': u'Test Star',
        'studio': u'Test',
        'summary': u'For testing.',
        'title': u'Test Movie',
        'use_vdrm': False,
        'vodType': u'hls'}]
