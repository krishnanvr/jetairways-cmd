
/*
 *
 * How to assign a track to a playlist tab view in the html markup:
 *
 * In "Browse Playlists" Tab, each playlist has it's own list of tracks.
 * In order for the player round buttons to show in front of the current playlist and
 * in front of the current track, the playlist track's "track_id" must match the 
 * attribute 'data-track-id'
 *
 *
 */

var myCirclePlayer;

jQuery(function($){
	$.fn.extend({
		openModal: function(options){
			// use 'relative_to' as the selector of the element you want to attach the modal to
			var options = options || {};
			var is_relative = 'relative_to' in options;
			if(is_relative){
				if($(options.relative_to).length > 0){
					return this.each(function(){
						
						var arrowHeight = 8;
						
						// choose 1 of 3 animations:
						// 1. animate from right to centered (like mobile version):
							/*$(this).css({
								'margin': '0 0 0 ' + Math.ceil($(window).innerWidth() * 0.51) + 'px',
								'top': $(options.relative_to).offset().top + $(options.relative_to).innerHeight() - arrowHeight
							}).fadeIn(1);
							$(this).animate({ marginLeft: '-502px' }, "slow", function(){
								// callback
							});*/

						// 2. animate growing height from the playlist clicked:
							/*$(this).css({
								'margin-left': '-502px',
								'top': $(options.relative_to).offset().top + $(options.relative_to).innerHeight() - arrowHeight
							});
							$(this).slideDown(400, function(){
								// callback
							});*/

						// 3. fadeIn:
							$(this).css({
								'margin-left': '-502px',
								'top': $(options.relative_to).offset().top + $(options.relative_to).innerHeight() - arrowHeight
							}).fadeIn("fast");

						// end of animation options

						myCirclePlayer.updatePlayerElements();
					});
				}
			} else {
				return this.each(function(){
					$(this).css({
						left: 'auto',
						right: -$(window).innerWidth() + 'px',
						opacity: 1
					}).show();
					$(this).animate({ right: 0 }, "slow", function(){
						// callback
					});
					myCirclePlayer.updatePlayerElements();
				});
			}
		},
		closeModal: function(){
			return this.each(function(){
				$(this).fadeOut();
			});
		}
	});
	$('body').on('click', '#music-nav-btn', function(e){
		$('#header-nav-music-item').toggleClass('active');
		$('body').toggleClass('bar-player-active');
	});
	$('body').on('click', '.playlist-dropdown-click-area', function(e){
		$(e.currentTarget).closest('.playlist-info').addClass('pressed').toggleClass('active');
		$('.playlist-info:not(.pressed)').removeClass('active');
		$(e.currentTarget).closest('.playlist-info').removeClass('pressed');
		var playlist_id = $(e.currentTarget).closest('.playlist').attr('data-playlist-id');
		if (typeof playlist_id != "undefined") {
			e.stopPropagation();
			launchPlaylistModal(playlist_id);
		}
	});
	$('body').on('click', '.close-playlists-dropdown', function(e){
		$(e.currentTarget).closest('.playlist-info').removeClass('active');
		$('#modal').closeModal();
	});
	$('body').on('click', '.next-song-btn.enabled', function(e){
		var currentIndex = myCirclePlayer.player.getPlaylistIndex();
		if(currentIndex + 1 < myCirclePlayer.player.getPlaylist().length){
			myCirclePlayer.player.playlistItem(currentIndex+1);
		}
	});
	$('body').on('click', '.playlist-dropdown .playlist-track[data-track-id]', function(e){
		var trackId = $(e.currentTarget).attr('data-track-id');
	});
	$('body').on('click', '.playlist-track:not(.active)', function(e){
		var playlistId = $(e.currentTarget).closest('.playlist-dropdown').attr('data-playlist-id') || $(e.currentTarget).closest('[data-playlist-id]').attr('data-playlist-id') || null;
		var trackIndex = $(e.currentTarget).closest('.playlist-track').attr('data-track-index') || null;
		var findThePlaylist = _.where(playlists, {"playlist_id": playlistId});
		if (findThePlaylist.length > 0){
			loadNewPlaylist(findThePlaylist[0], trackIndex);
		}
	});

});

/*
 *
 * this playlists array is an example that combines jwplayer and beatsmusic info
 * 
 */
var playlists = [
	{
		"image": "/images/assets/music/album_small_1.jpg",
		"title": "Electopop",
		"playlist_id": "random_nr_r7839085",
		"description": "Electronic",
		"duration": 464,
		"total_tracks": 2,
		"tracks": [
			{
				// jwplayer dependencies
				image: 'http://yt3.ggpht.com/-A7wu2IGsk4U/AAAAAAAAAAI/AAAAAAAAAAA/b0Thd3EWNmA/s100-c-k-no/photo.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/01-christina_perri-trust.mp3' }],
				title: 'Trust',
				// beats design and functionality dependencies
				track_id: 'random_id_sdaoufghla',
				artists: 'Christina Perri',
				album: 'Head or Heart',
				thumbnail: 'http://yt3.ggpht.com/-A7wu2IGsk4U/AAAAAAAAAAI/AAAAAAAAAAA/b0Thd3EWNmA/s100-c-k-no/photo.jpg',
				duration: 212
			},
			{
				image: 'http://indiemusicheartbeat.files.wordpress.com/2012/02/cover.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/El_Ten_Eleven_-_Living_On_Credit_Blues.mp3' }],
				title: 'Living On Credit Blues',
				track_id: 'another_random_id_asoighdos',
				artists: 'El Ten Eleven',
				album: 'El Ten Eleven',
				thumbnail: 'http://www.thesilentballet.com/images/top50/2005/elteneleven.jpg',
				duration: 252
			}
		],
		"author":{
			"ref_type":"genre",
			"display":"Classic Rock",
			"id":"eg97079905791115776"
		}
	},
	{
		"image": "/images/assets/music/album_small_2.jpg",
		"title": "Shh...I’m Trying to Study",
		"playlist_id": "random_nr_r3793085",
		"description": "R&B",
		"duration": 470,
		"total_tracks": 2,
		"tracks": [
			{
				image: 'http://4.bp.blogspot.com/_I3vy8a7Bfqw/TQZtGkhBToI/AAAAAAAAE4s/EdH42MlyZN4/s1600/pon.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/Rihanna/Music_of_the_Sun/Pon_de_Replay.mp3' }],
				title: 'Pon de Replay',
				track_id: 'random_id_riripon_02',
				artists: 'Rihanna',
				album: 'Music of the Sun',
				thumbnail: 'http://4.bp.blogspot.com/_I3vy8a7Bfqw/TQZtGkhBToI/AAAAAAAAE4s/EdH42MlyZN4/s1600/pon.jpg',
				duration: 247
			},
			{
				image: 'http://www.steady130.com/wp-content/uploads/2013/02/arhs-400x400.png',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/Adele/21/Rumour_Has_It.mp3' }],
				title: 'Rumour Has It',
				track_id: 'random_id_adellerhi_02',
				artists: 'Adele',
				album: '21',
				thumbnail: 'http://www.steady130.com/wp-content/uploads/2013/02/arhs-400x400.png',
				duration: 223
			}
		],
		"author":{
			"ref_type":"genre",
			"display":"Classic Rock",
			"id":"eg97079905791115776"
		}
	},
	{
		"image": "/images/assets/music/album_small_3.jpg",
		"title": "Rolling Down the Street",
		"playlist_id": "random_nr_r6716872",
		"description": "R&B",
		"duration": 782,
		"total_tracks": 3,
		"tracks": [
			{
				image: 'http://www.steady130.com/wp-content/uploads/2013/02/arhs-400x400.png',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/Adele/21/Rumour_Has_It.mp3' }],
				title: 'Rumour Has It',
				track_id: 'random_id_adellerhi_03',
				artists: 'Adele',
				album: '21',
				thumbnail: 'http://www.steady130.com/wp-content/uploads/2013/02/arhs-400x400.png',
				duration: 223
			},
			{
				image: 'http://4.bp.blogspot.com/_I3vy8a7Bfqw/TQZtGkhBToI/AAAAAAAAE4s/EdH42MlyZN4/s1600/pon.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/Rihanna/Music_of_the_Sun/Pon_de_Replay.mp3' }],
				title: 'Pon de Replay',
				track_id: 'random_id_riripon_03',
				artists: 'Rihanna',
				album: 'Music of the Sun',
				thumbnail: 'http://4.bp.blogspot.com/_I3vy8a7Bfqw/TQZtGkhBToI/AAAAAAAAE4s/EdH42MlyZN4/s1600/pon.jpg',
				duration: 247
			},
			{
				image: 'http://yt3.ggpht.com/-A7wu2IGsk4U/AAAAAAAAAAI/AAAAAAAAAAA/b0Thd3EWNmA/s100-c-k-no/photo.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/01-christina_perri-trust.mp3' }],
				title: 'Trust',
				track_id: 'random_id_sdaoufghla_03',
				artists: 'Christina Perri',
				album: 'Head or Heart',
				thumbnail: 'http://yt3.ggpht.com/-A7wu2IGsk4U/AAAAAAAAAAI/AAAAAAAAAAA/b0Thd3EWNmA/s100-c-k-no/photo.jpg',
				duration: 212
			}
		],
		"author":{
			"ref_type":"genre",
			"display":"Classic Rock",
			"id":"eg97079905791115776"
		}
	},
	{
		"image": "/images/assets/music/album_small_4.jpg",
		"title": "Young love",
		"playlist_id": "random_nr_r21199",
		"description": "Alternative",
		"duration": 782,
		"total_tracks": 4,
		"tracks": [
			{
				image: 'http://www.steady130.com/wp-content/uploads/2013/02/arhs-400x400.png',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/Adele/21/Rumour_Has_It.mp3' }],
				title: 'Rumour Has It',
				track_id: 'random_id_adellerhi_03',
				artists: 'Adele',
				album: '21',
				thumbnail: 'http://www.steady130.com/wp-content/uploads/2013/02/arhs-400x400.png',
				duration: 223
			},
			{
				image: 'http://4.bp.blogspot.com/_I3vy8a7Bfqw/TQZtGkhBToI/AAAAAAAAE4s/EdH42MlyZN4/s1600/pon.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/Rihanna/Music_of_the_Sun/Pon_de_Replay.mp3' }],
				title: 'Pon de Replay',
				track_id: 'random_id_riripon_03',
				artists: 'Rihanna',
				album: 'Music of the Sun',
				thumbnail: 'http://4.bp.blogspot.com/_I3vy8a7Bfqw/TQZtGkhBToI/AAAAAAAAE4s/EdH42MlyZN4/s1600/pon.jpg',
				duration: 247
			},
			{
				image: 'http://yt3.ggpht.com/-A7wu2IGsk4U/AAAAAAAAAAI/AAAAAAAAAAA/b0Thd3EWNmA/s100-c-k-no/photo.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/01-christina_perri-trust.mp3' }],
				title: 'Trust',
				track_id: 'random_id_sdaoufghla_03',
				artists: 'Christina Perri',
				album: 'Head or Heart',
				thumbnail: 'http://yt3.ggpht.com/-A7wu2IGsk4U/AAAAAAAAAAI/AAAAAAAAAAA/b0Thd3EWNmA/s100-c-k-no/photo.jpg',
				duration: 212
			},
			{
				image: 'http://indiemusicheartbeat.files.wordpress.com/2012/02/cover.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/El_Ten_Eleven_-_Living_On_Credit_Blues.mp3' }],
				title: 'Living On Credit Blues',
				track_id: 'another_random_id_asoighdos',
				artists: 'El Ten Eleven',
				album: 'El Ten Eleven',
				thumbnail: 'http://www.thesilentballet.com/images/top50/2005/elteneleven.jpg',
				duration: 252
			}
		],
		"author":{
			"ref_type":"genre",
			"display":"Classic Rock",
			"id":"eg97079905791115776"
		}
	},
	
	{
		"image": "/images/assets/music/album_small_1.jpg",
		"title": "Electopop",
		"playlist_id": "random_nr_r7839085",
		"description": "Electronic",
		"duration": 464,
		"total_tracks": 2,
		"tracks": [
			{
				// jwplayer dependencies
				image: 'http://yt3.ggpht.com/-A7wu2IGsk4U/AAAAAAAAAAI/AAAAAAAAAAA/b0Thd3EWNmA/s100-c-k-no/photo.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/01-christina_perri-trust.mp3' }],
				title: 'Trust',
				// beats design and functionality dependencies
				track_id: 'random_id_sdaoufghla',
				artists: 'Christina Perri',
				album: 'Head or Heart',
				thumbnail: 'http://yt3.ggpht.com/-A7wu2IGsk4U/AAAAAAAAAAI/AAAAAAAAAAA/b0Thd3EWNmA/s100-c-k-no/photo.jpg',
				duration: 212
			},
			{
				image: 'http://indiemusicheartbeat.files.wordpress.com/2012/02/cover.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/El_Ten_Eleven_-_Living_On_Credit_Blues.mp3' }],
				title: 'Living On Credit Blues',
				track_id: 'another_random_id_asoighdos',
				artists: 'El Ten Eleven',
				album: 'El Ten Eleven',
				thumbnail: 'http://www.thesilentballet.com/images/top50/2005/elteneleven.jpg',
				duration: 252
			}
		],
		"author":{
			"ref_type":"genre",
			"display":"Classic Rock",
			"id":"eg97079905791115776"
		}
	},
	
	{
		"image": "/images/assets/music/album_small_4.jpg",
		"title": "Young love",
		"playlist_id": "random_nr_r2119",
		"description": "Alternative",
		"duration": 782,
		"total_tracks": 4,
		"tracks": [
			{
				image: 'http://www.steady130.com/wp-content/uploads/2013/02/arhs-400x400.png',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/Adele/21/Rumour_Has_It.mp3' }],
				title: 'Rumour Has It',
				track_id: 'random_id_adellerhi_03',
				artists: 'Adele',
				album: '21',
				thumbnail: 'http://www.steady130.com/wp-content/uploads/2013/02/arhs-400x400.png',
				duration: 223
			},
			{
				image: 'http://4.bp.blogspot.com/_I3vy8a7Bfqw/TQZtGkhBToI/AAAAAAAAE4s/EdH42MlyZN4/s1600/pon.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/Rihanna/Music_of_the_Sun/Pon_de_Replay.mp3' }],
				title: 'Pon de Replay',
				track_id: 'random_id_riripon_03',
				artists: 'Rihanna',
				album: 'Music of the Sun',
				thumbnail: 'http://4.bp.blogspot.com/_I3vy8a7Bfqw/TQZtGkhBToI/AAAAAAAAE4s/EdH42MlyZN4/s1600/pon.jpg',
				duration: 247
			},
			{
				image: 'http://yt3.ggpht.com/-A7wu2IGsk4U/AAAAAAAAAAI/AAAAAAAAAAA/b0Thd3EWNmA/s100-c-k-no/photo.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/01-christina_perri-trust.mp3' }],
				title: 'Trust',
				track_id: 'random_id_sdaoufghla_03',
				artists: 'Christina Perri',
				album: 'Head or Heart',
				thumbnail: 'http://yt3.ggpht.com/-A7wu2IGsk4U/AAAAAAAAAAI/AAAAAAAAAAA/b0Thd3EWNmA/s100-c-k-no/photo.jpg',
				duration: 212
			},
			{
				image: 'http://indiemusicheartbeat.files.wordpress.com/2012/02/cover.jpg',
				sources: [{ file: 'http://ec2-54-81-127-152.compute-1.amazonaws.com/beats-mobile/beatsplayer/music/mp3/El_Ten_Eleven_-_Living_On_Credit_Blues.mp3' }],
				title: 'Living On Credit Blues',
				track_id: 'another_random_id_asoighdos',
				artists: 'El Ten Eleven',
				album: 'El Ten Eleven',
				thumbnail: 'http://www.thesilentballet.com/images/top50/2005/elteneleven.jpg',
				duration: 252
			}
		],
		"author":{
			"ref_type":"genre",
			"display":"Classic Rock",
			"id":"eg97079905791115776"
		}
	}
	
	
];
// user sentence options, keep this variable updated
var chosen_sentence_options = {
	'places': 'in the air',
	'activities': 'chilling out',
	'people': 'myself',
	'genres': 'Jazz'
}

// pagination of sentence options:
var nr_of_options_per_view = 6;

var defaultPlaylist = currentPlaylist = playlists[0];
// var playerPrimary = "flash";

// if (navigator.userAgent.toLowerCase().match(/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/) != null) {
// // if (navigator.userAgent.match(/android/i) != null) {
// 	playerPrimary = "html5";
// }

jwplayer("musicPlayer").setup({
	playlist: defaultPlaylist.tracks,
	height: 46,
	width: 46,
	primary: "html5",
	type: "mp3",
	skin: "/js/beats/skin.xml",
	autostart: false,
	analytics: {
		enabled: false,
		cookies: false
	},
	modes: [
		{ type: "html5" },
		{ type: "flash" }
	],
	events:	{
		onPlay: function(){
			// updates the dropdown player element with the current track's image and info:
			var currentIndex = myCirclePlayer.player.getPlaylistIndex();
			var track = myCirclePlayer.player.getPlaylist()[currentIndex];
			
			$('.dropdown-player .song-thumbnail').css({
				"backgroundImage": 'url('+track.thumbnail+')'
			});
			$('.dropdown-player .song-title').html(track.title);
			$('.dropdown-player .artists').html(track.artists);
			
			// simulates a player in the playlist tracks list with the same id as the current track
			var $playlistTrackElem = $('.playlist-track[data-track-id="'+ track.track_id +'"]');
			$('.playlist-track').removeClass('active playing');
			$playlistTrackElem.addClass('active playing');
			// $('.playlist-item-play-pause').removeClass('active');
			// $playlistTrackElem.closest('.playlist').find('.playlist-item-play-pause').addClass('active');
			$('.playlist').removeClass('active');
			// $playlistTrackElem.closest('.playlist').addClass('active');
			$('.playlist[data-playlist-id=' + currentPlaylist.playlist_id + ']').addClass('active');
			
			// if this is the last song of the playlist, disable next button
			$('.next-song-btn').removeClass('enabled');
			
			// if there are songs after this one, enable next-song-btn:
			if(currentIndex + 1 < myCirclePlayer.player.getPlaylist().length){
				$('.next-song-btn').addClass('enabled');
			} else {
				$('.next-song-btn').removeClass('enabled');
			}
			
			updateSentenceTabPlayer(track, false);
			myCirclePlayer.updatePlayerElements();
			
		},
		onPause: function(){
			var currentIndex = myCirclePlayer.player.getPlaylistIndex();
			var track = myCirclePlayer.player.getPlaylist()[currentIndex];
			var $playlistTrackElem = $('.playlist-track[data-track-id="'+ track.track_id +'"]');
			$playlistTrackElem.removeClass('playing').addClass('active');
			// $playlistTrackElem.closest('.playlist').find('.playlist-item-play-pause').removeClass('active');
			// $playlistTrackElem.closest('.playlist').removeClass('active');
			$('.playlist').removeClass('active');
			myCirclePlayer.updatePlayerElements();
		},
		onIdle: function(){
			var currentIndex = myCirclePlayer.player.getPlaylistIndex();
			var track = myCirclePlayer.player.getPlaylist()[currentIndex];
			var $playlistTrackElem = $('.playlist-track[data-track-id="'+ track.track_id +'"]');
			$playlistTrackElem.removeClass('playing').addClass('active');
			// $playlistTrackElem.closest('.playlist').find('.playlist-item-play-pause').removeClass('active');
			$playlistTrackElem.closest('.playlist').removeClass('active');
			updateSentenceTabPlayer(false, false);
		},
		onComplete: function(){
			// $('.playlist-item-play-pause').removeClass('active');
			$('.playlist').removeClass('active');
		}
	}
});


$(document).ready(function(){
	
	myCirclePlayer = new CirclePlayer("musicPlayer");

	updatePlaylistsView();
	// examole fetching playlists from beats (not implemented)
	/*
	$.ajax({
		dataType: "json",
		url: 'jwtest/playlists-obj-example.json',
		cache: false,
		success: function(response){
			// console.log(response);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
		}
	});
	*/
	
});

// how to load a single file
// function loadSong(track) { 
// 	myCirclePlayer.player.load([{
// 		file: track.file_name
// 	}]).play();
// };

// define timezone offset
// new Date().getTimezoneOffset()/60;

var paginated_sentence_options = {};
var sentence_options;

// The Sentence wizard:
$(function(){
	return; // Disable Sentence Wizard
	// run plugins
	// $.getScript('js/underscore.min.js', function(){
	// 	$.getScript('js/carousel.js', function(){
			
			// Sentence Options example (make sure you use an updated source)
			// (see https://developer.beatsmusic.com/docs/recs/The_Sentence_Options )

			$(function(){
				$.ajax({
					dataType: "json",
					url: '/js/beats/sentence_options.json',
					cache: false,
					success: function(response){
						sentence_options = response;

						launchTheSentenceWizard();
					},
					error: function(XMLHttpRequest, textStatus, errorThrown){
						console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
					}
				});
			});

	// 	}).fail(function(){ console.log('failed loading carousel.js')});
	// }).fail(function(){ console.log('failed loading underscore.min.js')});

	$('body').on('click', '.sentence-wrapper .load-more', function(e){
		
		var slide = $(e.currentTarget).closest('[data-sentence]').attr('data-sentence');
		var currentIndex = $(e.currentTarget).closest('[data-sentence]').find('.sentence-options').attr('data-index');
		var newIndex = parseInt(currentIndex) + 1;

		if (newIndex >= paginated_sentence_options[slide].length) {
			newIndex = 0;
		}

		showMoreSentenceOptions(slide, newIndex, animateSentenceOptions);
	});

	$('body').on('click', '.sentence-options .sentence-option', function(e){
		
		var optionId = $(e.currentTarget).attr('data-id');
		var optionType = $(e.currentTarget).attr('data-ref-type');
		var optionText = $('em', $(e.currentTarget)).text();

		// console.log($(e.currentTarget).text());
		// console.log(optionType);

		var slide;
		switch (optionType){
			case 'sentence_place':
				slide = 'places';
				break;
			case 'sentence_activity':
				slide = 'activities';
				break;
			case 'sentence_people':
				slide = 'people';
				break;
			case 'sentence_genre':
				slide = 'genres';
				break;
		}
		chosen_sentence_options[slide] = $(e.currentTarget).text();
		updateChosenSentenceOptionsText();
		paginateSentenceOptions();

		// populates the first set of options for each sentence picker slide
		_.each(paginated_sentence_options, function(index, slide){
			
			showMoreSentenceOptions(slide, 0);

		});

		// replace all sentence text fields:
		$("." + optionType + ' em').text(optionText);
		
		// todo: update current sentence option by id (using beats sentence api and optionId)

		// go back to sentence main slide
		$('#sentenceCarousel').carousel(0);

	});

	$('body').on('click', '.defining-sentence .sentence-option.chosen', function(e){
		$('#sentenceCarousel').carousel(0);
	});

	$('body').on('click', '#sentence-play-info .playlist-starter', function(){
		// will load a new playlist based on the sentence options
		// this example loads 'playlists[2]'
		loadNewPlaylist( playlists[2] );
	});

	// button to go back to "Now Playing" when user cancels editing the sentence
	$('body').on ('click', '.back-to-now-playing', function(){
		var currentIndex = myCirclePlayer.player.getPlaylistIndex();
		var track = myCirclePlayer.player.getPlaylist()[currentIndex];
		updateSentenceTabPlayer(track);
	});

});
	

function updateChosenSentenceOptionsText() {
	// populate the original chosen values:
	$('.sentence_place.chosen em').text(chosen_sentence_options.places);
	$('.sentence_activity.chosen em').text(chosen_sentence_options.activities);
	$('.sentence_people.chosen em').text(chosen_sentence_options.people);
	$('.sentence_genre.chosen em').text(chosen_sentence_options.genres);
}

/*
 * Builds the interface that handles The Sentence
 * based on a bootstrap carousel
 *
 */
function launchTheSentenceWizard(){
	// run wizard
	$.fn.carousel.defaults = {
		interval: false, // no cycling timers
		pause: 'false' // pauses carousel on mouseover
	};
	$('.music-carousel').carousel('pause').on('slide', function (e) {
		// console.log('sliding started'); // transitions disabled
		$('.show-me-after-anim').hide();
	}).on('slid', function (e) {
		// console.log('slide complete');
		animateSentenceOptions();
	});

	// show the first slide (hidden by default)
	$('#sentenceCarousel .item.active .sentence-wrapper').stop().slideDown(400);

		
	updateChosenSentenceOptionsText();

	paginateSentenceOptions();

	// populates the first set of options for each sentence picker slide
	_.each(chosen_sentence_options, function(index, slide){
		
		showMoreSentenceOptions(slide, 0);

	});

	// switches the sentence interface to the "edit sentence" state
	updateSentenceTabPlayer();
}

function paginateSentenceOptions(){
	
	// empty existing object:
	paginated_sentence_options = {};

	// populate sentence options:
	var slides = ['places', 'activities', 'people', 'genres'];

	_.each(slides, function(slide) {
		
		var s = 0;
		var newArray = [];
		paginated_sentence_options[slide] = [];

		_.each(sentence_options['data'][slide], function(sentence_option) {
			
			// only if option is not chosen:
			if(chosen_sentence_options[slide] !== sentence_option.display){
				newArray.push(sentence_option);
				
				if ( (s+1) % nr_of_options_per_view == 0 && (s+1) != sentence_options['data'][slide].length) {
					paginated_sentence_options[slide].push(newArray);
					newArray = [];
				}

				if ( (s+1) == sentence_options['data'][slide].length) {
					paginated_sentence_options[slide].push(newArray);
				}
				s++;
			} else {
				// this option matches the chosen one and we don't want to display it
				// console.log('boom: ' + sentence_option.display)
			}


		});

	});

	// console.log(paginated_sentence_options);
	return paginated_sentence_options;
}

/*
 *
 * loads more sentence options to choose from, uses paginated_sentence_options array as source
 *
 * @param slide: current slide that we are editing
 * @param index: current index of the pagination
 *
 */
function showMoreSentenceOptions(slide, index, callback) {
	// paginateSentenceOptions(); // updates options and hides the chosen option

	var $slideDiv = $('#sentence-' + slide + ' .sentence-options');
	$slideDiv.attr('data-index', index).empty();

	_.each(paginated_sentence_options[slide][index], function(sentence_option){
		
		var slideHtml = _.template($('#sentence-options-template').html(), {option: sentence_option});
		$('#sentence-' + slide + ' .sentence-options').append(slideHtml);
		
	});
	if (typeof callback == 'function') {
		return callback();
	};

}

/*
 *
 * Animates the sentence options as seen in the beats music mobile app
 *
 */
function animateSentenceOptions(){
	$('#sentenceCarousel .sentence-wrapper').stop().hide();
	$('#sentenceCarousel .item.active .sentence-wrapper span').addClass('animate');
	$('#sentenceCarousel .item.active .sentence-wrapper').stop().slideDown(400, function(){
		$('#sentenceCarousel .item.active .sentence-wrapper span').removeClass('animate');
		$('#sentenceCarousel .item.active .sentence-wrapper').css({ height: 'auto'});
		if(myCirclePlayer.player.getState() == 'PLAYING'){
			$('#sentenceCarousel .item.active .show-me-after-anim').show();
		}
	});
}

/*
 *
 * Updates the Sentence Tab Player with the current track information
 *
 * @param track: Object that needs the following properties as children: 
 * 				 (string) title, (string) artists, (string) album, (string) image
 * 				 Will default to false if no parameter is set.
 *
 * @param sentence: Object that needs the following properties as children: 
 * 					place_id, activity_id, people_id, genre_id
 * 					Will default to false if no parameter is set.
 *
 */
function updateSentenceTabPlayer(track, sentence){
	
	var track = track || false;
	var sentence = sentence || false;

	if (sentence) {
		var assignement = {
			"place": "places",
			"activity": "activities",
			"people": "people",
			"genre": "genres"
		}
		_.each(assignement, function(i, v){
			// finds the sentence option object by id:
			var sentence_option_object = _.where(sentence_options[assignement[v]], {"id": sentence[assignement[i] + '_id']});
			// updates all relevant sentence option texts in the html:
			$('.sentence_' + assignement[i]).text(sentence_option_object.display);
		});
	}

	// shows the correct slide on left half tab:
	if (track) {

		// updates track's background-image:
		if(typeof track.image == "string" && track.image.length > 0){
			$('#sentence-now-playing-bg').attr('src', track.image).show();
		} else {
			$('#sentence-now-playing-bg').hide();
		}

		$('#sentenceCarousel').carousel(5);
	} else {
		$('#sentenceCarousel').carousel(0);
	}

	// updates the right half tab with the player's information:
	var newTopHalf = _.template($('#sentence-player-template').html(), {track: track});
	$('#sentence-play-info').html(newTopHalf);
	
	// shows / hides the sentence player:
	track ? $('#sentence-play-ctrl').show() : $('#sentence-play-ctrl').hide();

}

function loadNewPlaylist(newPlaylist, track_index){
	var newPlaylist = newPlaylist || currentPlaylist; // if no playlist is given use the current one
	var track_index = track_index || 0; // defaults to zero if not given as a parameter

	// if the new playlist is the same as the current one:
	if (newPlaylist.playlist_id == currentPlaylist.playlist_id) {
		
		var currentIndex = myCirclePlayer.player.getPlaylistIndex() || false;
		
		// if the current song is the requested song:
		if (currentIndex != track_index) {
			// plays the requested track index
			myCirclePlayer.player.playlistItem(track_index);
		}
	} else {
		
		// updates currentPlaylist variable:
		currentPlaylist = newPlaylist;
		
		// loads the new playlist into the player
		jwplayer("musicPlayer").load(newPlaylist.tracks);

		// starts the playlist at the requested index
		myCirclePlayer.player.playlistItem(track_index);
		
	}

}


function updatePlaylistsView() {
	var playlistsHtml = _.template($('#playlists-list-template').html(), {"playlists": playlists});
	$('.playlists-list').html(playlistsHtml);
}

function launchPlaylistModal( playlist_id ) {
	
	var playlistFound = _.where(playlists, {"playlist_id": playlist_id});
	// note: if playlist isn't found the array will be empty

	// console.log(myCirclePlayer.player.getPlaylistItem())
	var playing_track_id = false,
		playlistItem,
		playlistOn = myCirclePlayer.player.getPlaylist() || false;
	if (playlistOn) {
		playlistItem =  myCirclePlayer.player.getPlaylistItem() || false;
		if (playlistItem) {
			playing_track_id = playlistItem.track_id
		}
	}

	// if there is a playlist with the given id in our array of playlists:
	if (playlistFound.length > 0){
		
		// populate the modal content using the playlist information:
		var modalHtml = _.template($('#playlist-modal-template').html(), {
			"playlist": playlistFound[0],
			"playing_track_id": playing_track_id,
			"playlist_id": playlist_id
		});
		$('#modal-content').html(modalHtml);

		// launch the modal:
		$('#modal').openModal({
			'relative_to' : '.playlist[data-playlist-id="' + playlist_id + '"]'
		});
	}
}

// Clicks outside the modal popup close it
$(document).on('click', function(event) {
	if ($('#modal').length) {
		$('#modal').closeModal();
	};
}).on('click', '#modal', function(event) {
	event.stopPropagation();
});

