var CirclePlayer = function(jPlayerSelector) {
	var	self = this,

		cssSelector = {
			bufferHolder: ".cp-buffer-holder",
			buffer1: ".cp-buffer-1",
			buffer2: ".cp-buffer-2",
			progressHolder: ".cp-progress-holder",
			progress1: ".cp-progress-1",
			progress2: ".cp-progress-2",
			circleControl: ".cp-circle-control",
			play: ".cp-play",
			pause: ".cp-pause"
		};

	this.cssClass = {
		gt50: "cp-gt50",
	};

	this.player = jwplayer(jPlayerSelector);

	this.audio = {};
	this.dragging = false; // Indicates if the progressbar is being 'dragged'.
    this.position = 0;
    this.duration = 1;
    this.complete = false;

	this.jq = {};
	$.each(cssSelector, function(entity, cssSel) {
		self.jq[entity] = $(cssSel);
	});

	this._initSolution();
	this._initPlayer();
};
var is_touch_device = 'ontouchstart' in document.documentElement;

CirclePlayer.prototype = {
	_initPlayer: function() {
		var self = this;
        self._initCircleControl();
        self._progress(100);
        this.player.onTime( function(event){
            self.position = jwplayer().getPosition();
            self.duration = jwplayer().getDuration();
            var p = Math.round(100 * self.position / self.duration);
                if (!self.dragging) {
                    self._timeupdate(p);
			}
		});
        this.player.onComplete( function(event){
            self.complete = true;
			self._pause();
			self._resetSolution();
        });
        this.player.onPlay(function(event){
        	self._play();
        });
	},
	_initSolution: function() {
		this.jq.progressHolder.show();
		this.jq.bufferHolder.show();
		this._resetSolution();
	},
	_resetSolution: function() {
		this.jq.progressHolder.removeClass(this.cssClass.gt50);
		this.jq.progress1.css({
			'-ms-transform': 'rotate(0deg)',
			'-webkit-transform': 'rotate(0deg)',
			'transform': 'rotate(0deg)',
			'width': '0%'});
		this.jq.progress2.css({
			'-ms-transform': 'rotate(0deg)',
			'-webkit-transform': 'rotate(0deg)',
			'transform': 'rotate(0deg)',
			'width': '0%'}).hide();
	},
    _play: function() {
        this.jq.play.hide();
        this.jq.pause.show();
        this.complete = false;
    },
    _pause: function() {
        this.jq.play.show();
        this.jq.pause.hide();
    },
	_initCircleControl: function() {
		var self = this;
		if(!is_touch_device){
			this.jq.circleControl.grab({
				onstart: function(){
					self.dragging = true;
				}, onmove: function(event){
					var pc = self._getArcPercent(event.position.x, event.position.y);
					var p =  pc * self.duration / 100;
					self.player.seek(p);
					self._timeupdate(pc);
					self._play();
				}, onfinish: function(event){
					self.dragging = false;
					var pc = self._getArcPercent(event.position.x, event.position.y);
					var p =  pc * self.duration / 100;
					self.player.seek(p);
					self._play();
				}
			});
		}
		this.jq.play.unbind('click');
		this.jq.play.click(function(){
				self._play();
				self.player.play();
        });
		this.jq.pause.unbind('click');
		this.jq.pause.click(function(){
				self._pause();
				self.player.pause();
        });
	},
	_timeupdate: function(percent) {
		var degs = percent * 3.6+"deg";
        if (this.complete) { return; }
    

		if (percent <= 50) {
				this.jq.progressHolder.removeClass(this.cssClass.gt50);
				this.jq.progress1.css({
					'-ms-transform': 'rotate(' + degs + ')',
					'-webkit-transform': 'rotate(' + degs + ')',
					'transform': 'rotate(' + degs + ')'});
				this.jq.progress2.hide();
		} else  
        if (percent <= 100) {
				this.jq.progressHolder.addClass(this.cssClass.gt50);
				this.jq.progress1.css({
					'-ms-transform': 'rotate(180deg)',
					'-webkit-transform': 'rotate(180deg)',
					'transform': 'rotate(180deg)'});
				this.jq.progress2.css({
					'-ms-transform': 'rotate(' + degs + ')',
					'-webkit-transform': 'rotate(' + degs + ')',
					'transform': 'rotate(' + degs + ')'});
				this.jq.progress2.show();
		}
		this.jq.progress1.show();
		this.jq.progress2.show();

		// Linear Progress Override
		this.jq.progressHolder.removeClass(this.cssClass.gt50);
		this.jq.progress1.css({
			'-ms-transform': 'rotate(0deg)',
			'-webkit-transform': 'rotate(0deg)',
			'transform': 'rotate(0deg)',
			'width': percent + '%'});
		this.jq.progress2.css({
			'-ms-transform': 'rotate(0deg)',
			'-webkit-transform': 'rotate(0deg)',
			'transform': 'rotate(0deg)',
			'width': percent + '%'});
		


		this._progress(this.player.getBuffer());
	},
	_progress: function(percent) {
		var degs = percent * 3.6+"deg";

			if (percent <= 50) {
				this.jq.bufferHolder.removeClass(this.cssClass.gt50);
				this.jq.buffer1.css({
					'-ms-transform': 'rotate(' + 0 + 'deg)',
					'-webkit-transform': 'rotate(' + 0 + 'deg)',
					'transform': 'rotate(' + 0 + 'deg)'});
				this.jq.buffer2.hide();
			} else if (percent <= 100) {
				this.jq.bufferHolder.addClass(this.cssClass.gt50);
				this.jq.buffer1.css({
					'-ms-transform': 'rotate(180deg)',
					'-webkit-transform': 'rotate(180deg)',
					'transform': 'rotate(180deg)'});
				this.jq.buffer2.show();
				this.jq.buffer2.css({
					'-ms-transform': 'rotate(' + 0 + 'deg)',
					'-webkit-transform': 'rotate(' + 0 + 'deg)',
					'transform': 'rotate(' + 0 + 'deg)'});
			}
			
			this.jq.buffer1.css({
				'width':  percent + '%'});
			this.jq.buffer2.css({
				'width':  percent + '%'});


	},
	_getArcPercent: function(pageX, pageY) {
		var	offset	= this.jq.circleControl.offset(),
			x	= pageX - offset.left - this.jq.circleControl.width()/2,
			y	= pageY - offset.top - this.jq.circleControl.height()/2,
			theta	= Math.atan2(y,x);

		if (theta > -1 * Math.PI && theta < -0.5 * Math.PI) {
			theta = 2 * Math.PI + theta;
		}

		// theta is now value between -0.5PI and 1.5PI
		// ready to be normalized and applied

		return (theta + Math.PI / 2) / 2 * Math.PI * 10;
	},
	/*
	 * updates the player using new selectors from the DOM, allowing the player to 
	 * update elements that were not in the DOM by the time of when the player started
	 * or last updated by this function
	 */
	updatePlayerElements : function(){
		// updates the player with the existing selectors in the DOM:
		var	self = this,
			cssSelector = {
				bufferHolder: ".cp-buffer-holder",
				buffer1: ".cp-buffer-1",
				buffer2: ".cp-buffer-2",
				progressHolder: ".cp-progress-holder",
				progress1: ".cp-progress-1",
				progress2: ".cp-progress-2",
				circleControl: ".cp-circle-control",
				play: ".cp-play",
				pause: ".cp-pause"
			};
		self.jq = {};
		$.each(cssSelector, function(entity, cssSel) {
			self.jq[entity] = $(cssSel);
		});
		
		var currentState = myCirclePlayer.player.getState() || "IDLE";
		var bufferPerc = self.player.getBuffer() || 0;
		var currentTime = self.player.getPosition() || 0;
		var currentDuration = self.player.getDuration() || false;

		// updates scrub position
		self._timeupdate(currentTime/currentDuration * 100);

		switch(currentState){
			case "PLAYING":
				self._play();
				break;
			case "PAUSED":
				self._pause();
				break;
			case "IDLE":
				// 
				break;
			case "BUFFERING":
				// 
				break;
		}
		// updates buffer positions:
		self._progress(bufferPerc);

		// update player control elements:
		self._initCircleControl();
	}
};