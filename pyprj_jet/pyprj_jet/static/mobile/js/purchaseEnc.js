
var ua = navigator.userAgent;
var isIOS = /iPad/i.test(ua) || /iPhone OS/i.test(ua);

var errorText = '';
function validate_error_callback() {
    if (errorText.length > 0) {
        jQuery('.errore-text').html(errorText).show();
    }
    else {
        jQuery('.errore-text').html('Please enter the missing / incorrect values below.').show();
    }
    jQuery(window).scrollTop(0);
}

function processCCInfo() {
    errorText = '';
    try {
        var cardSecurity = '' + jQuery('#cardSecurity').val();

        // Get credit card number and then validate it.
        var cardType = jQuery('#cardType').val();
        var cardNumber = '';
        if (cardType == 'American Express') {
            cardNumber = '' + jQuery('#cardAMEX1').val() + jQuery('#cardAMEX2').val() + jQuery('#cardAMEX3').val();

            var result = checkCreditCard(cardNumber, cardType);
            if (result != 0) {
                jQuery('.cardAMEX').addClass('errore');
                if (result != 1) errorText = 'The credit card number you have entered is invalid.';
                return false;
            }

            if (cardSecurity.length != 4) {
                jQuery('#cardSecurity').addClass('errore');
                if (result != 1) errorText = 'The credit card security code you have entered is invalid.';
                return false;
            }
        }
        else {
            cardNumber = '' + jQuery('#cardMCVD1').val() + jQuery('#cardMCVD2').val() + jQuery('#cardMCVD3').val() + jQuery('#cardMCVD4').val();

            var result = checkCreditCard(cardNumber, cardType);
            if (result != 0) {
                jQuery('.cardMCVD').addClass('errore');
                if (result != 1) errorText = 'The credit card number you have entered is invalid.';
                return false;
            }

            if (cardSecurity.length != 3) {
                jQuery('#cardSecurity').addClass('errore');
                if (result != 1) errorText = 'The credit card security code you have entered is invalid.';
                return false;
            }
        }
        //alert('cardNumber = ' + cardNumber + '   -   cardSecurity = ' + cardSecurity);

        // Create an RSA object.
        // Note: The xmlKeyParams should already be initialized.
        if (xmlKeyParams.length == 0) {
            throw 'Missing Key';
        }
        var rsa = new System.Security.Cryptography.RSACryptoServiceProvider();
        rsa.FromXmlString(xmlKeyParams);

        // Encrypt the CC card number and security code using the RSA object.
        var doOaepPadding = true;
        var decryptedBytes = System.Text.Encoding.Unicode.GetBytes(cardNumber);
        var encryptedBytes = rsa.Encrypt(decryptedBytes, doOaepPadding);
        var cardNumberEncrypted = System.Convert.ToBase64String(encryptedBytes);
        decryptedBytes = System.Text.Encoding.Unicode.GetBytes(cardSecurity);
        encryptedBytes = rsa.Encrypt(decryptedBytes, doOaepPadding);
        var cardSecurityEncrypted = System.Convert.ToBase64String(encryptedBytes);

        // Save the encrypted values to hidden form fields.
        jQuery('#cardNumberValue').val(cardNumberEncrypted);
        jQuery('#cardSecurityValue').val(cardSecurityEncrypted);
  
        // Save the last 4 digits of the credit card number.
        jQuery('#cardNumberLast4').val(cardNumber.substr(cardNumber.length - 4));

        // Clear the values from the input fields so they will not be submitted
        // to the server.
/*
        jQuery("#cardMCVD").mask("?");
        jQuery("#cardAMEX").mask("?");
        jQuery('#cardMCVD, #cardAMEX').val('Card Number');
        jQuery("#cardMCVD").one('focus', function() {jQuery("#cardMCVD").mask("9999 9999 9999 9999", {placeholder:"____ ____ ____ ____"});});
        jQuery("#cardAMEX").one('focus', function() {jQuery("#cardAMEX").mask("9999 999999 99999", {placeholder:"____ ______ _____"});});
*/
        jQuery('#cardMCVD1, #cardMCVD2, #cardMCVD3, #cardMCVD4').val('');
        jQuery('#cardAMEX1, #cardAMEX2, #cardAMEX3').val('');
        jQuery('#cardSecurity').val('Security Code');

        jQuery('.cardMCVD, .cardAMEX').removeClass('errore');

        return true;
    }
    catch(err) {
        errorText = 'There was a system error.  Please reload the page and try again.';
        return false;
    }
}

function validate_okay_callback() {
    var paymethod = jQuery("input:radio[name='paymethod']:checked").val();
    if (paymethod != 'pm_cc') {
        return true;
    }

    return processCCInfo();
}

/*================================================================================================*/
jQuery(document).ready(function() {

    // Open the Personal Information tab.
    jQuery('#pi-opener').click();

    jQuery('.info-form .next').click(function(e) {
        // Hide the current section.
        var parentNode = jQuery(this).parents(".info-section");
        jQuery('#' + parentNode.attr('id') + ' .opener').click();

        // Find the next section that can be displayed.
        var nextSection = null;
        var nextNodes = parentNode.nextAll(".info-section.visible");
        if (nextNodes.length > 0) {
            nextNodes.each(function(index) {
                //if (index == 0) jQuery('#' + jQuery(this).attr('id') + ' .opener').click();
                if (index == 0) nextSection = jQuery('#' + jQuery(this).attr('id'));
            });
        }
        else {
            //jQuery('#tos-info-section .opener').click();
            nextSection = jQuery('#tos-info-section');
        }

        // If the section is not already open, open it.
        if (nextSection && ! nextSection.hasClass('active')) {
            jQuery('#' + nextSection.attr('id') + ' .opener').click();
        }

        e.preventDefault();
        return false;
    });

    if (isIOS) {
        jQuery('.cardMCVD, .cardAMEX, #cardSecurity').keydown(function (e) {
            // Allow: backspace, delete, tab, escape and enter
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // Let it happen, don't do anything
                     return;
            }
            // If not a number, stop the keypress
            if (e.keyCode < 48 || e.keyCode > 57) {
                e.preventDefault();
            } 
        }); 
    }

    jQuery('.cardMCVD').keyup(function () {
        if (this.value.length == this.maxLength) {
           if(jQuery(this).get(0).id == 'cardMCVD4'){
            jQuery(this).blur();
           }else{
              jQuery(this).next().focus(); 
           }  
        }
    });
    jQuery('.cardAMEX').keyup(function () {
        if (this.value.length == this.maxLength) {
           if(jQuery(this).get(0).id == 'cardAMEX3'){
              jQuery(this).blur();
           }else{
              jQuery(this).next().focus(); 
           }
        }
    });
    jQuery('#cardSecurity').keyup(function () {
        if (this.value.length == this.maxLength) {
            jQuery(this).blur();
        }
    });

});

