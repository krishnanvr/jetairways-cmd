
function watchStream(){
        jQuery('#video')[0].play();

}



function openVodFlyout(){
    
showFlyout('epgFlyout','flyoutHolder', 0,0 );  
/*var tvSection = $('.tv-section');
  var page = $('.vod-flyout');
      page.addClass('openedFlyout');
      page.css('display', 'block');
      page.animate({
        'margin-left': '0',
        opacity: '1'
      },200, hideBody());
  function hideBody(){
    tvSection.css('display','none');
    $("html, body").animate({ scrollTop: 0 }, "slow");
  }*/
};
  
function closeVodFlyout(){
    var tvSection = $('.tv-section');
        tvSection.css('display','block');
    var flyout = $('.openedFlyout');
      flyout.animate({
        'margin-left': '900',
        opacity: "0"
      },1200, function(){
        flyout.css('display', 'none');
        
      });
};


function manageHash(timeout){
    timeout = typeof timeout !== 'undefined' ? timeout : 0;
    var hash = location.hash.replace('#','');
    var tabAndValue = hash.split('/');
    var tab = tabAndValue[0];
    var id = tabAndValue[1];
    if(tab == 'serie-details'){
        setTimeout(function(){ angular.element('.epg').scope().selectSerieById(id); }, timeout); 
        //angular.element('.epg').scope().selectSerieById(id);
    }else if(tab == 'channel-details'){
        if (! nonrz_to_sponsor){
            openFlyoutTV(id);
        }
    }
    if(hash == ''){
        closeFlyout();
        jQuery('#' + currentId + '_video').empty();
       if(angular.element('.epg').scope()){
            angular.element('.epg').scope().resetVideoContainer();
            angular.element('.epg').scope().$apply();
        }
    }
    if (nonrz_to_sponsor){
        nonrz_to_sponsor = false;
        showFlyout('sponsorFlyout','flyoutHolder', 0,0,1 );

        logPreSplashAd();
        if (tvsponsor != 'abg') {
            sponsorAuthorizedTV();
        }
    }
};

jQuery(window).bind('hashchange',function(event){
    manageHash();
}); 


jQuery(document).ready(function() {
    epgCall();
    manageHash(1000); 
    jQuery("#epg_carousel").css({display:'block'});
    jQuery(".viewer-holder").css({display:'block'});
    
    var watchNowLink = $('.watch-now-action');
    watchNowLink.on('click', function(){
        openFlyoutTV(String(1));
    });

    var channelLink = $('.channels .image-box');

    channelLink.on('click', function(event){
        event.preventDefault();
        var channel_id = $(this).data('channel-id');
        openFlyoutTV(String(channel_id));
        window.location.hash = 'channel-details/'+String(channel_id);
        //window.console && console.log ('channel_id:' + channel_id);
    });

    jQuery('.close').on('click', function(){
        closeFlyout();
        jQuery('#' + currentId + '_video').empty();
    });
    
    var channelLink = jQuery('.slideset .slide .image-box');
    channelLink.on('click', channelClick);
    function channelClick(e){
        var channel_id = jQuery(e.currentTarget).data('channel-id');
        showChannelInformationById(String(channel_id));
    }

});


function showChannelInformationById(id){
    var newSelection = null;
    jQuery('.info-block').children().each(function () {
        var channel = $(this);
        if(channel.attr("id") == id){
            newSelection = channel;
        }
        hideChannel(channel);
    });
    if (newSelection == null){
        newSelection = jQuery('.info-block').children().first();
    }
    showChannelInformation(newSelection);

    function hideChannel(channel){
        removeSelection(channel);
        addHiddenClass(channel);

        function removeSelection(channel){
            var isSelected = channel.attr('selected_id');
            if (typeof isSelected !== typeof undefined && isSelected !== false) {
                channel.removeAttr('selected_id');
            }
        }

        function addHiddenClass(channel){
            var isHidden = channel.hasClass('js-tab-hidden');
            if (!isHidden) {
                channel.addClass('js-tab-hidden');
            }
        }
    }
    function showChannelInformation(channel){
        setSelection(channel);
        removeHiddenClass(channel);

        function setSelection(channel){
            var isSelected = channel.attr('selected_id');
            if (typeof isSelected == typeof undefined || isSelected == false) {
                channel.attr('selected_id', 'True');
            }

            o_epg.eq(0).data('ScrollGallery').refreshState();

            var channelId = channel.attr('id');
            var cindex = channel.attr('cindex');
            var cstep = o_epg.eq(0).data('ScrollGallery').currentStep;
            var nstep = (cindex/3|0) - cstep;
            var next_slide = true;
            if (nstep < 0 ) {
                next_slide = false;
                nstep *= -1;
            }

            jQuery('div#c' + channelId + ' a').click();

            for (i=0;i<nstep;i++) {
                try {
                    if (next_slide) {
                        o_epg.eq(0).data('ScrollGallery').nextSlide();
                    } else {
                        o_epg.eq(0).data('ScrollGallery').prevSlide();
                    }
                } catch(err) { } 
            }
        }

        function removeHiddenClass(channel){
            var isHidden = channel.hasClass('js-tab-hidden');
            if (isHidden) {
                channel.removeClass('js-tab-hidden');
            }
        }
    }
}

function openFlyoutTV(id){
    showChannelInformationById(id);
    showFlyout('epgFlyout','flyoutHolder', 0,0,1 );
};

function sponsorAuthorizedTV() {
    logPostSplashAd();

    jQuery.ajax({
        url: '/watched_sponsor_video.json',
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: 'authTVCallback',
        timeout: 5000,
        success: function(data, textStatus, request) {
        },
        error: function(results) {
        }
    });
}
function authTVCallback(response) {
    // Do nothing
}

function logPreSplashAd() {
    // Log the TV sponsor "pre-splash" ad.
    obj = jQuery("#tvsponsor_pre_mobile");
    if (obj.length > 0) {
        logLocalAd(obj, 'ad_tvsponsor_pre_mobile', 'view');
    }
}
function logPostSplashAd() {
    // Log the TV sponsor "post-splash" ad.
    obj = jQuery("#tvsponsor_post_mobile");
    if (obj.length > 0) {
        logLocalAd(obj, 'ad_tvsponsor_post_mobile', 'view');
        if (tvsponsor != 'abg') {
            obj.click(function(e) {
                var obj = jQuery(this);
                processLocalAdClick(e, obj, obj, 'ad_tvsponsor_post_mobile');
            });
        }
    }
}

