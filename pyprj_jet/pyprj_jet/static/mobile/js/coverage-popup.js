/*
 * Has logic to handle displaying the International coverage popup.
 */

var clicked_coverage_wifi = false;
var clicked_coverage_messaging = false;
var clicked_coverage_movie = false;
var clicked_coverage_tv = false;

function coverageCallback(response) {
    //showFlyout('coverageWarningFlyout','main', 0,0 );
}

function seenCoveragePopup(product) {
    jQuery.ajax({
        url: '/seen_coverage_warning.json?p=' + product,
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: 'coverageCallback',
        timeout: 5000,
        success: function(data, textStatus){
        },
        error: function(results){
        }
    });
}

function hideCallback(popupObject) {
    if (popupObject.options.product) {
        seenCoveragePopup(popupObject.options.product);
    }

    // Remove popup from DOM so it will not be displayed again on this page.
    jQuery(popupObject.options.popup).remove();
}

jQuery(function() {
    jQuery(".buy_coverage_wifi, .buy_coverage_messaging").click(function(e) {
        var product = '';
        var el = jQuery(this);
        //el.unbind('click');

        if (el.hasClass('buy_coverage_wifi')) {
            if (! clicked_coverage_wifi) {
                clicked_coverage_wifi = true;
                product = 'wifi';
            }
            else {
                return true;
            }
        }
        if (el.hasClass('buy_coverage_messaging')) {
            if (! clicked_coverage_messaging) {
                clicked_coverage_messaging = true;
                product = 'messaging';
            }
            else {
                return true;
            }
        }
        if (el.hasClass('buy_coverage_movie')) {
            if (! clicked_coverage_movie) {
                clicked_coverage_movie = true;
                product = 'movie';
            }
            else {
                return true;
            }
        }
        if (el.hasClass('buy_coverage_tv')) {
            if (! clicked_coverage_tv) {
                clicked_coverage_tv = true;
                product = 'tv';
            }
            else {
                return true;
            }
        }

        if (! product) {
            return true;
        }

        e.preventDefault();

        seenCoveragePopup(product);

        // Set the href for the popup's Continue button to the href
        // of the link the user clicked.
        var continueBtn = jQuery('.coverage-popup .btn-buy');
        continueBtn.attr('href', el.attr('href'));
        showFlyout('coverageWarningFlyout','main', 0,0 );
        /*jQuery('.coverage-popup-container').contentPopup({
            popup: '.coverage-popup',
            mode: 'click',
            wifiMode: true,
            display: true,
            hideCallback: 'hideCallback',
            product: product
        });*/
    });

    jQuery(".coverage-popup .btn-cancel").click(function(e) {
        closeFlyout();
        e.preventDefault();
    });

});

