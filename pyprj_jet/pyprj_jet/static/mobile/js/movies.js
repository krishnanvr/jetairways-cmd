function movieGenreSelected(e) {
    var val = jQuery("#movieGenreSelect").val();

    jQuery(".movie-list-all").hide();
    jQuery(".movie-list-" + val).show();

    e.preventDefault();
    return false;
}
function movieGenreCleared(e) {
    jQuery(".movie-list-all").show();

    e.preventDefault();
    return false;
}

// page init
jQuery(function(){
    jQuery("#movieGenreApply").click(movieGenreSelected);
    jQuery("#movieGenreClear").click(movieGenreCleared);

    jQuery(".player-box .close").click(goBack);
});

