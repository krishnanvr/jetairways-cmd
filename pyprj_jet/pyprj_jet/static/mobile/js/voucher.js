
function isNumber(val) {
    try {
        return ! isNaN(val - 0);
    }
    catch(err) {
        return false;
    }
}

function voucherAPICallback(response) {
    //alert('voucherAPICallback ' + response.status + ' ' + response.msg);
    if (response.status == 0) {
        //alert('Voucher Accepted: New price = ' + response.voucherPrice);
        if (isNumber(response.voucherPrice)) {
            jQuery('#voucherAppliedAmt').html('- ' + response.formattedVoucherAmount);
            jQuery('#voucherAppliedDiv').addClass("promo-code").show();
            jQuery('#finalPrice').html(response.formattedPrice);

            if (response.voucherPrice <= 0) {
                jQuery('#voucherInputDiv').hide();
                jQuery('#voucherMsg').hide();

                jQuery('#payment-info-section').hide().removeClass('visible');
                jQuery('#billing-info-section').hide().removeClass('visible');

                jQuery('.notfree').addClass('hiden');

                jQuery('.required-text').removeClass('required-text');
                jQuery('.required-email').removeClass('required-email');
                jQuery('.required-email-optional').removeClass('required-email-optional');
                jQuery('.required-select').removeClass('required-select');
                jQuery('.required-number').removeClass('required-number');
                jQuery('.required-number-optional').removeClass('required-number-optional');

                jQuery('#firstName').addClass('required-text');
                jQuery('#lastName').addClass('required-text');
/*
                jQuery('#ccNoteSpan').remove();
                jQuery('#emailDiv').remove();
                jQuery('#billingDiv').remove();
                jQuery('#paymentDiv').remove();
*/
                jQuery('#pm_cc').remove();
            }
        }
        else {
            jQuery('#voucherMsg').html('<span class="voucher-error">There was an internal error processing the voucher code.</span>').show();
        }
    }
    else {
        jQuery('#voucherMsg').html('<span class="voucher-error">' + response.msg + '</span>').show();
    }

    jQuery("#voucherSubmit").removeAttr('disabled');
    jQuery.uiUnlock();
}

function callVoucherAPI() {
    var voucherCode = jQuery.trim(jQuery("#voucherInput").val())
    //alert('callVoucherAPI: code = ' + voucherCode);
    if (voucherCode != '' && voucherCode != 'optional') {
        jQuery.uiLock('');

        //alert('Calling nubill GetVoucher function');
        jQuery('#voucherMsg').html('<span class="voucher-error">Voucher code is being processed...</span>').show();
        jQuery("#voucherSubmit").attr('disabled', 'disabled');

        var theUrl = location.protocol + '//' + location.host + '/process_voucher_code.json';
        //alert('theUrl = ' + theUrl);

        jQuery.ajax({
            url: theUrl,
            //data: 'v=' + voucherCode + '&p=' + price,
            data: 'v=' + voucherCode,
            dataType: 'jsonp',
            jsonp: 'callback',
            jsonpCallback: 'voucherAPICallback',
            timeout: 90000,
            success: function(){
                //alert("success");
            },
            error: function(results){
                //alert("error " + results.toSource());
                jQuery("#voucherSubmit").removeAttr('disabled');
                jQuery.uiUnlock();
                jQuery('#voucherMsg').html('<span class="voucher-error">There was a problem looking up the voucher code.</span>').show();
            }
        });
    }
    return false;
}

function paymentMethodChanged() {
    radioValue = $(this).val();
//alert('Radio button clicked ' + radioValue);
    if (radioValue == 'pm_pp') {
        jQuery('.paymethod_cc').hide();
        jQuery('.paymethod_cc').removeClass('visible');
    }
    else {
        jQuery('.paymethod_cc').show();
        jQuery('.paymethod_cc').addClass('visible');
        displayCreditCardInputs();
    }
}

/*================================================================================================*/
jQuery(document).ready(function() {

    jQuery('#voucherMsg').hide();
    jQuery('#voucherAppliedDiv').hide();

    jQuery('.info-form #voucherSubmit').click(function(e) {
        callVoucherAPI();

        e.preventDefault();
    });

    // Set event handler for credit card/paypal radio buttons.
    jQuery('.radio-button input[name=paymethod]').change(paymentMethodChanged);

    // Automatically select credit card radio button.
    jQuery(".radio-button input[name=paymethod]:first").prop("checked", true).trigger("click");

});

