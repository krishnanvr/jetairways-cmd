(function(){
  'use strict';
  function EpgFactory($http, $q){
    var video_data = null;
    return {
      getVideoData: function(data, callback){
        var deferred = $q.defer();
        $http.get('/mocks/video_data.json')
          .success(function(result){
            if(result.error){
              deferred.reject(result);
            }else{
              video_data = result;
              deferred.resolve(result);
            }
          })
          .error(function(result, error){
            deferred.reject({error:error, data:result})
          });
          return deferred.promise;
      },
      getVideoPlayerById: function(payload, callback){
        var deferred = $q.defer();
        var params = {params:payload};
        $http.get('/video_load.jq', params)
        .success(function(result){
          if(result.error){
            deferred.reject(result);
          }else{
            deferred.resolve(result);
          }
        })
        .error(function(result, error){
          deferred.reject({error:error, data:result})
        });
        return deferred.promise;
      },
      getEpisodeInfos: function(payload, callback){
        var deferred = $q.defer();
        $http.get('/mocks/episode_info.json', payload)
        .success(function(result){
          if(result.error){
            deferred.reject(result);
          }else{
            video_data = result;
            deferred.resolve(result);
          }
        })
        .error(function(result, error){
          deferred.reject({error:error, data:result})
        });
        return deferred.promise;
      }
    }
  }
  angular.module('epg').factory('EpgFactory', [
    '$http',
    '$q',
    EpgFactory
  ])
})();
