(function(){
  'use strict';
  function EgpController($scope, $sce, $timeout, $window, EpgFactory){
    $timeout(function(){
        var serieId = getUrlParameter('id');
        var tabType = getUrlParameter('tab');

        function getUrlParameter(sParam){
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam){
                    return sParameterName[1];
                }
            }
        };

        if(serieId){
            $scope.selectSerieById(serieId);
        };

    },1000);
    
    $scope.series = {};
    $scope.window = $window;
    $scope.videoData = $window.json_video_data.videoData;
    $scope.series = $scope.videoData.vodtv_data;
    //$scope.videoHtmlContent = '';
    $scope.videoUrl ='';// 'http://getconnected.southwestwifi.com/video_load.jq?cid=1640';
    //EpgFactory.getVideoData()
    //  .then(responseSucces, responseError);

    
    $scope.$on('changeEpisodeSelection', function(event, episode) {
      $scope.getVideoPlayerForEpisodeId($scope.selectedEpisodeId);
    });

    function responseSucces(result){
      $scope.videoData = result.videoData;
      $scope.series = $scope.videoData.vodtv_data;
    }

    function responseError(error){
      //TODO handle error factory
    }

    $scope.selectSerie = function(serie){
      //$scope.openVodFlyout();
      $scope.$emit('changeSerieSelection', serie);
      $scope.window.location.hash = 'serie-details/'+serie.id;
      $scope.openVodFlyout();
    }
    
    $scope.openVodFlyout = function(){
        //$scope.window.openVodFlyout();
        $scope.window.showFlyout('vodFlyout','flyoutHolder', 0,0 );
    }

    $scope.closeVodFlyout = function(){
        $scope.window.closeFlyout();
        $scope.resetVideoContainer();
    }
    
    $scope.resetVideoContainer = function(){
        $scope.videoAsHtml = $sce.trustAsHtml("<div class='videoload'></div>");
    };
    
    $scope.getVideoPlayerForEpisodeId = function(episodeId){
      var payload = {cid:episodeId};
      //$scope.videoUrl = 'http://getconnected.southwestwifi.com/video_load.jq?cid='+episodeId;
      //$scope.window.jcf.customForms.replaceAll();
      //$scope.window.jQuery('.dark')[0].jcf.buildDropdown();

      EpgFactory.getVideoPlayerById(payload)
        .then(responseSucces, responseError);

      function responseSucces(result){
        //$scope.videoHtmlContent = result;
        $scope.videoAsHtml = $sce.trustAsHtml(result);
        $scope.window.jcf.customForms.replaceAll();
        //$scope.window.jQuery('.dark')[0].jcf.buildDropdown()
      }

      function responseError(error){
        //TODO handle error factory
      }
    }
    $scope.getSerieById = function (serieId){
        var serieToReturn = {};
        var keepGoing = true;

        angular.forEach($scope.series,function(serie){
          if(keepGoing && serie.id ==  serieId){
            serieToReturn = serie;
            keepGoing = false;
          }
        })
        return serieToReturn;
      }
    $scope.selectSerieById = function(serieId){
        var serie = $scope.getSerieById(serieId);
        $scope.selectSerie(serie);
    }
  }

  angular.module('epg').controller('EgpController', [
    '$scope',
    '$sce',
    '$timeout',
    '$window',
    'EpgFactory',
    EgpController
  ]);

  function SerieController($scope, $filter, EpgFactory){
    $scope.serie = {};
    $scope.selectedEpisode = {};
    $scope.$on('changeSerieSelection', function(event, serie) {
      $scope.serie = serie;
      if($scope.serie.episodes.length > 0){
        $scope.serie.episodes = $filter('orderBy')($scope.serie.episodes, "id");
        $scope.selectedEpisodeId = $scope.serie.episodes[0].id;
        $scope.selectEpisode();
      }
    });
    
    $scope.selectEpisode = function(){
      var payload = {sid:$scope.selectedEpisodeId,eid:$scope.serie.id};
      $scope.selectedEpisode = getEpisodeById($scope.serie.episodes, $scope.selectedEpisodeId);
      $scope.$broadcast('changeEpisodeSelection', $scope.selectedEpisode);
      //EpgFactory.getEpisodeInfos(payload)
      //  .then(responseSucces, responseError);
      
      $scope.watchStream = function(){
        console.log('clicked'); 
      }

      function responseSucces(result){
        $scope.episode = result;
      }

      function responseError(error){
        //TODO handle error factory
      }

      function getEpisodeById (episodes, episodeId){
        var currentEpisode = {};
        var keepGoing = true;

        angular.forEach(episodes,function(episode){
          if(keepGoing && episode.id ==  episodeId){
            currentEpisode = episode;
            keepGoing = false;
          }
        })
        return currentEpisode;
      }
    }
  }

  angular.module('epg').controller('SerieController', [
  '$scope',
  '$filter',
  'EpgFactory',
  SerieController
  ]);

})();
