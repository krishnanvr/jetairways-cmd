(function(){
  'use strict';
  function ListSeasonNumbers(){
    return function(episodes) {
      var seasonNumberList = '';
      if(episodes){
        episodes.sort(function(obj1, obj2) {
          // Ascending: first age less than the previous
          return obj1.number - obj2.number;
        });
      }
      angular.forEach(episodes,function(episode, index){
        seasonNumberList = seasonNumberList+ episode.number;
        if(index < episodes.length-1){
          seasonNumberList = seasonNumberList+'-'
        }
      })
      return seasonNumberList;
    }
  };

  angular.module('epg').filter('ListSeasonNumbers', [
    ListSeasonNumbers
  ]);

  function Slice(arr, start, end) {
    return function(arr, start, end) {
      return arr.slice(start, end);
    };
  }

  angular.module('epg').filter('slice', [ 
    Slice
  ]);


})();
