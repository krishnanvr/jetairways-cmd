(function(){
  'use strict';
  function EpgDirective(){
    return{
      restrict:'EA',
      replace:'true',
      controller:'SerieController',
      templateUrl: templatesUrl+'epg'
    }
  }
  angular.module('epg').directive('serieView',[
    EpgDirective
  ]);

  function EpisodeInfoDirective(){
    return{
      restrict:'E',
      replace:'true',
      scope:false,
      templateUrl:templatesUrl+'episode-info'
    }
  }

  angular.module('epg').directive('episodeInfo',[
    EpisodeInfoDirective
  ]);
})();
