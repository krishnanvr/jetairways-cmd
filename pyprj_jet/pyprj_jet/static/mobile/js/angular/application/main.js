
var templatesUrl = '/angular-directive/'
angular.module('epg', []);
var app = angular.module('app', ['ngSanitize', 'epg']);
app.config( [
    '$compileProvider',
    function( $compileProvider )
    {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(dtihls):/);
    }
]);
