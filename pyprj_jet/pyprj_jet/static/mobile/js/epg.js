
var ua = navigator.userAgent;
var isIOS = /iPad/i.test(ua) || /iPhone OS/i.test(ua);
var isiPad = /iPad/i.test(ua) || /iPhone OS 3_1_2/i.test(ua) || /iPhone OS 3_2_2/i.test(ua);
var isAndroid = /android/i.test(ua);


function seasonChangeCallback(response) {

    var sel = jQuery('#sel_episode-' + response.sid);
    sel.find('option').remove();

    var eps = response.eps;
    var l = eps.length;
    for (var i = 0; i < l; ++i) {
        number = eps[i][0];
        eid = eps[i][1];
        sel.append('<option value="' + eid + '">' + number + '</option>');
    }

    // Refresh the dropdown display.
    sel[0].jcf.buildDropdown();
    sel[0].jcf.refreshState();

    episodeChangeCallback(response.ep1);
}

function processSeasonEpisodeChange(seriesId, season) {
    var episodeObj = jQuery('#sel_episode-' + seriesId);
    var episodeId = episodeObj.val();
    var theUrl = location.protocol + '//' + location.host + '/episodes_for_season.json';

    jQuery.ajax({
        url: theUrl,
        data: 'sid=' + seriesId + '&s=' + season,
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: 'seasonChangeCallback',
        timeout: 5000,
        success: function(){
        },
        error: function(results){
        }
    });
}

function episodeChangeCallback(response) {
    var sid = response.sid;

    jQuery('#ep_title-' + sid).html(response.title);

    if (response.year) {
        jQuery('#ep_year-' + sid).html(response.year).removeClass('disabled');
    }
    else {
        jQuery('#ep_year-' + sid).addClass('disabled');
    }

    if (response.duration) {
        jQuery('#ep_duration-' + sid).html(response.duration + ' minutes').removeClass('disabled');
    }
    else {
        jQuery('#ep_duration-' + sid).addClass('disabled');
    }

    if (response.summary) {
        jQuery('#ep_summary-' + sid).html(response.summary).removeClass('disabled');
    }
    else {
        jQuery('#ep_summary-' + sid).addClass('disabled');
    }
}

function processSeriesEpisodeChange(seriesId) {
    var episodeObj = jQuery('#sel_episode-' + seriesId);
    var episodeId = episodeObj.val();

    var theUrl = location.protocol + '//' + location.host + '/episode_info.json';

    jQuery.ajax({
        url: theUrl,
        data: 'sid=' + seriesId + '&eid=' + episodeId,
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: 'episodeChangeCallback',
        timeout: 5000,
        success: function(){
        },
        error: function(results){
        }
    });
}

function epgClear() {
    jQuery("div.shedule div").remove()
}

function epgCall() {
    var theUrl = location.protocol + '//' + location.host + '/epg.json';

        jQuery.ajax({
            url: theUrl,
            dataType: 'jsonp',
            jsonpCallback: 'epgCallback',
            error: function(results){
            }
        });
    return false;
}

function epgEmptySlot(o, n) {
    for (var i=0; i<n; i++) {
        o.append(jQuery('<div>', {'class':'broadcast empty'}));
    }
}

function epgCallback(response) {
    if (response.t_refresh) {
        t_refresh = 1000*response.t_refresh
        var i_id=setInterval(function() {
           epgCall();
           clearInterval(i_id);
        }, t_refresh); 
    }
    if (response.current_schedule.length) {
        epgClear();
        jQuery.each(response.current_epg, function(station_id,v) {
                if (v.length) {
                    var s_o=0;
                    jQuery.each(v, function(k1,v1) {
                        // var o=jQuery("li#c"+v1.channelId+" div.shedule");
                        var o=jQuery("div.slideset div#c"+v1.channelId+" div.shedule");
                        var t_o=-1;
                        jQuery.each(response.current_schedule, function(k,vs) {
                            if (vs==v1.startTimeFloor) { t_o=k; }
                        });
                        if (t_o==-1 && !v1.started) {t_o=8;}
                        if(o.length && v1.duration) {
                            if (t_o>0 && !k1) {
                                s_o=t_o;
                                epgEmptySlot(o, t_o);
                            }
                            var prg_class_n=Math.round(v1.duration/30);
                            if (s_o && k1==(v.length-1)) { prg_class_n-=s_o; s_o=0}
                            prg_title=v1.program.title;
                            if (v1.program.eventTitle) { prg_title+=': '+v1.program.eventTitle; }
                            prg_start_time=(v1.startTimeFloor) ? v1.startTimeFloor : '';
                            prg_title="<time>"+prg_start_time+"</time><h3>"+prg_title+"</h3>";
                            o.append(jQuery('<div>', {'class':'broadcast'}).append(prg_title));
                            epgEmptySlot(o, prg_class_n-1);
                        }
                    });
                }
                return
        });
    }
}


/*================================================================================================*/
jQuery(document).ready(function() {



    jQuery('.sel_season').change(function(e) {
		var s = jQuery(this);
        id = s.attr('id');
        var idx = id.indexOf('-');
        var seriesId = id.substring(idx + 1);
        processSeasonEpisodeChange(seriesId, s.val());
        e.preventDefault();
    });

    jQuery('.sel_episode').change(function(e) {
		var s = jQuery(this);
        id = s.attr('id');
        var idx = id.indexOf('-');
        var seriesId = id.substring(idx + 1);
        processSeriesEpisodeChange(seriesId);
        e.preventDefault();
    });

    jQuery('.watch_btn').click(function(e) {
		var b = jQuery(this);
        id = b.attr('id');
        var ids = id.split('-');
        var seriesId = ids[1];
        var episodeId = jQuery('#sel_episode-' + seriesId).val();
        if (ids.length>2) { episodeId+="&vid="+ids[2];}

        // Remove the current video player.
        jQuery('#' + seriesId + '_video').empty();

        // Load the video player.
        jQuery("#" + seriesId + "_video").load("/video_load.jq?cid=" + episodeId + " .videoload", function() {
            jQuery("#" + seriesId + "_nonvideo").hide();
        });

        e.preventDefault();
    });

    jQuery("#cepgad a").unbind("click");  

    jQuery('.epg-series').click(function(e) {
		var b = jQuery(this);
        var id_str = b.attr('href');
        var seriesId = parseInt(id_str.substr(1));
        if (isiPad) $('input[id^="watch_btn-'+seriesId+'"]:first').click();
    });

});


