
/*
 * This function is called when the user switches tabs on the epg page.
 */

function epgTabChange(tabRef, evt) {
    // Remove the current video player.
    jQuery('#' + currentId + '_video').empty();

    var cid = tabRef.attr('href').substring(1);

/*    if (cid != '20115107' && nonrz_to_sponsor) {
        // Manually load sponsor page.
 //       window.location.assign('/sponsor?tab=iptv&id=' + cid);
        window.console && console.log ("epgTabChange: return false");
        return false;
    }
*/
    // Move ads to the appropriate ad placeholder.
    jQuery('#ad_big').appendTo('#' + cid + '_ad_spot');
    jQuery('#ad_dish').appendTo('#' + cid + '_ad_spot');

    // Load the video player.
    jQuery("#" + cid + "_video").load("/video_load.jq?cid=" + cid + " .videoload", function() {
        if (!isAndroid && !isIOS) {
            get_channel(curIptvId);
        }
    });

    currentId = cid;
    curIptvId = cid;
    return true;
}
