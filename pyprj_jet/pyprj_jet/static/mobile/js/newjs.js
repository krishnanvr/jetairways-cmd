// Term Of Use and Privacy Policy

var TOULink = $(".TOULink");
var PPLink = $(".PPLink");
var TOU = $(".TOU");
var PP = $(".PP");


TOULink.on( "click",function(){
	PPLink.removeClass("selected");
	PP.removeClass("selected");
	TOULink.addClass("selected");
	TOU.addClass("selected");
});

PPLink.on( "click",function(){
	TOULink.removeClass("selected");
	TOU.removeClass("selected");
	PPLink.addClass("selected");
	PP.addClass("selected");
});

$(document).ready(function(){

$('.terms-link').on('click',function(){
    $('body').css('overflow','visible');
});
$('.privacy-link').on('click',function(){
    $('body').css('overflow','visible');
});
$('#termsTabModal .btn-cancel-register').on('click',function(){
    $('body').css('overflow','hidden');
});
$('#privacyTabModal .btn-cancel-register').on('click',function(){
    $('body').css('overflow','hidden');
});


	$("body").not(".question-mark-email").not(".question-mark-access").on("click",function(){
		$(".tooltip-email").removeClass("tooltip");
		$(".tooltip-access").removeClass("tooltip");
	});
	$(".question-mark-email").on("click",function(){
		$(".tooltip-email").toggleClass("tooltip");
		event.stopPropagation();
	});
	$(".question-mark-access").on("click",function(){
		$(".tooltip-access").toggleClass("tooltip");
		event.stopPropagation();
	});
});
