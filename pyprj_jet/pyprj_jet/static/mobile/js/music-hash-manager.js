function manageHash(timeoutValue, hashValue){
    var timeout = typeof timeoutValue !== 'undefined' ? timeoutValue : 0;
    var hash = typeof hashValue !== 'undefined' ? hashValue : location.hash;
    hash = hash.replace('#', '');
    var tabAndValue = hash.split('/');
    var tab = tabAndValue[0];
    var id = tabAndValue[1];
    if(tab == 'music-playlist'){
        //window.console && console.log('manageHash: music playlist open');
        var playlistElement = jQuery('.'+id)[0];
        var playlistLink = jQuery(playlistElement).find('.playlist-dropdown-click-area')[0];
        var browserPlaylistTab = jQuery('.browse-playlist')[0];
        jQuery(browserPlaylistTab).trigger( "click" );
        jQuery(playlistLink).trigger( "click" );
    };
    if(hash == ''){
       jQuery( ".close-playlists-dropdown" ).trigger( "click" );
    };
};

jQuery(window).bind('hashchange',function(event){
   manageHash();
});

