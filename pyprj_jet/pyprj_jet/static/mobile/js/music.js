/** Music player interface scripts */

var beats = {};

// Note: These constant values are also defined in beats.py, so if any of them
//       are changed, change them in beats.py as well.
beats.STOPPED  = 0;
beats.PLAYING  = 1;
beats.PAUSED   = 2;

beats.BEATS_RESULT_NO_SONGS_LEFT = 50;
beats.BEATS_RESULT_NO_PLAYLIST   = 60;

beats.BEATS_AVAILABILITY_OK                      = 0;
beats.BEATS_AVAILABILITY_INTL_ASCENDING          = 100;
beats.BEATS_AVAILABILITY_INTL_DESCENDING_WARNING = 110;
beats.BEATS_AVAILABILITY_INTL_DESCENDING         = 120;

beats.currentPlaylist = '';
beats.currentTrack    = '';
beats.currentState    = beats.STOPPED;
beats.lastPlaylist    = '';
beats.lastTrack       = '';

beats.playlist        = {};
beats.trackindex      = 0;
beats.currentImage    = '';
beats.currentTitle    = '';
beats.currentAlbum    = '';
beats.currentArtists  = '';
beats.currentUrl      = '';

beats.savedHash       = '';

beats.dec = function() {
    return (typeof beats.songs_left !== 'undefined') ? --beats.songs_left : 0;
}

jQuery(document).ready(function() {
    if (typeof beats.myCirclePlayer === 'undefined') {
        initNonMusicPage();
    }
    else {
        initMusicPage();
    }
});

var regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var regPhone = /^([0-9\s\-\)\(])+$/;

function validateEmail(email) {
    var valid = regEmail.test(email);
    return valid;
}

function initMusicPage() {
	jQuery.fn.extend({
		openModal: function(){
			return this.each(function(){
				jQuery(this).css({
					left: 'auto',
					right: -$(window).innerWidth() + 'px',
					opacity: 1
				}).show();
				jQuery(this).animate({ right: 0 }, "slow", function(){
					// callback
				});
				//myCirclePlayer.updatePlayerElements();
			});
		},
		closeModal: function(fn_callback){
            if (typeof fn_callback == 'function') {
                fn_callback();
            }
			return this.each(function(){
                window.location.hash = '';
				jQuery(this).fadeOut();
			});
		}
    
	});

    updateDisplays();

    // Set up the myCirclePlayer controls.  Note that the node (inv_container) must match 
    // the node used to setup the JW Player in music.pt.
    beats.myCirclePlayer = new CirclePlayer("inv_container", circleCallbacks);

    updateSongsLeftCircle(beats.songs_left, beats.songs_max);

    if (beats.showBeatsUnb) {
        // Display the music section in the header.
        jQuery('#header-nav-music-item').show();
    }

    beats.currentGenre = 'genre-all';
    beats.allPlaylistsLoaded = true;

    // Header music icon.
	jQuery('body').on('click', '#music-nav-btn', function(e) {
        if (beats.intl != beats.BEATS_AVAILABILITY_INTL_DESCENDING_WARNING && beats.currentState != beats.STOPPED) {
            jQuery('#header-nav-music-item').toggleClass('active');
            jQuery('body').toggleClass('bar-player-active');
        }
        else {
            jQuery('#header-nav-music-item').removeClass('active');
            jQuery('body').removeClass('bar-player-active');
        }
	});

    jQuery('.music_exit_popup').click(function(e) {
        if (beats.currentState == beats.STOPPED) {
            return true;
        }

        if (beats.seenExitModal) {
            // Only show the modal once.
            return true;
        }


        var currentTarget = jQuery(e.currentTarget);
        currentTarget.attr('target', 'r44portal');

        // Hide dropdown menu.
        jQuery('#r44universalnavbar .dropdown').hide();

        jQuery('#newTabModal').show();

       jQuery('#leaving-page-link').attr('href', currentTarget.attr('href'));
       jQuery('#leaving-page-link').attr('target', 'r44portal');
       jQuery('#leaving-page-link').click(function(e) {
            jQuery('#newTabModal').hide();
        });

        beats.seenExitModal = true;

        // Let server know the exit modal has been displayed (so that
        // it will not be displayed again in the future).
        jQuery.ajax({
            url: '/beats_song_op.json?op=seenexitmodal',
            dataType: 'json',
            headers : { "cache-control": "no-cache" },
            timeout: 2000,
            success: function(data, textStatus, request) {
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
            }
        });

        return false;
    });

    jQuery('.terms-link').click(function(e) {
        var node = jQuery("#termsTabModal .tpLoad");
        if (node.length) {
            node.load("/beats_popup_load.jq?p=beats-terms-of-use-popup", function() {
                node.removeClass('tpLoad'); // To avoid loading content again
                jQuery("#termsTabModal").show();
            });
        }
        else { // Content has already been loaded
            jQuery("#termsTabModal").show();
        }
        return false;
    });
    jQuery('.privacy-link').click(function(e) {
        var node = jQuery("#privacyTabModal .tpLoad");
        if (node.length) {
            node.load("/beats_popup_load.jq?p=beats-privacy-policy-popup", function() {
                node.removeClass('tpLoad'); // To avoid loading content again
                jQuery("#privacyTabModal").show();
            });
        }
        else { // Content has already been loaded
            jQuery("#termsTabModal").show();
        }
        return false;
    });

    jQuery('body').on('click', '.playlist-featured', function(e){
        var target = jQuery(e.currentTarget);
        var playlist_id = target.data('plid');
        jQuery('body').css('overflow','hidden');
		jQuery('.playlist-info').removeClass('active');
		if (typeof playlist_id != "undefined") {
		    get_playlist_info(playlist_id, false);
		}
	});

	jQuery('body').on('click', '.playlist-thumb', function(e){
		$('body').css('overflow','hidden');
        var playlist_id = jQuery(e.currentTarget).closest('.playlist').attr('data-playlist-id');
        if (beats.currentState == beats.PLAYING && playlist_id == beats.currentPlaylist) {
            beats.myCirclePlayer.player.pause();
        }
        else if (beats.currentState == beats.PAUSED && playlist_id == beats.currentPlaylist) {
            beats.myCirclePlayer.player.play();
        }
		else if (typeof playlist_id != "undefined") {
		    jQuery('.' + playlist_id + ' .playlist-info').addClass('pressed').toggleClass('active');
		    jQuery('.playlist-info:not(.pressed)').removeClass('active');
		    jQuery('.' + playlist_id + ' .playlist-info').removeClass('pressed');

            // By passing true to get_playlist_info, it will cause the first
            // track to be played when the playlist is loaded.
            get_playlist_info(playlist_id, true);
		}
	});

	jQuery('body').on('click', '.playlist-item-play-pause', function(e){
		$('body').css('overflow','hidden');
        var playlist_id = jQuery(e.currentTarget).closest('.playlist').attr('data-playlist-id');
        if (beats.currentState == beats.PLAYING && playlist_id == beats.currentPlaylist) {
            beats.myCirclePlayer.player.pause();
        }
        else if (beats.currentState == beats.PAUSED && playlist_id == beats.currentPlaylist) {
            beats.myCirclePlayer.player.play();
        }
		else if (typeof playlist_id != "undefined") {
		    jQuery('.' + playlist_id + ' .playlist-info').addClass('pressed').toggleClass('active');
		    jQuery('.playlist-info:not(.pressed)').removeClass('active');
		    jQuery('.' + playlist_id + ' .playlist-info').removeClass('pressed');
            // By passing true to get_playlist_info, it will cause the first
            // track to be played when the playlist is loaded.
            get_playlist_info(playlist_id, true);
		}

    });
	jQuery('body').on('click', '.playlist-dropdown-click-area', function(e){
        $('body').css('overflow','hidden');
		var playlist_id = jQuery(e.currentTarget).closest('.playlist').attr('data-playlist-id');
		jQuery(e.currentTarget).closest('.playlist-info').addClass('pressed').toggleClass('active');
		jQuery('.playlist-info:not(.pressed)').removeClass('active');
		jQuery(e.currentTarget).closest('.playlist-info').removeClass('pressed');
		if (typeof playlist_id != "undefined") {
		    get_playlist_info(playlist_id, false);
		}
	});
	jQuery('body').on('click', '.close-playlists-dropdown', function(e){

         $('body').css('overflow','auto');
		jQuery(e.currentTarget).closest('.playlist-info').removeClass('active');

        // Hide and move the playlist control so it can be re-used.
        detachPlaylistControl();

		jQuery('#modal').closeModal();
	});
	$('body').on('click', '.next-song-btn.enabled', beatsNextBtnClicked);
	$('body').on('click', '.playlist-dropdown .playlist-track[data-track-id]', function(e){
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
		var playlistId = $(e.currentTarget).closest('.playlist-dropdown').attr('data-playlist-id') || null;
		var trackId = $(e.currentTarget).attr('data-track-id') || null;
		var trackIndex = $(e.currentTarget).attr('data-track-index') || null;
        // Play the selected track.
        var trackData = { 'playlistId': playlistId, 'trackId': trackId, 'trackIndex': trackIndex };
        beatsPlayTrack(trackData);
	});

	jQuery('body').on('click', '.playlist-item-play', function(e){
            var fip = jQuery(e.currentTarget).closest('.featured-item-play');
            if (fip.length > 0) {
                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();

		        $('body').css('overflow','hidden');
                var playlist_id = jQuery(e.currentTarget).closest('.playlist-featured').attr('data-plid');
                if (beats.currentState == beats.PLAYING && playlist_id == beats.currentPlaylist) {
                    beats.myCirclePlayer.player.pause();
                }
                else if (beats.currentState == beats.PAUSED && playlist_id == beats.currentPlaylist) {
                    beats.myCirclePlayer.player.play();
                }
		        else if (typeof playlist_id != "undefined") {
		            jQuery('.playlist-info').removeClass('active');

                    // By passing true to get_playlist_info, it will cause the first
                    // track to be played when the playlist is loaded.
                    get_playlist_info(playlist_id, true);
		        }

                return false;
            }

            // Start playing the first track.
            var trackNodes = jQuery(".playlist-tracklist").find('.track-title-and-artists')
            if (trackNodes.length > 0) {
                trackNodes[0].click();
            }
	});
	$('body').on('click', '.playlist-track .track-title-and-artists', function(e){
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
		var playlistId = $(e.currentTarget).closest('.playlist-dropdown').attr('data-playlist-id') || null;
		var trackId = $(e.currentTarget).closest('.playlist-track').attr('data-track-id') || null;
		var trackIndex = $(e.currentTarget).closest('.playlist-track').attr('data-track-index') || null;
        // Play the selected track.
        var trackData = { 'playlistId': playlistId, 'trackId': trackId, 'trackIndex': trackIndex };
        beatsPlayTrack(trackData);
	});

	jQuery('body').on('click', '#btn-start-reg, #beatsLoginPage a.signup', function(e){
		jQuery('#registerPage .ended').hide();
		jQuery('#registerPage .user').show();
	});

	jQuery('body').on('click', '#btn-cancel-reg', function(e){
        return false;
	});
     jQuery('body').on('focus', '#registerPage #email', function(e) {
        jQuery(this).css('color', '#6a6a6a');
        jQuery(this).val('');
    });

	jQuery('#registerPage .sign_up_form').submit(function(e){
        e.preventDefault();
        var emailObj = jQuery('#registerPage .sign_up_form #email');
        var email = emailObj.val().trim();

        var isEmailValid = validateEmail(email);

        if (! isEmailValid) {
            emailObj.css('color', 'red');
            emailObj.val('Invalid email address, please enter again.');
        }
        else{
            closeFlyout();
            if (beats.savedHash.length > 1) {
                manageHash(0, beats.savedHash);
                beats.savedHash = '';
            }

            beatsSignupNotify(email, '');
	    }
    });

	jQuery('body').on('click', '#force-reg-login', function(e){
        jQuery('#beatsLoginPage #errorMsgMisc, #beatsLoginPage #errorMsgExc1, #beatsLoginPage #errorMsgExc2').hide();
	});

	jQuery('body').on('click', '#btn-cancel-login', function(e){
        return false;
	});
	jQuery('#beatsLoginPage .login_form').submit(function(e){
        e.preventDefault();

        var username = jQuery('#beatsLoginPage .login_form #username').val().trim();
        if (username == 'Email Address or Username') {
            username = '';
        }

        var password = jQuery('#beatsLoginPage .login_form #password').val().trim();
        if (password == 'Password') {
            password = '';
        }

        if (username && password) {
            jQuery('#beatsLoginPage #errorMsgMisc').hide();
            beatsLoginNotify(username, password);
        }
        else {
            jQuery('#beatsLoginPage #errorMsgMisc').text('Please enter a username and password.');
            jQuery('#beatsLoginPage #errorMsgMisc').show();
        }
	});

    jQuery('#musicGenreSelect').change(function() {
        genreChanged(this);
    });

    // If the Browse tab should be displayed first, "click" the link that causes it to display.
    if (beats.first_tab == 'browse') {
        jQuery('#beats-link-playlist').click();
    }

    // If a playlist was specified, "click" the link that causes its dropdown to display.
    if (beats.first_pid) {
        jQuery('.' + beats.first_pid + ' .playlist-dropdown-click-area').click();
        beats.first_pid = '';
    }
}

function initNonMusicPage() {
    jQuery('.dropdown-player').hide();

    if (beats.showBeatsUnb) {
        // Display the music section in the header.
        jQuery('#header-nav-music-item').show();
    }
}

/* **************************************** */

function circleCallbacks(eventName) {

    if (eventName == 'play') {
        beats.currentState = beats.PLAYING;
        update_playlist_area();
    }
    else if (eventName == 'pause') {
        beats.currentState = beats.PAUSED;
        update_playlist_area();
    }
    else if (eventName == 'complete') {
        // Start the next track.
        beatsSongOperation('done', null);
    }
    else if (eventName == 'counted') {
        // Decrement the available songs count.
        beatsSongOperation('dec', null);
    }
}

/* **************************************** */

function beatsSongOperation(op, trackData) {
    var params = '';
    var callback = '';

    if (beats.intl == beats.BEATS_AVAILABILITY_INTL_DESCENDING || beats.intl == beats.BEATS_AVAILABILITY_INTL_ASCENDING) {
        updateDisplays();
        return;
    }

    if (op == 'track') {
        callback = 'beatsTrackCallback';

        if (trackData.playlistId == beats.currentPlaylist && trackData.trackId == beats.currentTrack) {
            if (beats.currentState == beats.PLAYING) {
                beats.myCirclePlayer.player.pause();
            }
            else {
                beats.myCirclePlayer.player.play();
            }

            return false;
        }

        server_op = 'trackplaylist';

        params = '?op=' + server_op + '&plid=' + trackData.playlistId + '&idx=' + trackData.trackIndex;
        beats.myCirclePlayer.player.stop();
    }
    else if (op == 'skip' || op == 'done') {
        callback = 'beatsNoopCallback';

        params = '?op=' + op;
    }
    else if (op == 'dec') {
        update_song_count();
        callback = 'beatsDecrementCallback';

        params = '?op=dec';
    }

    if (beats.currentState == beats.PLAYING || beats.currentState == beats.PAUSED) {
        var cur_tid = beats.currentTrack;
        var cur_pid = beats.currentPlaylist;
        var cur_pos = Math.round(beats.myCirclePlayer.player.getPosition());
        var dur = Math.round(beats.myCirclePlayer.player.getDuration());
        if (op == 'done' || cur_pos > dur) {
            cur_pos = dur;
        }

        params = params + '&cur_tid=' + cur_tid + '&cur_pid=' + cur_pid + '&cur_pos=' + cur_pos;

        if (op != 'dec') {
            beats.myCirclePlayer.player.stop();
            beats.myCirclePlayer._timeupdate(0); 
            beats.currentTrack = '';
            beats.currentState = beats.STOPPED;
            update_playlist_area();
        }
    }

    if (op == 'skip' || op == 'done') {
        var c = (is_allowed()) ? false : (set_next_track(), no_songs_left(), true);
        if (c || beats.intl == beats.BEATS_AVAILABILITY_INTL_ASCENDING || beats.intl == beats.BEATS_AVAILABILITY_INTL_DESCENDING) { 
            updateDisplays();
            return;
        }
        else {
            play_track(true);
                var trackList = $('.playlist-tracklist').find('.playlist-track');
                for(var i = 0; i < trackList.length; i++){
                    if(trackList.eq(i).attr('data-track-id') == beats.currentTrack){
                        trackList.eq(i).click();
                        break;
                    }
                }
            
        }
    }

    jQuery.ajax({
        url: '/beats_song_op.json' + params,
        dataType: 'jsonp',
        jsonp: 'callback',
        headers : { "cache-control": "no-cache" },
        jsonpCallback: callback,
        timeout: 10000,
        success: function(data, textStatus, request) {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

function update_playlist_area() {
    // Make sure none of the areas are active.
/* new flow */
/*
    jQuery('.playlist-item-play-pause').removeClass('active');

    // Make the current playlist area active.
    if (beats.currentPlaylist && beats.currentState == beats.PLAYING) {
        jQuery('li.' + beats.currentPlaylist + ' .playlist-item-play-pause').addClass('active');
    }
*/
}

function update_dropdown_player() {
    jQuery('.dropdown-player .song-title').html(beats.currentTitle);
    jQuery('.dropdown-player .album').html(beats.currentAlbum);
    jQuery('.dropdown-player .artists').html(beats.currentArtists);
    jQuery('.dropdown-player .song-thumbnail').css('background-image', 'url(' + beats.currentImage + ')');
    jQuery('.dropdown-player .half-top').show();
}

function play_track(next_track) {
    if (next_track) {
        if (! set_next_track()) {
            return false;
        }
    }
    else if (beats.playlist && beats.playlist.tracks) {
        set_track_data(beats.playlist.tracks[beats.trackindex]);
    }

    update_dropdown_player();

    if (! is_allowed()) {
        jQuery('.next-song-btn').removeClass('enabled');
        return false;
    }

    beats.myCirclePlayer.player.loadFile(beats.currentUrl);
    beats.myCirclePlayer.player.setTitle(beats.currentTitle);
    beats.myCirclePlayer.player.play();
    jQuery('.next-song-btn').addClass('enabled');

    attachPlaylistControl();

    return true;
}

function set_next_track() {
    try {
        beats.trackindex++;
        if (beats.trackindex >= beats.playlist.tracks.length) {
            beats.trackindex=0;
        }
        set_track_data(beats.playlist.tracks[beats.trackindex]);

        return true;
    }
    catch (err) {
        return false;
    }
}

function set_track_data(trackdata) {
    beats.currentTrack = trackdata.trid;
    beats.lastTrack = trackdata.trid;
    beats.currentImage = trackdata.trimage;
    beats.currentTitle = trackdata.trtitle;
    beats.currentAlbum = trackdata.tralbum;
    beats.currentArtists = trackdata.trartists;
    beats.currentUrl = trackdata.trurl;
}

function set_playlist_data(response) {
    beats.trackindex = 0;
    var trackdata = response.tracks[beats.trackindex];
    beats.playlist = response;
    beats.currentPlaylist = response.plid;
    beats.lastPlaylist = response.plid;
    set_track_data(trackdata);
}

function no_songs_left() {
    beats.myCirclePlayer.player.stop();
    //beats.currentPlaylist = '';
    beats.currentTrack = '';
    beats.currentUrl = '';
    beats.currentState = beats.STOPPED;
    update_playlist_area();
    jQuery('.dropdown-player .half-top').hide();
/* TLG
    jQuery('#header-nav-music-item').toggleClass('active', false);
    jQuery('#display-playlist-area').hide();
    jQuery('#edit-playlist-area').show();
    jQuery('#force-reg-link').click();
*/
    jQuery('.next-song-btn').removeClass('enabled');

    updateSavedHash();

	jQuery('#modal').closeModal();

    // Hide main page and show registration page.
    //jQuery('#btn-start-reg').click();
    showFlyout('registerFlyout','flyoutHolder', 0,0 );
}

function updateSavedHash() {
    if (! beats.savedHash) {
        hashValue = window.location.hash;
        var idx = hashValue.indexOf('music-playlist/');
        if (idx >= 0) {
            beats.savedHash = hashValue;
        }
        else {
            beats.savedHash = '#';
        }
    }
}

function is_allowed() {
    return (beats.songs_left > 0 || beats.unlocked) ? true : false;
}

function genreChanged(node) {
    if (node.value != beats.currentGenre) {
        beats.currentGenre = node.value;

        var selNodes = jQuery('.' + node.value);
        if (node.value == 'genre-all') {
            jQuery('.genre-all').show();
        }
        else {
            jQuery('.genre-all').hide();
            selNodes.show();
        }
    }
}

function beatsNoopCallback(response) {
}

function beatsTrackCallback(response) {
    beats.intl = response.result_code;
    if (response.result == 'ok') {
        if (beats.currentPlaylist != response.plid || beats.currentTrack != response.trid) {
            set_playlist_data(response);


            beats.myCirclePlayer.player.loadFile(beats.currentUrl);
            beats.myCirclePlayer.player.setTitle(beats.currentTitle);
            beats.myCirclePlayer.player.play();

            update_playlist_area();
            update_dropdown_player(); 
            updateDisplays();
/* TLG
            attachPlaylistContainer();
*/

            // Display the music section in the header.
            jQuery('#header-nav-music-item').show();
        }
        else {

            beats.myCirclePlayer.player.pause();
        }
    }
    else if (response.result_code == beats.BEATS_RESULT_NO_SONGS_LEFT) {
        set_playlist_data(response);
        no_songs_left();
    }
    else if (response.result_code == beats.BEATS_AVAILABILITY_INTL_ASCENDING || response.result_code == beats.BEATS_AVAILABILITY_INTL_DESCENDING) {
        updateDisplays();
    }
    else {
    }
}

/* **************************************** */

function beatsDecrementCallback(response) {
}

function update_song_count() {
    var v = (is_allowed()) ? beats.dec() : 0;
    v = (v <= 0) ? 0 : v;
    if (typeof beats !== 'undefined') { updateSongsLeftCircle(v, beats.songs_max); }
    jQuery('.song-left-count').html((v > 0) ? v : '!');
}

/*
 * Plays the track specified by the trackData parameter.
 *
 * Parameters: trackData - an object with the following fields:
 *                playlistId
 *                trackId
 *                trackIndex
 */
function beatsPlayTrack(trackData) {

    if (beats.intl == beats.BEATS_AVAILABILITY_INTL_DESCENDING || beats.intl == beats.BEATS_AVAILABILITY_INTL_ASCENDING) {
        updateDisplays();
    }
    else {
/*
        if (trackData.trackId != beats.currentTrack) {
        }
*/
            attachPlaylistControl(trackData);

        beatsSongOperation('track', trackData);
    }
}

function beatsNextBtnClicked(e) {
    e.preventDefault();

    if (beats.intl == beats.BEATS_AVAILABILITY_INTL_DESCENDING || beats.intl == beats.BEATS_AVAILABILITY_INTL_ASCENDING) {
        //displayRightHalfTab('play_sentence', false);
        updateDisplays();
    }
    else {
        beatsSongOperation('skip', null);
    }
}


function loadNewPlaylist(newPlaylist, track_index){
	var newPlaylist = newPlaylist || currentPlaylist; // if no playlist is given use the current one
	var track_index = track_index || 0; // defaults to zero if not given as a parameter

	// if the new playlist is the same as the current one:
	if (newPlaylist.playlist_id == currentPlaylist.playlist_id) {
		
		var currentIndex = myCirclePlayer.player.getPlaylistIndex() || false;
		
		// if the current song is the requested song:
		if (currentIndex != track_index) {
			// plays the requested track index
			myCirclePlayer.player.playlistItem(track_index);
		}
	} else {
		
		// updates currentPlaylist variable:
		currentPlaylist = newPlaylist;
		
		// loads the new playlist into the player
		jwplayer("musicPlayer").load(newPlaylist.tracks);

		// starts the playlist at the requested index
		myCirclePlayer.player.playlistItem(track_index);
		
	}

}

function beatsPlaylistInfoCallbackAndPlay(response) {
   // if (beatsPlaylistInfoCallback(response)) {
        // Play the first track.
        var track = response.result.tracks[0];
        var trackData = { 'playlistId': response.result.id, 'trackId': track.id, 'trackIndex': 0 };
        beatsPlayTrack(trackData);
        //$('.playlist-tracklist .playlist-track:first-of-type .track-music-id').css('opacity',0);
    //}
}
function beatsPlaylistInfoCallback(response) {

    if (response.result_code == beats.BEATS_AVAILABILITY_OK) {
        // populate the modal content using the playlist information:
        /* move the player control first,  otherwise ,_template will overwrite it */
        detachPlaylistControl();
        var modalHtml = _.template($('#playlist-modal-template').html(), {
            "playlist": response.result
        });
        $('#modal-content').html(modalHtml);
        // Display the modal.
        $('#modal').openModal();

        if (beats.currentState == beats.PLAYING || beats.currentState == beats.PAUSED) {
            attachPlaylistControl();
        }

        window.location.hash = 'music-playlist/'+String(response['result']['id']);

        return true;
    }
    return false;
}

function get_playlist_info(playlist_id, play_first_track) {
    var callbackUrl = 'beatsPlaylistInfoCallback'
    if (play_first_track) {
        callbackUrl = 'beatsPlaylistInfoCallbackAndPlay'
    }
    jQuery.ajax({
        url: '/beats_get_playlist.json?plid=' + playlist_id,
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: callbackUrl,
        headers : { "cache-control": "no-cache" },
        timeout: 10000,
        success: function(data, textStatus, request){
        },
        /*error: function(results){ */
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            jQuery.fancybox.close();
        }
    });
}

/* **************************************** */

function beatsSignupCallback(response) {
    jQuery('.song-left-display').hide();

    beats.unlocked = true;

    if ((beats.intl == beats.BEATS_AVAILABILITY_OK || beats.intl == beats.BEATS_AVAILABILITY_INTL_DESCENDING_WARNING) && beats.currentState == beats.STOPPED) {
        play_track(false);

    }

    updateDisplays();

	jQuery('#registerPage').hide();
	jQuery('#mainPage').show();
}

function beatsSignupNotify(email, phone) {
    var params = '?e=' + email;
    if (phone) {
        params = params + '&p=' + phone;
    }
    if (beats.lastTrack) {
        params = params + '&lt=' + beats.lastTrack;
    }
    if (beats.lastPlaylist) {
        params = params + '&lp=' + beats.lastPlaylist;
    }

    jQuery.ajax({
        url: '/beats_signup.json' + params,
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: 'beatsSignupCallback',
        headers : { "cache-control": "no-cache" },
        timeout: 10000,
        success: function(data, textStatus, request){
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            jQuery('.song-left-display').hide();
        }
    });
}

/* **************************************** */

function cancelRegistration() {
    closeFlyout();
    if (beats.savedHash.length > 1) {
        manageHash(0, beats.savedHash);
        beats.savedHash = '';
    }
}

function beatsLoginCallback(response) {

    jQuery('#beatsLoginPage .login_form .btn-register').removeClass('disabled');
    if (response.result == 'ok') {
        beats.unlocked = true;

        if (beats.intl == beats.BEATS_AVAILABILITY_OK || beats.intl == beats.BEATS_AVAILABILITY_INTL_DESCENDING_WARNING && beats.currentState == beats.STOPPED) {
            play_track(false);

        }

        update_playlist_area();
        update_dropdown_player(); 
        updateDisplays();

        closeFlyout();
        jQuery('.song-left-display').hide();
        jQuery('#beatsLoginPage #errorMsgExc1, #beatsLoginPage #errorMsgExc2, #beatsLoginPage #errorMsgMisc').hide();
		jQuery('#beatsLoginPage').hide();
	    jQuery('#registerPage').hide();
	    jQuery('#mainPage').show();

        if (beats.savedHash.length > 1) {
            manageHash(0, beats.savedHash);
            beats.savedHash = '';
        }
    }
    else if (response.result == 'exc') {
        jQuery('#beatsLoginPage #errorMsgMisc').hide();
        jQuery('#beatsLoginPage #errorMsgExc1, #beatsLoginPage #errorMsgExc2').show();
    }
    else {
        // Login failed.
        jQuery('#beatsLoginPage #errorMsgExc1, #beatsLoginPage #errorMsgExc2').hide();
        jQuery('#beatsLoginPage #errorMsgMisc').text(response.result);
        jQuery('#beatsLoginPage #errorMsgMisc').show();
    }
}

function beatsLoginNotify(username, password) {
    // Ignore button click if already in the middle of processing.
    if (jQuery('#beatsLoginPage .login_form .btn-register').hasClass('disabled') == true) {
        return;
    }
    jQuery('#beatsLoginPage .login_form .btn-register').addClass('disabled');

    jQuery('#beatsLoginPage #errorMsgExc1, #beatsLoginPage #errorMsgExc2, #beatsLoginPage #errorMsgMisc').hide();

    var cur_tid = beats.currentTrack;
    var cur_pid = beats.currentPlaylist;
    var cur_pos = 0;
    if (beats.currentState == beats.PLAYING || beats.currentState == beats.PAUSED) {
        var cur_pos = Math.round(beats.myCirclePlayer.player.getPosition());
        var dur = Math.round(beats.myCirclePlayer.player.getDuration());
        if (cur_pos > dur) {
            cur_pos = dur;
        }
    }

    params = '&cur_tid=' + cur_tid + '&cur_pid=' + cur_pid + '&cur_pos=' + cur_pos;

    var url = 'https://' + window.location.host + '/beats_login.json?u=' + username + '&p=' + password + params;

    jQuery.ajax({
        url: url,
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: 'beatsLoginCallback',
        headers : { "cache-control": "no-cache" },
        timeout: 60000,
        success: function(data, textStatus, request){
        },
        /*error: function(results){*/
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            jQuery('#beatsLoginPage .login_form .btn-register').removeClass('disabled');
            jQuery('#beatsLoginPage #errorMsgMisc').hide();
            jQuery('#beatsLoginPage #errorMsgExc1, #beatsLoginPage #errorMsgExc2').show();
        }
    });
}

/* **************************************** */

function attachPlaylistControl(trackData) {
    var theTrackId = beats.currentTrack;
    var thePlaylistId = beats.currentPlaylist;
    if ( typeof trackData != 'undefined' ) {
        theTrackId = trackData.trackId; 
        thePlaylistId = trackData.playlistId;
    }

    var selNode = jQuery("li[data-track-id='" + theTrackId + "']");

    if (selNode.length == 1) {
           jQuery('#track-cp-container').detach().appendTo(selNode);
           jQuery('#track-cp-container').show();
    }

    var featured = false;
    var currentTarget = jQuery('.' + thePlaylistId + ' .playlist-item-play-pause');
    if (currentTarget.length == 0) {
        currentTarget = jQuery('.playlist-featured.' + thePlaylistId + ' .featured-item-play');
        featured = true;
    }

    if (currentTarget.length > 0) {
        if (featured) {
            jQuery('.playlist-featured .featured-item-play .playlist-item-play').hide();
        }

        jQuery('#playlist-cp-container').detach().appendTo(currentTarget);
        jQuery('#playlist-cp-container').show();

        if (! featured) {
            jQuery('.playlist-featured .featured-item-play .playlist-item-play').show();
        }
    }
    else {
        jQuery('#playlist-cp-container').hide();
        jQuery('#playlist-cp-container').detach().appendTo(jQuery("div#wrapper"));
        jQuery('.playlist-featured .featured-item-play .playlist-item-play').show();
    }

    jQuery('.playlist-item-play-pause').removeClass('active');
    currentTarget.addClass('active');

    var currentCoverTarget = jQuery('.playlist-dropdown[data-playlist-id="' + thePlaylistId + '"] .playlist-image');
    if (currentCoverTarget.length > 0) {
        jQuery('#playlist-image-container').detach().appendTo(currentCoverTarget);
        jQuery('#playlist-image-container').show();
    }
    else {
        jQuery('#playlist-image-container').hide();
        jQuery('#playlist-image-container').detach().appendTo(jQuery("div#wrapper"));
    }

    var currentItemPlay = jQuery('.playlist-dropdown[data-playlist-id="' + thePlaylistId + '"] .playlist-item-play');
    currentItemPlay.hide();
}

function detachPlaylistControl() {
    jQuery('#track-cp-container').hide();
    jQuery('#track-cp-container').detach().appendTo(jQuery('div#wrapper'));

    jQuery('#playlist-image-container').hide();
    jQuery('#playlist-image-container').detach().appendTo(jQuery('div#wrapper'));

    var currentItemPlay = jQuery('.playlist-dropdown .playlist-item-play');
    currentItemPlay.show();
}

function updateDisplays() {
    if (beats.unlocked) {
        //jQuery('#displayPreview').hide();
        jQuery('.a_locked').hide();
        jQuery('.a_unlocked').show();
        jQuery('body').addClass('logged');
    }
    else {
        //jQuery('#displayPreview').show();
        jQuery('.a_locked').show();
        jQuery('.a_unlocked').hide();
        jQuery('body').removeClass('logged');
    }
    if (is_allowed() && (beats.intl == beats.BEATS_AVAILABILITY_OK || beats.intl == beats.BEATS_AVAILABILITY_INTL_DESCENDING_WARNING)) {
       // jQuery('.playlist-item-play-pause').show();
    }
    else {
      //  jQuery('.playlist-item-play-pause').hide();
    }
     
    updateInternational();
}

function updateInternational() {

    if (beats.intl == beats.BEATS_AVAILABILITY_OK || beats.intl == beats.BEATS_AVAILABILITY_INTL_DESCENDING_WARNING) {
        jQuery('.a_intl_asc').hide();
        jQuery('.a_intl_desc').hide();
        jQuery('.a_intl_ok').show();
        if (beats.intl == beats.BEATS_AVAILABILITY_INTL_DESCENDING_WARNING) {
            jQuery('.a_intl_desc_wng').show();
        }
        else {
            jQuery('.a_intl_desc_wng').hide();
        }
    }
    else if (beats.intl == beats.BEATS_AVAILABILITY_INTL_DESCENDING) {
        jQuery('.a_intl_asc').hide();
        jQuery('.a_intl_desc').show();
        jQuery('.a_intl_desc_wng').hide();
        jQuery('.a_intl_ok').hide();
    }
    else if (beats.intl == beats.BEATS_AVAILABILITY_INTL_ASCENDING) {
        jQuery('.a_intl_asc').show();
        jQuery('.a_intl_desc').hide();
        jQuery('.a_intl_desc_wng').hide();
        jQuery('.a_intl_ok').hide();
    }
}

function updateSongsLeftCircle(songs_left, songs_max) {
    var degrees = Math.round(songs_left * 1.0 / songs_max * 360);
    var progress_holder = jQuery('#main-songs-left')
    var progress_p1 = jQuery('#main-songs-left .first-half')
    var progress_p2 = jQuery('#main-songs-left .second-half')

    if (degrees > 180) {
        progress_holder.addClass('over-half');
        progress_p1.css({
                    '-ms-transform': 'rotate(180deg)',
                    '-webkit-transform': 'rotate(180deg)',
                    'transform': 'rotate(180deg)'
        });
        progress_p2.css({
                    '-ms-transform': 'rotate(' + degrees + 'deg)',
                    '-webkit-transform': 'rotate(' + degrees + 'deg)',
                    'transform': 'rotate(' + degrees + 'deg)'
        });
        progress_p1.show();
        progress_p2.show();
    }
    else if (degrees > 0) {
        progress_holder.removeClass('over-half');
        progress_p1.css({
                    '-ms-transform': 'rotate(' + degrees + 'deg)',
                    '-webkit-transform': 'rotate(' + degrees + 'deg)',
                    'transform': 'rotate(' + degrees + 'deg)'
        });
        progress_p1.show();
        progress_p2.hide();
    }
    else {
        progress_p1.hide();
        progress_p2.hide();
    }
}

