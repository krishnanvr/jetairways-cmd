
/*
 * TLG - Added July 29, 2013
 *
 * This function is called when the user switches tabs on the TV page.
 * It sets the destination of the top "Buy Now" button based on which
 * tab is being selected so that if user clicks the button, the correct
 * parameter is passed to the purchase page.
 */
function tabset1TVTabChange(tabRef) {
    if (tabRef.attr('href') == '#tab2_2') {
        jQuery('#topbuynow').attr('href', '/purchase?vodtv');
    }
    else {
        jQuery('#topbuynow').attr('href', '/purchase?iptv');
    }
}

function tabset5ChannelChange(tabRef){
	// console.log(tabRef);
	$('.news_grid .tabset5 .slide.active').not($(tabRef).parents('.slide').first()).removeClass('active');
	$('.tabset5').removeClass('current').filter($(tabRef).parents('.tabset5').first()).addClass('current');
}
