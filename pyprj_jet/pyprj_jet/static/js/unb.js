// page init
jQuery(function(){
	initDropDown();
	initAccordion();
});


// animated navigation init
function initDropDown() {
	jQuery('nav.menu-holder').animDropdown({
		items: 'li',
		drop: '>div.drop',
		animSpeed: 400,
		effect: 'slide'
	});
}

// accordion menu init
function initAccordion() {
	jQuery('ul#r44universalnavbar').slideAccordion({
		activeClass: 'r44universalnavbar',
		opener: 'a.opener',
		slider: 'div.slide',
		animSpeed: 300
	});
}


/*
 * jQuery Dropdown plugin
 */
;(function($){
	$.fn.animDropdown = function(o){
		// default options
		var options = $.extend({
			hoverClass:'hover',
			dropClass:'drop-active',
			items: 'li',
			drop: '>ul',
			delay: 100,
			animSpeed: 300,
			effect: 'fade'
		},o);

		return this.each(function(){
			// options
			var nav = $(this);
				items = nav.find(options.items);

			items.addClass(options.hoverClass).each(function(){
				var item = $(this), delayTimer;
				var drop = item.find(options.drop);
				item.data('drop', drop);
				if(drop.length) {
					dropdownEffects[options.effect].prepare({item:item,drop:drop});
				}

				item.bind('mouseenter', function(){
					hideAllDropdowns(item);
					item.addClass(options.hoverClass);
					clearTimeout(delayTimer);
					delayTimer = setTimeout(function(){
						if(drop.length && item.hasClass(options.hoverClass)) {
							item.addClass(options.dropClass);
							dropdownEffects[options.effect].animate({drop:drop, state:true, speed:options.animSpeed, complete:function(){
								// callback
							}});
						}
					}, options.delay);
					item.data('timer', delayTimer);
				}).bind('mouseleave', function(){
					if(!item.hasClass(options.dropClass)) {
						item.removeClass(options.hoverClass);
					}
					clearTimeout(delayTimer);
					delayTimer = setTimeout(function(){
						if(drop.length && item.hasClass(options.dropClass)) {
							dropdownEffects[options.effect].animate({drop:drop, state:false, speed:options.animSpeed, complete:function(){
								// callback
								item.removeClass(options.hoverClass);
								item.removeClass(options.dropClass);
							}});
						}
					}, options.delay);
					item.data('timer', delayTimer);
				});
			});

			// hide dropdowns
			items.removeClass(options.hoverClass);
			if(dropdownEffects[options.effect].postProcess) {
				items.each(function(){
					dropdownEffects[options.effect].postProcess({item: $(this)});
				});
			}

			// hide current level dropdowns
			function hideAllDropdowns(except) {
				var siblings = except.siblings();
				siblings.removeClass(options.hoverClass).each(function(){
					var item = $(this);
					clearTimeout(item.data('timer'));
				});
				siblings.filter('.' + options.dropClass).each(function(){
					var item = jQuery(this).removeClass(options.dropClass);
					if(item.data('drop').length) {
						dropdownEffects[options.effect].animate({drop:item.data('drop'), state:false, speed:options.animSpeed});
					}
				});
			}
		});
	};

	// dropdown effects
	var dropdownEffects = {
		fade: {
			prepare: function(o) {
				o.drop.css({opacity:0,display:'none'});
			},
			animate: function(o) {
				o.drop.stop().show().animate({opacity: o.state ? 1 : 0},{duration: o.speed || 0, complete: function(){
					if(o.state) {
						o.drop.css({opacity:''});
					} else {
						o.drop.css({opacity:0,display:'none'});
					}
					if(typeof o.complete === 'function') {
						o.complete.call(o.drop);
					}
				}});
			}
		},
		slide: {
			prepare: function(o) {
				var elementWrap = o.drop.wrap('<div class="drop-slide-wrapper">').show().parent();
				var elementHeight = o.drop.outerHeight(true);
				var elementWidth = o.drop.outerWidth(true);
				elementWrap.css({
					height:elementHeight,
					width: elementWidth,
					position:'absolute',
					overflow:'hidden',
					top: o.drop.css('top'),
					left: o.drop.css('left')
				});
				o.drop.css({
					position:'static',
					display:'block',
					top: 'auto',
					left: 'auto'
				});
				o.drop.data('height', elementHeight).data('wrap', elementWrap).css({marginTop: -elementHeight});
			},
			animate: function(o) {
				o.drop.data('wrap').show().css({overflow:'hidden'});
				o.drop.stop().animate({marginTop: o.state ? 0 : -o.drop.data('height')},{duration: o.speed || 0, complete: function(){
					if(o.state) {
						o.drop.css({marginTop:''});
						o.drop.data('wrap').css({overflow:''});
					} else {
						o.drop.data('wrap').css({display:'none'});
					}
					if(typeof o.complete === 'function') {
						o.complete.call(o.drop);
					}
				}});
			},
			postProcess: function(o) {
				if(o.item.data('drop').length) {
					o.item.data('drop').data('wrap').css({display:'none'});
				}
			}
		}
	};
}(jQuery));

/*
 * jQuery Accordion plugin
 */
;(function($){
	$.fn.slideAccordion = function(opt){
		// default options
		var options = $.extend({
			addClassBeforeAnimation: false,
			activeClass:'active',
			opener:'.opener',
			slider:'.slide',
			animSpeed: 300,
			collapsible:true,
			event:'click'
		},opt);

		return this.each(function(){
			// options
			var accordion = $(this);
			var items = accordion.find(':has('+options.slider+')');

			items.each(function(){
				var item = $(this);
				var opener = item.find(options.opener);
				var slider = item.find(options.slider);
				opener.bind(options.event, function(e){
					if(!slider.is(':animated')) {
						if(item.hasClass(options.activeClass)) {
							if(options.collapsible) {
								slider.slideUp(options.animSpeed, function(){
									hideSlide(slider);
									item.removeClass(options.activeClass);
								});
							}
						} else {
							// show active
							var levelItems = item.siblings('.'+options.activeClass);
							var sliderElements = levelItems.find(options.slider);
							item.addClass(options.activeClass);
							showSlide(slider).hide().slideDown(options.animSpeed);
						
							// collapse others
							sliderElements.slideUp(options.animSpeed, function(){
								levelItems.removeClass(options.activeClass);
								hideSlide(sliderElements);
							});
						}
					}
					e.preventDefault();
				});
				if(item.hasClass(options.activeClass)) showSlide(slider); else hideSlide(slider);
			});
		});
	};

	// accordion slide visibility
	var showSlide = function(slide) {
		return slide.css({position:'', top: '', left: '', width: '' });
	};
	var hideSlide = function(slide) {
		return slide.show().css({position:'absolute', top: -9999, left: -9999, width: slide.width() });
	};
}(jQuery));

